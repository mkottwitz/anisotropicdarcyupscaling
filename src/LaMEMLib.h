//---------------------------------------------------------------------------
//........................ LaMEM Library major context .......................
//---------------------------------------------------------------------------
#ifndef __LaMEMLib_h__
#define __LaMEMLib_h__
//---------------------------------------------------------------------------

enum RunMode
{
	//==================
	// simulation modes
	//==================

	_NORMAL_,    // start new simulation
	_DRY_RUN_,   // initialize model, output & stop
	_SAVE_GRID_, // write parallel grid to a file & stop

};

//---------------------------------------------------------------------------

struct LaMEMLib
{
	Scaling  		scal;   // scaling
	TSSol    		ts;     // time-stepping controls
	FDSTAG   		fs;     // grid layout
	FreeSurf 		surf;   // free-surface grid
	BCCtx    		bc;     // boundary condition context
	JacRes   		jr;     // Jacobian & residual context
	PVOut    		pvout;  // paraview output driver
	PVSurf   		pvsurf; // paraview output driver for surface

};

//---------------------------------------------------------------------------
// LAMEM LIBRARY FUNCTIONS
//---------------------------------------------------------------------------

PetscErrorCode LaMEMLibCreate(LaMEMLib *lm);

PetscErrorCode LaMEMLibSaveGrid(LaMEMLib *lm);

PetscErrorCode LaMEMLibDestroy(LaMEMLib *lm);

PetscErrorCode LaMEMLibSetLinks(LaMEMLib *lm);

PetscErrorCode LaMEMLibSaveOutput(LaMEMLib *lm, PetscInt dirInd);

PetscErrorCode LaMEMLibSolve(LaMEMLib *lm);

PetscErrorCode LaMEMLibDryRun(LaMEMLib *lm);

PetscErrorCode LaMEMLibSolveDarcy(LaMEMLib *lm);

//---------------------------------------------------------------------------

PetscErrorCode SNESPrintConvergedReason(SNES snes, PetscLogDouble t_beg);

//---------------------------------------------------------------------------

#endif
