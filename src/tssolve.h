//---------------------------------------------------------------------------
//......................   TIME STEPPING PARAMETERS   .......................
//---------------------------------------------------------------------------
#ifndef __tssolve_h__
#define __tssolve_h__
//---------------------------------------------------------------------------

struct FB;
struct Scaling;
struct FreeSurf;

//---------------------------------------------------------------------------

struct TSSol
{
	Scaling  *scal;  // scaling
	FreeSurf *surf;  // free surface

	PetscScalar dt;         // time step (zero for steady-state run)
	PetscScalar dt_min;     // minimum time step (declare divergence if smaller value is attempted)
	PetscScalar dt_max;     // maximum time step (truncate if larger value is attempted)
	PetscScalar dt_out;     // output step (output at least at fixed time intervals)
	PetscScalar inc_dt;     // time step increment per time step (fraction of unit)
	PetscScalar time;       // current time
	PetscScalar time_out;   // previous output time stamp
	PetscScalar time_end;   // simulation end time
	PetscScalar tol;        // tolerance for time comparisons
	PetscInt    nstep_max;  // maximum allowed number of steps (lower bound: time_end/dt_max)
	PetscInt    nstep_out;  // save output every n steps
	PetscInt    nstep_ini;  // save output for n initial steps
	PetscInt    istep;      // time step counter

	PetscInt    num_dtper;  				// number of time stepping periods
	PetscScalar t_dtper[_max_periods_+1];   // timestamps where timestep should be fixed
	PetscScalar dt_dtper[_max_periods_+1];  // target timesteps ar timestamps above
    PetscScalar schedule[_max_num_steps_];  // time stepping schedule
};

//---------------------------------------------------------------------------

PetscErrorCode TSSolCreate(TSSol *ts, FB *fb);

PetscInt       TSSolIsDone(TSSol *ts);

PetscErrorCode TSSolStepForward(TSSol *ts);

PetscInt       TSSolIsOutput(TSSol *ts);

PetscErrorCode TSSolGetStep(
	TSSol       *ts); // ,PetscInt    *restart time step restart flag

PetscErrorCode TSSolGetPeriodSteps(
		PetscScalar  dt_start, // timestep at the start of the period
		PetscScalar  dt_end,   // timestep at the end of the period
		PetscScalar  span,     // time span of period
		PetscScalar *dt,       // time steps in period
		PetscInt    &n);       // number of time steps

PetscErrorCode TSSolMakeSchedule(TSSol *ts);

PetscErrorCode TSSolAdjustSchedule(TSSol *ts, PetscScalar dt_cfl, PetscInt istep, PetscScalar *schedule);

//---------------------------------------------------------------------------
#endif


