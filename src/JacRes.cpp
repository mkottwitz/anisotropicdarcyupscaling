//---------------------------------------------------------------------------
//...................   FDSTAG JACOBIAN AND RESIDUAL  .......................
//---------------------------------------------------------------------------
#include "LaMEM.h"
#include "JacRes.h"
#include "parsing.h"
#include "tssolve.h"
#include "scaling.h"
#include "fdstag.h"
#include "surf.h"
#include "bc.h"
#include "tools.h"
#include "FEA.h"
//---------------------------------------------------------------------------
PetscErrorCode JacResCreate(JacRes *jr, FB *fb)
{
	Scaling  *scal;
	FreeSurf *surf;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	scal = jr->scal;
	surf = jr->surf;

	// read from options
	ierr = getScalarParam(fb, _REQUIRED_, "gravity",    &jr->grav,     1, 1.0); CHKERRQ(ierr);
	ierr = getScalarParam(fb, _REQUIRED_, "eta",        &jr->eta,      1, 1.0); CHKERRQ(ierr);
	ierr = getScalarParam(fb, _REQUIRED_, "rho",        &jr->rho,      1, 1.0); CHKERRQ(ierr);

	if(surf->UseFreeSurf)
	{
		// read Van-Genuchten model parameters
		ierr = getScalarParam(fb, _REQUIRED_, "phi",        &jr->phi,        1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _REQUIRED_, "beta",       &jr->beta,       1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _REQUIRED_, "vg_alpha",   &jr->alpha,      1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _REQUIRED_, "vg_l",       &jr->l,          1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _REQUIRED_, "vg_theta_r", &jr->theta_r,    1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _REQUIRED_, "krr",        &jr->krr,        1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "k_ar",       &jr->k_ar,       1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _REQUIRED_, "dh",         &jr->dh,         1, 1.0); CHKERRQ(ierr);

		// read steady state stopping parameters
		ierr = getIntParam   (fb, _OPTIONAL_, "check_steadyState",     &jr->check_ss,     1, 1);   CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "check_steadyState_tol", &jr->check_ss_tol, 1, 1.0); CHKERRQ(ierr);

		// read dynamic model parameters vor Van-Genuchten parameters
		ierr = getIntParam   (fb, _OPTIONAL_, "vg_dyn_use", &jr->vg_dyn_use, 1, 1); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "vg_dyn_a_p", &jr->vg_dyn_a_p, 1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "vg_dyn_a_b", &jr->vg_dyn_a_b, 1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "vg_dyn_a_n", &jr->vg_dyn_a_n, 1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "vg_dyn_l_p", &jr->vg_dyn_l_p, 1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "vg_dyn_l_b", &jr->vg_dyn_l_b, 1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "vg_dyn_l_n", &jr->vg_dyn_l_n, 1, 1.0); CHKERRQ(ierr);

		// activate gravity for the free surface flow
		jr->actGrav = 1;

		// set number of parameters
		jr->numPar = 7;
	}
	else
	{
		// activate effective permeability computation for saturated flow
		jr->getPermea = 1;

		// set number of parameters
		jr->numPar = 6;
	}

	// print summary
	PetscPrintf(PETSC_COMM_WORLD, "Solution parameters & controls:\n");

	if(jr->grav)     PetscPrintf(PETSC_COMM_WORLD, "   Gravity acceleration                 : %g %s \n", jr->grav,    scal->lbl_gravity_strength);
	if(jr->eta)      PetscPrintf(PETSC_COMM_WORLD, "   Fluid viscosity                      : %g %s \n", jr->eta,     scal->lbl_viscosity);
	if(jr->rho)      PetscPrintf(PETSC_COMM_WORLD, "   Fluid density                        : %g %s \n", jr->rho,     scal->lbl_density);
	if(jr->phi)      PetscPrintf(PETSC_COMM_WORLD, "   Matrix compressibility               : %g %s \n", jr->phi,     scal->lbl_inverse_stress);
	if(jr->beta)     PetscPrintf(PETSC_COMM_WORLD, "   Fluid compressibility                : %g %s \n", jr->beta,    scal->lbl_inverse_stress);
	if(jr->vg_dyn_use)
	{
		PetscPrintf(PETSC_COMM_WORLD, "   dynamic van Genuchten parameter model activated \n");
	}
	else
	{
		if(jr->alpha)    PetscPrintf(PETSC_COMM_WORLD, "   van Genuchten pre-factor             : %g %s \n", jr->alpha,   scal->lbl_inverse_length);
		if(jr->l)        PetscPrintf(PETSC_COMM_WORLD, "   van Genuchten exponent               : %g %s \n", jr->l,       "[ ]");
	}
	if(jr->theta_r)  PetscPrintf(PETSC_COMM_WORLD, "   van Genuchten residual fluid content : %g %s \n", jr->theta_r, "[ ]");
	if(jr->krr)      PetscPrintf(PETSC_COMM_WORLD, "   Residual relative permeability       : %g %s \n", jr->krr,     "[ ]");
	if(jr->k_ar)     PetscPrintf(PETSC_COMM_WORLD, "   Absolute relative permeability       : %g %s \n", jr->k_ar,    scal->lbl_area_si);
	if(jr->dh)       PetscPrintf(PETSC_COMM_WORLD, "   Fluid content differentiation step   : %g %s \n", jr->dh,      scal->lbl_length_si);
	if(jr->actGrav)  PetscPrintf(PETSC_COMM_WORLD, "   Activate gravity                     @ \n");
	if(jr->getPermea)PetscPrintf(PETSC_COMM_WORLD, "   Activate permeability computation    @ \n");

	PetscPrintf(PETSC_COMM_WORLD,"--------------------------------------------------------------------------\n");

	// scale parameters
	jr->grav  /=  scal->gravity_strength;
	jr->eta   /=  scal->viscosity;
	jr->rho   /=  scal->density;
	jr->phi   *=  scal->stress_si;
	jr->beta  *=  scal->stress_si;
	jr->alpha *=  scal->length_si;
	jr->dh    /=  scal->length_si;
	jr->k_ar  /=  scal->area_si;

	// create Jacobian & residual evaluation context
	ierr = JacResCreateData(jr); CHKERRQ(ierr);

	// read cell parameters from files in parallel
	ierr = JacResReadParm(jr, fb); CHKERRQ(ierr);

	// compute effective permeability
	ierr = JacResGetEffPerm(jr); CHKERRQ(ierr);

	// check if hpe is activated
	ierr = getIntParam(fb, _OPTIONAL_, "hpe_use", &jr->hpe_use, 1,  1); CHKERRQ(ierr);

	if(jr->hpe_use)
	{
		// read hpe parameters
		ierr = getScalarParam(fb, _OPTIONAL_, "hpe_a",       &jr->hpe_a,     1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "hpe_r",       &jr->hpe_r,     1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "hpe_beta",    &jr->hpe_beta,  1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _OPTIONAL_, "hpe_r50",     &jr->hpe_r50,   1, 1.0); CHKERRQ(ierr);

		// print summary
		PetscPrintf(PETSC_COMM_WORLD, "Hydraulic property evolution parameters:\n");

		if(jr->hpe_a)    PetscPrintf(PETSC_COMM_WORLD, "   Maximal reduction value              : %g %s \n", jr->hpe_a,    "[ ]");
		if(jr->hpe_r)    PetscPrintf(PETSC_COMM_WORLD, "   Magnitudal bandwith of increase      : %g %s \n", jr->hpe_r,    "[ ]");
		if(jr->hpe_beta) PetscPrintf(PETSC_COMM_WORLD, "   Approximation exponent               : %g %s \n", jr->hpe_beta, "[ ]");
		if(jr->hpe_r50)  PetscPrintf(PETSC_COMM_WORLD, "   Half-increase value                  : %g %s \n", jr->hpe_r50,  "[ ]");

		PetscPrintf(PETSC_COMM_WORLD,"--------------------------------------------------------------------------\n");

	}

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResCreateData"
PetscErrorCode JacResCreateData(JacRes *jr)
{
	FDSTAG   *fs;
	size_t   sz;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs = jr->fs;
	
	//========================
	// create solution vectors
	//========================

	// hydraulic head, pressure, residual, saturation
	ierr = DMCreateGlobalVector(fs->DA_COR, &jr->hn);     CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(fs->DA_COR, &jr->pv);     CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(fs->DA_COR, &jr->rv);     CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(fs->DA_COR, &jr->hdiff);  CHKERRQ(ierr);

	// Darcy velocities
	ierr = DMCreateGlobalVector(fs->DA_COR, &jr->vx); CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(fs->DA_COR, &jr->vy); CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(fs->DA_COR, &jr->vz); CHKERRQ(ierr);

	// cell parameters storage
	sz = jr->numPar*sizeof(PetscScalar)*(size_t)fs->nCells;
	ierr = PetscMalloc(sz, &jr->parm); CHKERRQ(ierr);

	// cell parameters storage
	sz = sizeof(PetscScalar)*(size_t)fs->nCells;
	ierr = PetscMalloc(sz, &jr->peff); CHKERRQ(ierr);

	// cell parameters storage
	sz = sizeof(PetscScalar)*(size_t)fs->nCells;
	ierr = PetscMalloc(sz, &jr->sat); CHKERRQ(ierr);

	// cell parameters storage
	sz = sizeof(PetscScalar)*(size_t)fs->nCells;
	ierr = PetscMalloc(sz, &jr->courant); CHKERRQ(ierr);

	// cell parameters storage
	sz = sizeof(PetscScalar)*(size_t)fs->nCells;
	ierr = PetscMalloc(sz, &jr->hpe_rf); CHKERRQ(ierr);


	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResDestroy"
PetscErrorCode JacResDestroy(JacRes *jr)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = VecDestroy(&jr->hn);         CHKERRQ(ierr);
	ierr = VecDestroy(&jr->pv);         CHKERRQ(ierr);
	ierr = VecDestroy(&jr->rv);         CHKERRQ(ierr);
	ierr = VecDestroy(&jr->hdiff);      CHKERRQ(ierr);

	ierr = VecDestroy(&jr->vx);         CHKERRQ(ierr);
	ierr = VecDestroy(&jr->vy);         CHKERRQ(ierr);
	ierr = VecDestroy(&jr->vz);         CHKERRQ(ierr);

	ierr = PetscFree(jr->parm);     	CHKERRQ(ierr);
	ierr = PetscFree(jr->peff);     	CHKERRQ(ierr);
	ierr = PetscFree(jr->sat);     	    CHKERRQ(ierr);
	ierr = PetscFree(jr->courant);     	CHKERRQ(ierr);
	ierr = PetscFree(jr->hpe_rf);     	CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//-----------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResReadParm"
PetscErrorCode JacResReadParm(JacRes *jr, FB *fb)
{
	FILE           *fp;
	size_t          sz;
	PetscLogDouble  t;
	PetscMPIInt     rank;
	PetscInt        i, n, numPar;
	PetscScalar    *parm, cf;
	struct          stat sb;
	char           *filename, file[_str_len_];

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// get file name
	ierr = getStringParam(fb, _OPTIONAL_, "parm_file", file, "./bc/parm"); CHKERRQ(ierr);

	PrintStart(&t, "Loading cell parameters in parallel from", file);

	// compile input file name with extension
	MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

	asprintf(&filename, "%s.%1.8lld.dat", file, (LLD)rank);

	// open file
	fp = fopen(filename, "rb");

	if(fp == NULL)
	{
		SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Cannot open input file %s\n", filename);
	}

	// check file size
	stat(filename, &sb);

	// get file size
	numPar = jr->numPar;
	sz     = numPar*sizeof(PetscScalar)*(size_t)jr->fs->nCells;

	if((size_t)sb.st_size != sz)
	{
		SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Wrong input file size %s\n", filename);
	}

	// read cell parameters
	fread(jr->parm, sz, 1, fp);

	// close file
	fclose(fp);

	// scale permeabilities
	n  = jr->fs->nCells;
	cf = jr->scal->area_si;

	for(i = 0; i < n; i++)
	{
		parm     = jr->parm + numPar*i;
		parm[0] /= cf;
		parm[1] /= cf;
		parm[2] /= cf;
		parm[3] /= cf;
		parm[4] /= cf;
		parm[5] /= cf;
	}

	PrintDone(t);

	PetscFunctionReturn(0);
}

//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResGetEffPerm"
PetscErrorCode JacResGetEffPerm(JacRes *jr)
{
	FDSTAG      *fs;
	PetscScalar *parm, *peff, kxx, kyy, kzz, kxy, kxz, kyz;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, iter, numPar;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs     = jr->fs;
	peff   = jr->peff;
	numPar = jr->numPar;

	ierr = DMDAGetCorners(fs->DA_CEN, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	iter = 0;

	START_STD_LOOP
	{
		// access permeability components
		parm = jr->parm + numPar*iter;
		kxx  = parm[0];
		kyy  = parm[1];
		kzz  = parm[2];
		kxy  = parm[3];
		kxz  = parm[4];
		kyz  = parm[5];

		// compute effective permeability
		peff[iter] = sqrt(kxx*kxx + kyy*kyy + kzz*kzz + kxy*kxy + kxz*kxz + kyz*kyz);

		iter++;
	}
	END_STD_LOOP

	PetscFunctionReturn(0);
}

//---------------------------------------------------------------------------

#undef __FUNCT__
#define __FUNCT__ "JacResInitGuess"
PetscErrorCode JacResInitGuess(JacRes *jr)
{
	// initialize head history
	FDSTAG      *fs;
	FreeSurf    *surf;
	PetscScalar ***topo, ***hn, ***hp, bz, hi, sweight, cf, z;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, level;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs      = jr->fs;
	surf    = jr->surf;
	level   = (PetscInt)fs->dsz.rank;
	sweight = jr->grav*jr->rho;

	// check gravity activation
	if(jr->actGrav) cf = 1.0;
	else            cf = 0.0;

	// initialize head history vector
	ierr = VecZeroEntries(jr->hn); CHKERRQ(ierr);

	// check whether free surface is activated
	if(!surf->UseFreeSurf) PetscFunctionReturn(0);

	// get global box bounds
	ierr = FDSTAGGetGlobalBox(fs, NULL, NULL, &bz, NULL, NULL, NULL); CHKERRQ(ierr);

	ierr = DMDAVecGetArray(surf->DA_SURF, surf->gtopo, &topo);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs  ->DA_COR,  jr->hn,      &hn);    CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs  ->DA_COR,  jr->pv,      &hp);    CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP
	{
		// get hydraulic head
		hi = topo[level][j][i] - bz;

		// set 2D hydraulic head distribution
		hn[k][j][i] = hi;

		// get elevation head
		z = COORD_NODE(k, sz, fs->dsz) - bz;

		// compute pressure
		hp[k][j][i] = (hi - cf*z)*sweight;

	}
	END_STD_LOOP

	// integration cell saturation from nodal pressure field as well
	ierr = JacResGetSaturation(jr, jr->hn); CHKERRQ(ierr);

	// output initial observation heads
	ierr = JacResGetObservationHeads(jr, 1); CHKERRQ(ierr);

	// restore access
	ierr = DMDAVecRestoreArray(surf->DA_SURF, surf->gtopo, &topo);  CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs  ->DA_COR,  jr->hn,      &hn);    CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs  ->DA_COR,  jr->pv,      &hp);    CHKERRQ(ierr);

	PetscFunctionReturn(0);


}

//---------------------------------------------------------------------------

#undef __FUNCT__
#define __FUNCT__ "JacResGetJac"
PetscErrorCode JacResGetJac(JacRes *jr, Vec x, Mat Pmat)
{
	FDSTAG     *fs;
	BCCtx      *bc;
	Vec         lh;
	MatStencil  idx[8];
	PetscScalar cX1[8], cX2[8], cX3[8], dx, dy, dz;
	PetscScalar dX1[8], dX2[8], dX3[8], H1, H2, H3, h1, h2, h3;
	PetscScalar px1[8], px2[8], px3[8], pwt[8], lx1, lx2, lx3, detJ, v[64];
	PetscScalar ***h, *parm, eta, kxx, kyy, kzz, kxy, kxz, kyz;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, iter, cnt, ip, ii, jj, numPar;
	PetscScalar N[8], Ni, Nj, eh[8], ph, z, zb, bz, rho, grav, dt, A, kr;
	PetscScalar n, Ss, Sw, phi, beta, theta, dh, dthetadh, k_eff, K_eff;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs     = jr->fs;
	bc     = jr->bc;
	numPar = jr->numPar;
	grav   = jr->grav;
	eta    = jr->eta;
	rho    = jr->rho;
	beta   = jr->beta;
	phi    = jr->phi;
	dh     = jr->dh;
	dt     = jr->ts->dt;

	// initialize steady-state simulation
	if(!dt)
	{
		A  = 0.0;
		kr = 1.0;
	}

	// setup local vector
	ierr = DMGetLocalVector(fs->DA_COR, &lh); CHKERRQ(ierr);

	GLOBAL_TO_LOCAL(fs->DA_COR, x, lh)

	ierr = DMDAVecGetArray(fs->DA_COR, lh, &h);  CHKERRQ(ierr);

	// get quadrature
	ierr = getQuad(px1, px2, px3, pwt);  CHKERRQ(ierr);

	// get global box bounds
	ierr = FDSTAGGetGlobalBox(fs, NULL, NULL, &bz, NULL, NULL, NULL); CHKERRQ(ierr);

	//------------------
	// assemble Jacobian
	//------------------

	ierr = MatZeroEntries(Pmat); CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_CEN, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	iter = 0;

	START_STD_LOOP
	{
		// access current head
		eh[0] = h[k]  [j]  [i];
		eh[1] = h[k]  [j]  [i+1];
		eh[2] = h[k]  [j+1][i+1];
		eh[3] = h[k]  [j+1][i];
		eh[4] = h[k+1][j]  [i];
		eh[5] = h[k+1][j]  [i+1];
		eh[6] = h[k+1][j+1][i+1];
		eh[7] = h[k+1][j+1][i];

		// get mesh steps
		dx = SIZE_CELL (i, sx, fs->dsx);
		dy = SIZE_CELL (j, sy, fs->dsy);
		dz = SIZE_CELL (k, sz, fs->dsz);
		zb = COORD_NODE(k, sz, fs->dsz);

		// get corner (relative) coordinates
		cX1[0] = 0.0;  cX2[0] = 0.0;  cX3[0] = 0.0;
		cX1[1] = dx;   cX2[1] = 0.0;  cX3[1] = 0.0;
		cX1[2] = dx;   cX2[2] = dy;   cX3[2] = 0.0;
		cX1[3] = 0.0;  cX2[3] = dy;   cX3[3] = 0.0;
		cX1[4] = 0.0;  cX2[4] = 0.0;  cX3[4] = dz;
		cX1[5] = dx;   cX2[5] = 0.0;  cX3[5] = dz;
		cX1[6] = dx;   cX2[6] = dy;   cX3[6] = dz;
		cX1[7] = 0.0;  cX2[7] = dy;   cX3[7] = dz;

		// get hydraulic conductivities
		parm  = jr->parm + numPar*iter;
		kxx   = rho*grav*parm[0]/eta;
		kyy   = rho*grav*parm[1]/eta;
		kzz   = rho*grav*parm[2]/eta;
		kxy   = rho*grav*parm[3]/eta;
		kxz   = rho*grav*parm[4]/eta;
		kyz   = rho*grav*parm[5]/eta;

		// compute effective local permeability and conductivity for relative permeability scaling
		k_eff = sqrt(parm[0]*parm[0] + parm[1]*parm[1] + parm[2]*parm[2] + parm[3]*parm[3] + parm[4]*parm[4] + parm[5]*parm[5]);
		K_eff = sqrt(kxx*kxx + kyy*kyy + kzz*kzz + kxy*kxy + kxz*kxz + kyz*kyz);

		// zero out element matrix
		ierr = PetscMemzero(v, sizeof(v)); CHKERRQ(ierr);

		// integration point loop
		for(ip = 0; ip < 8; ip++)
		{
			// local coordinates
			lx1 = px1[ip];
			lx2 = px2[ip];
			lx3 = px3[ip];

			// get shape functions and derivatives
			ierr = getShape     (lx1, lx2, lx3, N);                                  CHKERRQ(ierr);
			ierr = getShapeDeriv(lx1, lx2, lx3, cX1, cX2, cX3, dX1, dX2, dX3, detJ); CHKERRQ(ierr);

			// get hydraulic head, and elevation head
			ph = 0.0;
			z  = zb;

			for(ii = 0; ii < 8; ii++)
			{
				ph += N[ii]*eh [ii];
				z  += N[ii]*cX3[ii];
			}

			z -= bz;

			// get porosity, fluid content, saturation, specific storage & relative permeability
			// approximate fluid content derivative with central difference
			if(dt)
			{
				n        = parm[6];
				theta    = getFluidConent(jr, ph, z, n, K_eff);
				Sw       = theta/n;
				Ss       = rho*grav*(phi + n*beta);
				kr       = getRelPerm(jr, theta, n, k_eff, K_eff);
				dthetadh = (getFluidConent(jr, ph + dh/2.0, z, n, K_eff) - getFluidConent(jr, ph - dh/2.0, z, n, K_eff))/dh;
				A        = (dthetadh + Sw*Ss)/dt;
			}

			// compute element Jacobian matrix
			cnt = 0;

			for(ii = 0; ii < 8; ii++)
			{
				h1 = dX1[ii];
				h2 = dX2[ii];
				h3 = dX3[ii];
				Ni = N  [ii];

				H1 = kr*(h1*kxx + h2*kxy + h3*kxz);
				H2 = kr*(h1*kxy + h2*kyy + h3*kyz);
				H3 = kr*(h1*kxz + h2*kyz + h3*kzz);

				for(jj = 0; jj < 8; jj++)
				{
					h1 = dX1[jj];
					h2 = dX2[jj];
					h3 = dX3[jj];
					Nj = N  [jj];

					v[cnt++] += (Ni*A*Nj + (H1*h1 + H2*h2 + H3*h3))*pwt[ip]*detJ;
				}
			}
		}

		// set row/column indices
		idx[0].k = k,   idx[0].j = j,   idx[0].i = i,   idx[1].c = 0;
		idx[1].k = k,   idx[1].j = j,   idx[1].i = i+1, idx[1].c = 0;
		idx[2].k = k,   idx[2].j = j+1, idx[2].i = i+1, idx[2].c = 0;
		idx[3].k = k,   idx[3].j = j+1, idx[3].i = i,   idx[3].c = 0;
		idx[4].k = k+1, idx[4].j = j,   idx[4].i = i,   idx[4].c = 0;
		idx[5].k = k+1, idx[5].j = j,   idx[5].i = i+1, idx[5].c = 0;
		idx[6].k = k+1, idx[6].j = j+1, idx[6].i = i+1, idx[6].c = 0;
		idx[7].k = k+1, idx[7].j = j+1, idx[7].i = i,   idx[7].c = 0;

		// update Jacobian matrix
		ierr = MatSetValuesStencil(Pmat, 8, idx, 8, idx, v, ADD_VALUES); CHKERRQ(ierr);

		iter++;
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(fs->DA_COR, lh, &h); CHKERRQ(ierr);

	// finalize assembly
	ierr = MatAssemblyBegin(Pmat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd  (Pmat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	// constrain Jacobian
	ierr = MatZeroRowsColumnsStencil(Pmat, bc->nbc, bc->ibc, 1.0, NULL, NULL); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}

//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResGetRes"
PetscErrorCode JacResGetRes(JacRes *jr, Vec x, Vec f, PetscInt getFlux)
{
	FDSTAG      *fs;
	BCCtx       *bc;
	FreeSurf    *surf;
	Vec         lh, lhn, lr;
	PetscScalar eh[8], ehn[8], er[8], ph, phn, bz, z;
	PetscScalar cX1[8], cX2[8], cX3[8], dx, dy, dz;
	PetscScalar dX1[8], dX2[8], dX3[8], N[8], Ni, H1, H2, H3, h1, h2, h3;
	PetscScalar px1[8], px2[8], px3[8], pwt[8], lx1, lx2, lx3, detJ;
	PetscScalar *parm, kxx, kyy, kzz, kxy, kxz, kyz, dt;
	PetscScalar n, Ss, Sw, eta, rho, grav, phi, beta, theta, theta_n, kr, A, k_eff, K_eff;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, iter, ip, ii;
	PetscScalar ***h, ***hn, ***r, *gr, zb;
	PetscInt    nbc, *idx, numPar;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs      = jr->fs;
	bc      = jr->bc;
	numPar  = jr->numPar;
	surf    = jr->surf;
	grav    = jr->grav;
	eta     = jr->eta;
	rho     = jr->rho;
	beta    = jr->beta;
	phi     = jr->phi;
	dt      = jr->ts->dt;
	nbc     = bc->nbc;
	idx     = bc->idx;

	// initialize steady-state simulation
	if(!dt)
	{
		A  = 0.0;
		kr = 1.0;
	}

	// compute pressure from hydraulic head
	ierr = JacResGetPress(jr, x); CHKERRQ(ierr);

	// compute free surface topography from pressure field
	ierr = FreeSurfGetTopo(surf); CHKERRQ(ierr);

	// request local vectors
	ierr = DMGetLocalVector(fs->DA_COR, &lh);  CHKERRQ(ierr);
	ierr = DMGetLocalVector(fs->DA_COR, &lhn); CHKERRQ(ierr);
	ierr = DMGetLocalVector(fs->DA_COR, &lr);  CHKERRQ(ierr);

	// scatter to local vectors
	GLOBAL_TO_LOCAL(fs->DA_COR, x,      lh)
	GLOBAL_TO_LOCAL(fs->DA_COR, jr->hn, lhn)

	// zero out local residual
	ierr = VecZeroEntries(lr); CHKERRQ(ierr);

	// access local vectors
	ierr = DMDAVecGetArray(fs->DA_COR, lh,  &h);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, lhn, &hn); CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, lr,  &r);  CHKERRQ(ierr);

	// get quadrature
	ierr = getQuad(px1, px2, px3, pwt);  CHKERRQ(ierr);

	// get global box bounds
	ierr = FDSTAGGetGlobalBox(fs, NULL, NULL, &bz, NULL, NULL, NULL); CHKERRQ(ierr);

	//------------------
	// assemble residual
	//------------------

	ierr = DMDAGetCorners(fs->DA_CEN, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	iter = 0;

	PetscScalar krmin = 1.0;

	START_STD_LOOP
	{
		// access current head
		eh[0] = h[k]  [j]  [i];
		eh[1] = h[k]  [j]  [i+1];
		eh[2] = h[k]  [j+1][i+1];
		eh[3] = h[k]  [j+1][i];
		eh[4] = h[k+1][j]  [i];
		eh[5] = h[k+1][j]  [i+1];
		eh[6] = h[k+1][j+1][i+1];
		eh[7] = h[k+1][j+1][i];

		// access head history
		ehn[0] = hn[k]  [j]  [i];
		ehn[1] = hn[k]  [j]  [i+1];
		ehn[2] = hn[k]  [j+1][i+1];
		ehn[3] = hn[k]  [j+1][i];
		ehn[4] = hn[k+1][j]  [i];
		ehn[5] = hn[k+1][j]  [i+1];
		ehn[6] = hn[k+1][j+1][i+1];
		ehn[7] = hn[k+1][j+1][i];

		// get mesh steps
		dx = SIZE_CELL (i, sx, fs->dsx);
		dy = SIZE_CELL (j, sy, fs->dsy);
		dz = SIZE_CELL (k, sz, fs->dsz);
		zb = COORD_NODE(k, sz, fs->dsz);

		// get corner (relative) coordinates
		cX1[0] = 0.0;  cX2[0] = 0.0;  cX3[0] = 0.0;
		cX1[1] = dx;   cX2[1] = 0.0;  cX3[1] = 0.0;
		cX1[2] = dx;   cX2[2] = dy;   cX3[2] = 0.0;
		cX1[3] = 0.0;  cX2[3] = dy;   cX3[3] = 0.0;
		cX1[4] = 0.0;  cX2[4] = 0.0;  cX3[4] = dz;
		cX1[5] = dx;   cX2[5] = 0.0;  cX3[5] = dz;
		cX1[6] = dx;   cX2[6] = dy;   cX3[6] = dz;
		cX1[7] = 0.0;  cX2[7] = dy;   cX3[7] = dz;

		// get hydraulic conductivities
		parm = jr->parm + numPar*iter;
		kxx  = rho*grav*parm[0]/eta;
		kyy  = rho*grav*parm[1]/eta;
		kzz  = rho*grav*parm[2]/eta;
		kxy  = rho*grav*parm[3]/eta;
		kxz  = rho*grav*parm[4]/eta;
		kyz  = rho*grav*parm[5]/eta;

		// compute effective local permeability for relative permeability scaling
		k_eff = sqrt(parm[0]*parm[0] + parm[1]*parm[1] + parm[2]*parm[2] + parm[3]*parm[3] + parm[4]*parm[4] + parm[5]*parm[5]);
		K_eff = sqrt(kxx*kxx + kyy*kyy + kzz*kzz + kxy*kxy + kxz*kxz + kyz*kyz);

		// zero out element vector
		ierr = PetscMemzero(er, sizeof(er)); CHKERRQ(ierr);

		// integration point loop
		for(ip = 0; ip < 8; ip++)
		{
			// local coordinates
			lx1 = px1[ip];
			lx2 = px2[ip];
			lx3 = px3[ip];

			// get shape functions and derivatives
			ierr = getShape     (lx1, lx2, lx3, N);                                  CHKERRQ(ierr);
			ierr = getShapeDeriv(lx1, lx2, lx3, cX1, cX2, cX3, dX1, dX2, dX3, detJ); CHKERRQ(ierr);

			// get gradients, hydraulic head, and elevation head
			h1  = 0.0;
			h2  = 0.0;
			h3  = 0.0;
			ph  = 0.0;
			phn = 0.0;
			z   = zb;

			for(ii = 0; ii < 8; ii++)
			{
				h1  += dX1[ii]*eh [ii];
				h2  += dX2[ii]*eh [ii];
				h3  += dX3[ii]*eh [ii];
				ph  += N  [ii]*eh [ii];
				phn += N  [ii]*ehn[ii];
				z   += N  [ii]*cX3[ii];
			}

			z -= bz;

			// get porosity, fluid content, saturation, specific storage & relative permeability
			if(dt)
			{
				n        = parm[6];
				theta    = getFluidConent(jr, ph,   z, n, K_eff);
				theta_n  = getFluidConent(jr, phn,  z, n, K_eff);
				Sw       = theta/n;
				Ss       = rho*grav*(phi + n*beta);
				kr       = getRelPerm(jr, theta, n, k_eff, K_eff);
				A        = ((theta - theta_n) + Sw*Ss*(ph - phn))/dt;
			}

			if(kr < krmin) krmin = kr;

			// fluxes
			H1 = -kr*(h1*kxx + h2*kxy + h3*kxz);
			H2 = -kr*(h1*kxy + h2*kyy + h3*kyz);
			H3 = -kr*(h1*kxz + h2*kyz + h3*kzz);

			// compute element residual vector
			for(ii = 0; ii < 8; ii++)
			{
				h1 = dX1[ii];
				h2 = dX2[ii];
				h3 = dX3[ii];
				Ni = N  [ii];

				// update local residual
				er[ii] += (Ni*A - (H1*h1 + H2*h2 + H3*h3))*pwt[ip]*detJ;
			}
		}

		// update residual vector
		r[k]  [j]  [i]   += er[0];
		r[k]  [j]  [i+1] += er[1];
		r[k]  [j+1][i+1] += er[2];
		r[k]  [j+1][i]   += er[3];
		r[k+1][j]  [i]   += er[4];
		r[k+1][j]  [i+1] += er[5];
		r[k+1][j+1][i+1] += er[6];
		r[k+1][j+1][i]   += er[7];

		iter++;
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(fs->DA_COR, lh,  &h);  CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, lhn, &hn); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, lr,  &r);  CHKERRQ(ierr);

	// assemble residual
	LOCAL_TO_GLOBAL(fs->DA_COR, lr, f)

	// compute well fluxes and skip iteration triggered by solver
	if(getFlux)
	{
		ierr = JacResGetWellFlux(jr, f); CHKERRQ(ierr);

		// get observation well heads
		if(bc->nObsWells)
		{
			ierr = JacResGetObservationHeads(jr, 0); CHKERRQ(ierr);

		}

	}



	// constrain residual
	ierr = VecGetArray(f, &gr); CHKERRQ(ierr);

	for(ii = 0; ii < nbc; ii++) gr[idx[ii]] = 0.0;

	ierr = VecRestoreArray(f, &gr); CHKERRQ(ierr);

	// return local vectors
	ierr = DMRestoreLocalVector(fs->DA_COR, &lh);  CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(fs->DA_COR, &lhn); CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(fs->DA_COR, &lr);  CHKERRQ(ierr);


	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResGetWellFlux"
PetscErrorCode JacResGetWellFlux(JacRes *jr, Vec f)
{
	// get well fluxes

	FDSTAG      *fs;
	Scaling     *scal;
	vWell       *wells, *well;
	PetscScalar ***r, q[_max_num_wells_], *gq, t, dt;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, ii, nwells, istep;
	FILE        *fp;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs     = jr->fs;
	scal   = jr->scal;
	t      = jr->ts->time;
	dt     = jr->ts->dt;
	istep  = jr->ts->istep;
	nwells = jr->bc->nwells;
	wells  = jr->bc->wells;
	gq     = jr->bc->gq;

	// check whether constraint is activated
	if(!nwells) PetscFunctionReturn(0);

	// initialize well fluxes
	ierr = PetscMemzero(q,  (size_t)_max_num_wells_*sizeof(PetscScalar)); CHKERRQ(ierr);

	ierr = DMDAVecGetArray(fs->DA_COR, f, &r); CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP // calculate well fluxes from residual vector
	{
		for(ii = 0; ii < nwells; ii++)
		{
			well = wells + ii;

			// update well flux
			if(i == well->i && j == well->j && k >= well->k)
			{
				 q[ii] += r[k][j][i];
			}

		}
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(fs->DA_COR, f, &r); CHKERRQ(ierr);

	// exchange fluxes and store in gq
	ierr = MPI_Reduce(q, gq, (PetscMPIInt)nwells, MPIU_SCALAR, MPI_SUM, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);

	// write fluxes
	if(ISRankZero(PETSC_COMM_WORLD))
	{
		// open output file (write or update mode)
		if(!istep) fp = fopen("flux.dat", "w");
		else       fp = fopen("flux.dat", "a");

		if(fp == NULL) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Cannot open file flux.dat");

		// write header
		if(!istep)
		{
			fprintf(fp, "# t %s   ", scal->lbl_time);

			for(ii = 0; ii < nwells; ii++)
			{
				fprintf(fp, "q%lld %s   ", (LLD)(ii+1), scal->lbl_volumetric_rate);
			}

			fprintf(fp, "\n");
		}

		fprintf(fp, "%E   ", (t + dt)*scal->time);

		for(ii = 0; ii < nwells; ii++)
		{
			fprintf(fp, "%E   ", gq[ii]*scal->volumetric_rate);
		}

		fprintf(fp, "\n");

		// close file
		fclose(fp);
	}

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResGetObservationHeads"
PetscErrorCode JacResGetObservationHeads(JacRes *jr, PetscInt init)
{
	// get well fluxes

	FDSTAG      *fs;
	Scaling     *scal;
	obsWell     *wells, *well;
	PetscScalar ***p, cp, ch, wp[_max_num_obs_wells_], *gp, t, dt, sweight, z, bz;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, ii, nwells;
	FILE        *fp;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs      = jr->fs;
	scal    = jr->scal;
	nwells  = jr->bc->nObsWells;
	wells   = jr->bc->obsWells;
	gp      = jr->bc->gp;
	sweight = jr->grav*jr->rho;
	t       = jr->ts->time;

	if(init) dt = 0;
	else  	 dt = jr->ts->dt;

	// check whether constraint is activated
	if(!nwells) PetscFunctionReturn(0);

	// initialize well heads
	ierr = PetscMemzero(wp,  (size_t)_max_num_wells_*sizeof(PetscScalar)); CHKERRQ(ierr);

	// initialize all heads with DBL MAX
	for(ii = 0; ii < nwells; ii++) wp[ii] = DBL_MAX;

	// access pressure array
	ierr = DMDAVecGetArray(fs->DA_COR, jr->pv, &p); CHKERRQ(ierr);

	// get global box bounds
	ierr = FDSTAGGetGlobalBox(fs, NULL, NULL, &bz, NULL, NULL, NULL); CHKERRQ(ierr);

	// get processor dimensions
	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP // calculate well fluxes from residual vector
	{
		for(ii = 0; ii < nwells; ii++)
		{
			well = wells + ii;

			// update well heads (minimum well head, if overhangs are present)
			if(i == well->i && j == well->j)
			{
				// get current pressure
				cp = p[k][j][i];

				// checl, if pressure is negative
				if(cp <= 0)
				{
					// get elevation head
					z = COORD_NODE(k, sz, fs->dsz) - bz;

					// compute head
					ch = (cp/sweight) + z;

					// assign current head, only if lower than previous
					if(ch <= wp[ii]) wp[ii] = ch;
				}
			}

		}
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(fs->DA_COR, jr->pv, &p); CHKERRQ(ierr);

	// exchange heads and store in gp
	ierr = MPI_Reduce(wp, gp, (PetscMPIInt)nwells, MPIU_SCALAR, MPI_MIN, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);

	// write fluxes
	if(ISRankZero(PETSC_COMM_WORLD))
	{
		// open output file (write or update mode)
		if(init) fp = fopen("observationWells.dat", "w");
		else     fp = fopen("observationWells.dat", "a");

		if(fp == NULL) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Cannot open file observationWells.dat");

		// write header
		if(init)
		{
			fprintf(fp, "# t %s   ", scal->lbl_time);

			for(ii = 0; ii < nwells; ii++)
			{
				well = wells + ii;
				fprintf(fp, "%s %s   ", well->name, scal->lbl_length);
			}

			fprintf(fp, "\n");
		}

		fprintf(fp, "%E   ", (t + dt)*scal->time);

		for(ii = 0; ii < nwells; ii++)
		{
			fprintf(fp, "%E   ", gp[ii]*scal->length);
		}

		fprintf(fp, "\n");

		// close file
		fclose(fp);
	}

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResCheckSteadyState"
PetscErrorCode JacResCheckSteadyState(JacRes *jr, Vec x)
{
	PetscScalar h_norm, diff_norm, incr, tol, dt;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	dt  = jr->ts->dt;

	if(!dt) PetscFunctionReturn(0);

	if(!jr->check_ss) PetscFunctionReturn(0);

	tol = jr->check_ss_tol;

	// compute norms and diff
	ierr = VecNorm(x,  NORM_2, &h_norm);  CHKERRQ(ierr);
	ierr = VecWAXPY(jr->hdiff, -1, x, jr->hn);

	ierr = VecNorm(jr->hdiff, NORM_2, &diff_norm); CHKERRQ(ierr);

	incr = diff_norm/h_norm;

	if(incr < tol) // HARD CODED RESIDUAL COMPARISON TODO: Change, based on solver residual
	{
		// stop simulation by setting timestep parameters
		PetscPrintf(PETSC_COMM_WORLD, "======================== REACHED STEADY-STATE ============================\n");
		PetscPrintf(PETSC_COMM_WORLD, "--------------------------------------------------------------------------\n");

		jr->ts->dt    = 0;
		jr->ts->istep = 1;
	}

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResGetDarcyVel"
PetscErrorCode JacResGetDarcyVel(JacRes *jr, Vec x)
{
	FDSTAG      *fs;
	Vec         lh, lx, ly, lz, lw, gw;
	PetscScalar eh[8], lvel[3], *gvel;
	PetscScalar zb, z, ph, dt, n, theta, kr, grav, rho, k_eff, K_eff;
	PetscScalar cX1[8], cX2[8], cX3[8], dx, dy, dz;
	PetscScalar dX1[8], dX2[8], dX3[8], N[8], H1, H2, H3, h1, h2, h3;
	PetscScalar px1[8], px2[8], px3[8], pwt[8], lx1, lx2, lx3, detJ, wt;
	PetscScalar *parm, eta, kxx, kyy, kzz, kxy, kxz, kyz;
   	PetscScalar bx, by, bz, ex, ey, ez, vol;
	PetscInt    idxi[8], idxj[8], idxk[8];
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, iter, ip, ii, numPar;
	PetscScalar ***h,  ***vx,  ***vy,  ***vz, ***w;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs     = jr->fs;
	gvel   = jr->gvel;
	grav   = jr->grav;
	eta    = jr->eta;
	rho    = jr->rho;
	numPar = jr->numPar;
	dt     = jr->ts->dt;

	// initialize steady-state simulation
	if(!dt)
	{
		kr = 1.0;
	}

	// setup local vectors
	ierr = DMGetLocalVector (fs->DA_COR, &lh); CHKERRQ(ierr);
	ierr = DMGetLocalVector (fs->DA_COR, &lx); CHKERRQ(ierr);
	ierr = DMGetLocalVector (fs->DA_COR, &ly); CHKERRQ(ierr);
	ierr = DMGetLocalVector (fs->DA_COR, &lz); CHKERRQ(ierr);
	ierr = DMGetLocalVector (fs->DA_COR, &lw); CHKERRQ(ierr);
	ierr = DMGetGlobalVector(fs->DA_COR, &gw); CHKERRQ(ierr);

	GLOBAL_TO_LOCAL(fs->DA_COR, x, lh)

	ierr = VecZeroEntries(lx); CHKERRQ(ierr);
	ierr = VecZeroEntries(ly); CHKERRQ(ierr);
	ierr = VecZeroEntries(lz); CHKERRQ(ierr);
	ierr = VecZeroEntries(lw); CHKERRQ(ierr);

	ierr = DMDAVecGetArray(fs->DA_COR, lh, &h);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, lx, &vx); CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, ly, &vy); CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, lz, &vz); CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, lw, &w);  CHKERRQ(ierr);

	// get quadrature
	ierr = getQuad(px1, px2, px3, pwt); CHKERRQ(ierr);

	// get global box bounds
	ierr = FDSTAGGetGlobalBox(fs, &bx, &by, &bz, &ex, &ey, &ez); CHKERRQ(ierr);

	//--------------------------------------------
	// compute Darcy velocities & volume integrals
	//--------------------------------------------

	// integrate local velocity
	lvel[0] = 0.0;
	lvel[1] = 0.0;
	lvel[2] = 0.0;

	ierr = DMDAGetCorners(fs->DA_CEN, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	iter = 0;

	START_STD_LOOP
	{
		// access current head
		eh[0] = h[k]  [j]  [i];
		eh[1] = h[k]  [j]  [i+1];
		eh[2] = h[k]  [j+1][i+1];
		eh[3] = h[k]  [j+1][i];
		eh[4] = h[k+1][j]  [i];
		eh[5] = h[k+1][j]  [i+1];
		eh[6] = h[k+1][j+1][i+1];
		eh[7] = h[k+1][j+1][i];

		// get mesh steps
		dx = SIZE_CELL (i, sx, fs->dsx);
		dy = SIZE_CELL (j, sy, fs->dsy);
		dz = SIZE_CELL (k, sz, fs->dsz);
		zb = COORD_NODE(k, sz, fs->dsz);

		// get corner coordinates of cell
		cX1[0] = 0.0;  cX2[0] = 0.0;  cX3[0] = 0.0;
		cX1[1] = dx;   cX2[1] = 0.0;  cX3[1] = 0.0;
		cX1[2] = dx;   cX2[2] = dy;   cX3[2] = 0.0;
		cX1[3] = 0.0;  cX2[3] = dy;   cX3[3] = 0.0;
		cX1[4] = 0.0;  cX2[4] = 0.0;  cX3[4] = dz;
		cX1[5] = dx;   cX2[5] = 0.0;  cX3[5] = dz;
		cX1[6] = dx;   cX2[6] = dy;   cX3[6] = dz;
		cX1[7] = 0.0;  cX2[7] = dy;   cX3[7] = dz;

		// get hydraulic conductivities
		parm  = jr->parm + numPar*iter;
		kxx   = rho*grav*parm[0]/eta;
		kyy   = rho*grav*parm[1]/eta;
		kzz   = rho*grav*parm[2]/eta;
		kxy   = rho*grav*parm[3]/eta;
		kxz   = rho*grav*parm[4]/eta;
		kyz   = rho*grav*parm[5]/eta;

		// compute effective local permeability and conductivity for relative permeability scaling
		k_eff = sqrt(parm[0]*parm[0] + parm[1]*parm[1] + parm[2]*parm[2] + parm[3]*parm[3] + parm[4]*parm[4] + parm[5]*parm[5]);
		K_eff = sqrt(kxx*kxx + kyy*kyy + kzz*kzz + kxy*kxy + kxz*kxz + kyz*kyz);

		// update integration point target indices
		idxi[0] = i;   idxj[0] = j;   idxk[0] = k;
		idxi[1] = i+1; idxj[1] = j;   idxk[1] = k;
		idxi[2] = i+1; idxj[2] = j+1; idxk[2] = k;
		idxi[3] = i;   idxj[3] = j+1; idxk[3] = k;
		idxi[4] = i;   idxj[4] = j;   idxk[4] = k+1;
		idxi[5] = i+1; idxj[5] = j;   idxk[5] = k+1;
	    idxi[6] = i+1; idxj[6] = j+1; idxk[6] = k+1;
	    idxi[7] = i;   idxj[7] = j+1; idxk[7] = k+1;

		// integration point loop
		for(ip = 0; ip < 8; ip++)
		{
			// local coordinates
			lx1 = px1[ip];
			lx2 = px2[ip];
			lx3 = px3[ip];

			// get shape functions and derivatives
			ierr = getShape     (lx1, lx2, lx3, N);                                  CHKERRQ(ierr);
			ierr = getShapeDeriv(lx1, lx2, lx3, cX1, cX2, cX3, dX1, dX2, dX3, detJ); CHKERRQ(ierr);

			// get gradients, hydraulic head, and elevation head
			h1  = 0.0;
			h2  = 0.0;
			h3  = 0.0;
			ph  = 0.0;
			z   = zb;

			for(ii = 0; ii < 8; ii++)
			{
				h1  += dX1[ii]*eh [ii];
				h2  += dX2[ii]*eh [ii];
				h3  += dX3[ii]*eh [ii];
				ph  += N  [ii]*eh [ii];
				z   += N  [ii]*cX3[ii];
			}

			z -= bz;

			// get porosity, fluid content & relative permeability
			if(dt)
			{
				n     = parm[6];
				theta = getFluidConent(jr, ph, z, n, K_eff);
				kr    = getRelPerm(jr, theta, n, k_eff, K_eff);
			}

			// fluxes
			H1 = -kr*(h1*kxx + h2*kxy + h3*kxz);
			H2 = -kr*(h1*kxy + h2*kyy + h3*kyz);
			H3 = -kr*(h1*kxz + h2*kyz + h3*kzz);

			// get integration weight
			wt = pwt[ip]*detJ;

			// update integrals
			lvel[0] += PetscAbsScalar(H1)*wt;
			lvel[1] += PetscAbsScalar(H2)*wt;
			lvel[2] += PetscAbsScalar(H3)*wt;

			vx[ idxk[ip] ][ idxj[ip] ][ idxi[ip] ] += H1*wt;
			vy[ idxk[ip] ][ idxj[ip] ][ idxi[ip] ] += H2*wt;
			vz[ idxk[ip] ][ idxj[ip] ][ idxi[ip] ] += H3*wt;
			w [ idxk[ip] ][ idxj[ip] ][ idxi[ip] ] += wt;
		}

		iter++;
	}
	END_STD_LOOP

	ierr = DMDAVecRestoreArray(fs->DA_COR, lh, &h);  CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, lx, &vx); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, ly, &vy); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, lz, &vz); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, lw, &w);  CHKERRQ(ierr);

	// assemble global vectors
	LOCAL_TO_GLOBAL(fs->DA_COR, lx, jr->vx)
	LOCAL_TO_GLOBAL(fs->DA_COR, ly, jr->vy)
	LOCAL_TO_GLOBAL(fs->DA_COR, lz, jr->vz)
	LOCAL_TO_GLOBAL(fs->DA_COR, lw, gw)

	// normalize velocities
	ierr = VecPointwiseDivide(jr->vx, jr->vx, gw); CHKERRQ(ierr);
	ierr = VecPointwiseDivide(jr->vy, jr->vy, gw); CHKERRQ(ierr);
	ierr = VecPointwiseDivide(jr->vz, jr->vz, gw); CHKERRQ(ierr);

	// compute global integrals
	if(ISParallel(PETSC_COMM_WORLD))
	{
		ierr = MPI_Allreduce(lvel, gvel, 3, MPIU_SCALAR, MPI_SUM, PETSC_COMM_WORLD); CHKERRQ(ierr);
	}
	else
	{
		gvel[0] = lvel[0];
		gvel[1] = lvel[1];
		gvel[2] = lvel[2];
	}

	// compute global volume
	vol = (ex - bx)*(ey - by)*(ez - bz);

	// normalize velocity integrals by global volume
	gvel[0] /= vol;
	gvel[1] /= vol;
	gvel[2] /= vol;

	// clear work vectors
	ierr = DMRestoreLocalVector (fs->DA_COR, &lh); CHKERRQ(ierr);
	ierr = DMRestoreLocalVector (fs->DA_COR, &lx); CHKERRQ(ierr);
	ierr = DMRestoreLocalVector (fs->DA_COR, &ly); CHKERRQ(ierr);
	ierr = DMRestoreLocalVector (fs->DA_COR, &lz); CHKERRQ(ierr);
	ierr = DMRestoreLocalVector (fs->DA_COR, &lw); CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(fs->DA_COR, &gw); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResGetPermea"
PetscErrorCode JacResGetPermea(JacRes *jr, char *outfile)
{
	FILE        *db;
	FDSTAG      *fs;
	BCCtx       *bc;
	Scaling     *scal;
	Discret1D   *ds;
	char        path[_str_len_], dir[_str_len_];
	PetscScalar *gvel, perm[3];
	PetscScalar pbot, ptop, cbot, ctop, grad, eta, sweight;

	PetscFunctionBegin;

	// check activation
	if(!jr->getPermea) PetscFunctionReturn(0);

	// access context variables
	fs      = jr->fs;
	bc      = jr->bc;
	scal    = jr->scal;
	pbot    = bc->pbot;
	ptop    = bc->ptop;
	eta     = jr->eta;
	gvel    = jr->gvel;
	sweight = jr->grav*jr->rho;

	// get pressure head gradient
	if(bc->pdir == 1) { ds = &fs->dsx; sprintf(dir, "x"); }
	if(bc->pdir == 2) { ds = &fs->dsy; sprintf(dir, "y"); }
	if(bc->pdir == 3) { ds = &fs->dsz; sprintf(dir, "z"); }

	cbot = ds->gcrdbeg;
	ctop = ds->gcrdend;
	grad = (ptop - pbot)/(ctop - cbot);
	grad = PetscAbsScalar(grad);

	// compute permeability
	perm[0] = gvel[0]*eta/grad/sweight;
	perm[1] = gvel[1]*eta/grad/sweight;
	perm[2] = gvel[2]*eta/grad/sweight;

	// output to the file
	if(ISRankZero(PETSC_COMM_WORLD))
	{
		memset(path, 0, _str_len_);
		strcpy(path, outfile);
		strcat(path, ".darcy.dat");

		db = fopen(path, "w");

		fprintf(db, "# =====================================\n");
		fprintf(db, "# EFFECTIVE PERMEABILITY TENSOR COLUMN:\n");
		fprintf(db, "# =====================================\n");
		fprintf(db, "Kx%s: %E %s\n", dir, perm[0]*scal->area_si, scal->lbl_area_si);
		fprintf(db, "Ky%s: %E %s\n", dir, perm[1]*scal->area_si, scal->lbl_area_si);
		fprintf(db, "Kz%s: %E %s\n", dir, perm[2]*scal->area_si, scal->lbl_area_si);

		fclose(db);
	}

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResGetPress"
PetscErrorCode JacResGetPress(JacRes *jr, Vec x)
{
	// compute pressure from hydraulic head
	FDSTAG      *fs;
	PetscScalar ***p, ***h, bz, z, cf, sweight;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs      = jr->fs;
	sweight = jr->grav*jr->rho;

	// check gravity activation
	if(jr->actGrav) cf = 1.0;
	else            cf = 0.0;

	// get global box bounds
	ierr = FDSTAGGetGlobalBox(fs, NULL, NULL, &bz, NULL, NULL, NULL); CHKERRQ(ierr);

	ierr = DMDAVecGetArray    (fs->DA_COR, jr->pv, &p); CHKERRQ(ierr);
	ierr = DMDAVecGetArrayRead(fs->DA_COR, x,      &h); CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP
	{
		// get elevation head
		z = COORD_NODE(k, sz, fs->dsz) - bz;

		// compute pressure
		p[k][j][i] = (h[k][j][i] - cf*z)*sweight;
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray    (fs->DA_COR, jr->pv, &p); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayRead(fs->DA_COR, x,      &h); CHKERRQ(ierr);


	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResTruncateHead"
PetscErrorCode JacResTruncateHead(JacRes *jr, Vec x)
{
	// compute pressure from hydraulic head
	FDSTAG      *fs;
	FreeSurf    *surf;
	PetscScalar ***h, h0, bz, nh;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz;


	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs      = jr->fs;
	surf    = jr->surf;

	// access initial free surface level
	ierr = FDSTAGGetGlobalBox(fs, NULL, NULL, &bz, NULL, NULL, NULL); CHKERRQ(ierr);
	h0   = surf->level - bz;

	ierr = DMDAVecGetArrayRead(fs->DA_COR, x,      &h); CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP
	{
		nh = h[k][j][i];

		if( nh > h0)
		{
			h[k][j][i] = h0;
		}
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArrayRead(fs->DA_COR, x,      &h); CHKERRQ(ierr);


	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResGetSaturation"
PetscErrorCode JacResGetSaturation(JacRes *jr, Vec x)
{
	// initialize saturation in cell by integrating from intialized head
	Vec         lh;
	FDSTAG      *fs;
	FreeSurf    *surf;
	PetscScalar ***h, *parm, *sat;
	PetscScalar grav, eta, rho, K_eff, kxx, kyy, kzz, kxy, kxz, kyz;
	PetscScalar eh[8];
	PetscScalar bz, zb, z, ph, n, theta, ls;
	PetscScalar cX1[8], cX2[8], cX3[8], dX1[8], dX2[8], dX3[8], dx, dy, dz;
	PetscScalar px1[8], px2[8], px3[8], N[8], pwt[8], lx1, lx2, lx3, detJ;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz,  ip, ii, iter, numPar;
	PetscScalar g_wvol, l_wvol, cell_vol;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs      = jr->fs;
	surf    = jr->surf;
	sat     = jr->sat;
	grav    = jr->grav;
	eta     = jr->eta;
	rho     = jr->rho;
	g_wvol  = jr->g_wvol;

	// get number of hydraulic parameters
	numPar = jr->numPar;

	// check whether free surface is activated
	if(!surf->UseFreeSurf) PetscFunctionReturn(0);

	// get global box bounds
	ierr = FDSTAGGetGlobalBox(fs, NULL, NULL, &bz, NULL, NULL, NULL); CHKERRQ(ierr);

	// setup local head vector for satuartion integration
	ierr = DMGetLocalVector (fs->DA_COR, &lh); CHKERRQ(ierr);

	// scater global head vector (initalized previously) to local vector
	GLOBAL_TO_LOCAL(fs->DA_COR, x, lh)

	// get local head vector
	ierr = DMDAVecGetArray(fs->DA_COR, lh, &h);  CHKERRQ(ierr);

	// get processor corners
	ierr = DMDAGetCorners(fs->DA_CEN, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	// get quadrature for integration
	ierr = getQuad(px1, px2, px3, pwt); CHKERRQ(ierr);

	// compute local water volume
	l_wvol = 0.0;

	// Overwrite global water volume of previous timestep with current one, before new computation, ONLY AFTER FIRST TIMESTEP
	if(! jr->ts->istep == 0)
	{
		jr->g_wvol_n = g_wvol;
	}

	iter = 0;

	START_STD_LOOP
	{

		// access initalized head
		eh[0] = h[k]  [j]  [i];
		eh[1] = h[k]  [j]  [i+1];
		eh[2] = h[k]  [j+1][i+1];
		eh[3] = h[k]  [j+1][i];
		eh[4] = h[k+1][j]  [i];
		eh[5] = h[k+1][j]  [i+1];
		eh[6] = h[k+1][j+1][i+1];
		eh[7] = h[k+1][j+1][i];

		// get mesh steps
		dx = SIZE_CELL (i, sx, fs->dsx);
		dy = SIZE_CELL (j, sy, fs->dsy);
		dz = SIZE_CELL (k, sz, fs->dsz);
		zb = COORD_NODE(k, sz, fs->dsz);

		// compute cell volume
		cell_vol = dx*dy*dz;

		// get corner coordinates of cell
		cX1[0] = 0.0;  cX2[0] = 0.0;  cX3[0] = 0.0;
		cX1[1] = dx;   cX2[1] = 0.0;  cX3[1] = 0.0;
		cX1[2] = dx;   cX2[2] = dy;   cX3[2] = 0.0;
		cX1[3] = 0.0;  cX2[3] = dy;   cX3[3] = 0.0;
		cX1[4] = 0.0;  cX2[4] = 0.0;  cX3[4] = dz;
		cX1[5] = dx;   cX2[5] = 0.0;  cX3[5] = dz;
		cX1[6] = dx;   cX2[6] = dy;   cX3[6] = dz;
		cX1[7] = 0.0;  cX2[7] = dy;   cX3[7] = dz;

		// get hydraulic conductivities
		parm  = jr->parm + numPar*iter;

		// get porosity, permeabiltiy and fluid content
		kxx   = rho*grav*parm[0]/eta;
		kyy   = rho*grav*parm[1]/eta;
		kzz   = rho*grav*parm[2]/eta;
		kxy   = rho*grav*parm[3]/eta;
		kxz   = rho*grav*parm[4]/eta;
		kyz   = rho*grav*parm[5]/eta;
		n     = parm[6];

		// compute effective local conductivity for relative permeability scaling
		K_eff = sqrt(kxx*kxx + kyy*kyy + kzz*kzz + kxy*kxy + kxz*kxz + kyz*kyz);

		// initialize local saturation
		ls    = 0.0;

		// integration point loop
		for(ip = 0; ip < 8; ip++)
		{
			// local coordinates
			lx1 = px1[ip];
			lx2 = px2[ip];
			lx3 = px3[ip];

			// get shape functions and derivatives
			ierr = getShape(lx1, lx2, lx3, N);  								      CHKERRQ(ierr);
			ierr = getShapeDeriv(lx1, lx2, lx3, cX1, cX2, cX3,  dX1, dX2, dX3, detJ); CHKERRQ(ierr);

			// get hydraulic head, and elevation head
			ph  = 0.0;
			z   = zb;

			for(ii = 0; ii < 8; ii++)
			{
				ph  += N  [ii]*eh [ii];
				z   += N  [ii]*cX3[ii];
			}

			z -= bz;

			theta = getFluidConent(jr, ph, z, n, K_eff);

			// get integration weight
			//wt = pwt[ip]*detJ;

			// update saturation
			ls += (theta/n);
		}

		// store saturation and divide by 8 to compute average
		sat[iter] = ls/8;

		// update local water volume
		l_wvol += n*(ls/8)*cell_vol;

		// update cell counter
		iter++;

	}
	END_STD_LOOP

	// compute global integral of water volume
	if(ISParallel(PETSC_COMM_WORLD))
	{
		ierr = MPI_Allreduce(&l_wvol, &g_wvol, 1, MPIU_SCALAR, MPI_SUM, PETSC_COMM_WORLD); CHKERRQ(ierr);
	}
	else
	{
		g_wvol = l_wvol;
	}

	// restore access
	ierr = DMDAVecRestoreArray(fs->DA_COR, lh, &h); CHKERRQ(ierr);

	// restore global water volume value
	jr->g_wvol = g_wvol;

	// restore global water volume value of previous time step with the same, ONLY FOR FIRST TIMESTEP
	if(jr->ts->istep == 0)
	{
		jr->g_wvol_n = g_wvol;
	}

	// clear work vector
	ierr = DMRestoreLocalVector(fs->DA_COR, &lh);   CHKERRQ(ierr);

	PetscFunctionReturn(0);


}

//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResUpdatePoroPerm"
PetscErrorCode JacResUpdatePoroPerm(JacRes *jr)
{
	Scaling       *scal;
	PetscScalar    time_sc, length_sc, velo_sc;
	FDSTAG        *fs;
	Vec            lx, ly, lz;
	PetscScalar ***vx, ***vy, ***vz;
	PetscScalar    evx[8], evy[8], evz[8];
	PetscScalar   *parm, *courant, *hpe_rf;
	PetscInt       numPar, iter;
	PetscInt       i, j, k, nx, ny, nz, sx, sy, sz;
	PetscInt       vi;
	PetscScalar    vxa, vya, vza, v_eff;
	PetscScalar    dx, dt, lc, rf, rf_pow;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs        = jr->fs;
	numPar    = jr->numPar;
	courant   = jr->courant;
	hpe_rf    = jr->hpe_rf;
	scal      = jr->ts->scal;
	time_sc   = scal->time;
	length_sc = scal->length;
	velo_sc   = scal->velocity;

	// check whether free surface is activated
	if(!jr->hpe_use) PetscFunctionReturn(0);

	// setup local vectors
	ierr = DMGetLocalVector(fs->DA_COR, &lx); CHKERRQ(ierr);
	ierr = DMGetLocalVector(fs->DA_COR, &ly); CHKERRQ(ierr);
	ierr = DMGetLocalVector(fs->DA_COR, &lz); CHKERRQ(ierr);

	GLOBAL_TO_LOCAL(fs->DA_COR, jr->vx, lx)
	GLOBAL_TO_LOCAL(fs->DA_COR, jr->vy, ly)
	GLOBAL_TO_LOCAL(fs->DA_COR, jr->vz, lz)

	// access velocity vectors on nodes
	ierr = DMDAVecGetArray(fs->DA_COR, lx, &vx); CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, ly, &vy); CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, lz, &vz); CHKERRQ(ierr);

	// get processor corners
	ierr = DMDAGetCorners(fs->DA_CEN, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	// initialize cell counter
	iter = 0;

	START_STD_LOOP
	{

		// access stored darcy velocity on nodes (x)
		evx[0] = vx[k]  [j]  [i];
		evx[1] = vx[k]  [j]  [i+1];
		evx[2] = vx[k]  [j+1][i+1];
		evx[3] = vx[k]  [j+1][i];
		evx[4] = vx[k+1][j]  [i];
		evx[5] = vx[k+1][j]  [i+1];
		evx[6] = vx[k+1][j+1][i+1];
		evx[7] = vx[k+1][j+1][i];

		// access stored darcy velocity on nodes (y)
		evy[0] = vy[k]  [j]  [i];
		evy[1] = vy[k]  [j]  [i+1];
		evy[2] = vy[k]  [j+1][i+1];
		evy[3] = vy[k]  [j+1][i];
		evy[4] = vy[k+1][j]  [i];
		evy[5] = vy[k+1][j]  [i+1];
		evy[6] = vy[k+1][j+1][i+1];
		evy[7] = vy[k+1][j+1][i];

		// access stored darcy velocity on nodes (z)
		evz[0] = vz[k]  [j]  [i];
		evz[1] = vz[k]  [j]  [i+1];
		evz[2] = vz[k]  [j+1][i+1];
		evz[3] = vz[k]  [j+1][i];
		evz[4] = vz[k+1][j]  [i];
		evz[5] = vz[k+1][j]  [i+1];
		evz[6] = vz[k+1][j+1][i+1];
		evz[7] = vz[k+1][j+1][i];

		// initialize average values
		vxa = 0;
		vya = 0;
		vza = 0;

		// sum up velocities per cell
		for(vi = 0; vi < 8; vi++)
		{
			vxa+=evx[vi];
			vya+=evy[vi];
			vza+=evz[vi];
		}

		// divide by 8 to compute average
		vxa = vxa/8;
		vya = vya/8;
		vza = vza/8;

		// redimensionalise to actual velocities
		vxa *= velo_sc;
		vya *= velo_sc;
		vza *= velo_sc;

		// compute the norm of the average velocity vector of a cell
		v_eff = sqrt(vxa*vxa + vya*vya + vza*vza);

		// compute local courant number per cell "lc" (assuming rectangular cell!!!)
		dx = SIZE_CELL(i, sx, fs->dsx)*length_sc;
		dt = jr->ts->dt*time_sc;
		lc = v_eff*(dt/dx);

		// get the recution factor based on the local courant number
		rf = getReductionFactor(jr, lc);

		// access hydraulic properties
		parm = jr->parm + numPar*iter;

		// update hydraulic properties (permeabilities)
		parm[0] -= parm[0]*rf;
		parm[1] -= parm[1]*rf;
		parm[2] -= parm[2]*rf;
		parm[3] -= parm[3]*rf;
		parm[4] -= parm[4]*rf;
		parm[5] -= parm[5]*rf;

		// use porosity^3=permeability relation
		rf_pow = pow(rf,3);

		// porosity
		parm[6] -= parm[6]*rf_pow;

		// store reduction factor and local courant number for output
		courant[iter] = lc;
		hpe_rf [iter] = rf;

		// update cell counter
		iter++;
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(fs->DA_COR, lx, &vx); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, ly, &vy); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, lz, &vz); CHKERRQ(ierr);

	// clear work vectors
	ierr = DMRestoreLocalVector (fs->DA_COR, &lx); CHKERRQ(ierr);
	ierr = DMRestoreLocalVector (fs->DA_COR, &ly); CHKERRQ(ierr);
	ierr = DMRestoreLocalVector (fs->DA_COR, &lz); CHKERRQ(ierr);

	// recompute effective permeability
	ierr = JacResGetEffPerm(jr); CHKERRQ(ierr);

	PetscFunctionReturn(0);

}
//---------------------------------------------------------------------------
// HPE function
//---------------------------------------------------------------------------
PetscScalar getReductionFactor(JacRes *jr, PetscScalar c)
{
	// compute reduction factor based on smoothed piecwise linear function approximator
	// matlab code: @(t,beta)A/Tr*log(((1+exp(beta*(t-T50+Tr/2)))./(1+exp(beta*(t-T50-Tr/2)))).^(1/beta));
    // piece-wise linear step function:  x = @(t)(t > T50-Tr/2 & t < T50+Tr/2).*(A/Tr.*(t-T50)+A/2)+(t >= T50+Tr/2).*A;
	PetscScalar hpe_a, hpe_r, hpe_beta, hpe_r50;
	PetscScalar c_log, RF;
	PetscScalar exp_pls, exp_min, hpe_inner;

	// access context variables
	hpe_a      = jr->hpe_a;
	hpe_r      = jr->hpe_r;
	hpe_beta   = jr->hpe_beta;
	hpe_r50    = jr->hpe_r50;

	// compute function parts
	c_log     = log(c);
	exp_pls   = exp(hpe_beta*(c_log-hpe_r50+hpe_r/2));
	exp_min   = exp(hpe_beta*(c_log-hpe_r50-hpe_r/2));
	hpe_inner = pow((1+exp_pls)/(1+exp_min),(1/hpe_beta));

	// evaluate reduction factor
	RF = (hpe_a/hpe_r)*log(hpe_inner);

	return RF;
}
//---------------------------------------------------------------------------
// Van Genuchten functions
//---------------------------------------------------------------------------
PetscScalar getFluidConent(JacRes *jr, PetscScalar h, PetscScalar z, PetscScalar n, PetscScalar K_eff)
{
	// compute Van-Genuchten fluid content
	Scaling    *scal;
	PetscScalar velo_sc;
	PetscScalar psi, alpha, l, m, theta_r, theta_s, theta;
	PetscScalar vg_dyn_a_p, vg_dyn_a_b, vg_dyn_a_n, vg_dyn_l_p, vg_dyn_l_b, vg_dyn_l_n;

	psi       = h - z;
	theta_r   = jr->theta_r;
	theta_s   = n;
	scal      = jr->ts->scal;
	velo_sc   = scal->velocity;

	// check for dynamic or static vg parameter assignment
	if(jr->vg_dyn_use)
	{
		// redimensionalize K_eff
		K_eff *= velo_sc;

		vg_dyn_a_p = jr->vg_dyn_a_p;
		vg_dyn_a_b = jr->vg_dyn_a_b;
		vg_dyn_a_n = jr->vg_dyn_a_n;
		vg_dyn_l_p = jr->vg_dyn_l_p;
		vg_dyn_l_b = jr->vg_dyn_l_b;
		vg_dyn_l_n = jr->vg_dyn_l_n;

		alpha   = vg_dyn_a_b + vg_dyn_a_p*pow(K_eff, vg_dyn_a_n);
		l       = vg_dyn_l_b + vg_dyn_l_p*pow(K_eff, vg_dyn_l_n);
		m       = 1.0 - 1.0/l;

		// dynamic VG_parm parametrization assumes alpha to be in [1/kPa] but we need it in [1/m]
		// conversion: 1 Pa = 0.00010199773339984 = 0.000102 m

	}
	else
	{
		alpha   = jr->alpha;
		l       = jr->l;
		m       = 1.0 - 1.0/l;
	}

	// calculate theta
	if(psi < 0.0)
	{
		theta = (theta_s - theta_r)*pow(1.0 + pow(-alpha*psi, l), -m) + theta_r;
	}
	else
	{
		theta = theta_s;
	}

	return theta;
}
//---------------------------------------------------------------------------
PetscScalar getRelPerm(JacRes *jr, PetscScalar theta, PetscScalar n, PetscScalar k_eff,  PetscScalar K_eff)
{
	// compute Van-Genuchten relative permeability
	Scaling    *scal;
	PetscScalar velo_sc;
	PetscScalar l, m, theta_r, theta_s, H, B, kr, krr;
	PetscScalar vg_dyn_l_p, vg_dyn_l_b, vg_dyn_l_n;

	theta_r   = jr->theta_r;
	theta_s   = n;
	scal      = jr->ts->scal;
	velo_sc   = scal->velocity;

	// check for dynamic or static vg parameter assignment
	if(jr->vg_dyn_use)
	{
		// redimensionalize K_eff
		K_eff *= velo_sc;

		vg_dyn_l_p = jr->vg_dyn_l_p;
		vg_dyn_l_b = jr->vg_dyn_l_b;
		vg_dyn_l_n = jr->vg_dyn_l_n;

		l       = vg_dyn_l_b + vg_dyn_l_p*pow(K_eff, vg_dyn_l_n);
		m       = 1.0 - 1.0/l;
	}
	else
	{
		l       = jr->l;
		m       = 1.0 - 1.0/l;
	}

	// compute residual kr
	if(jr->k_ar)
	{
		// compute krr as a ratio of prescribed absolute min permeability value to local effective permeability
		krr     = jr->k_ar/k_eff;

		// if local permeability is smaller than abrolute residual, put it to one
		if(krr > 1)
		{
			krr = 1;
		}

	}
	else
	{
		krr     = jr->krr;
	}

	if(theta < theta_s)
	{
		H  = (theta - theta_r)/(theta_s - theta_r);
		B  = 1.0 - pow(1.0 - pow(H, 1.0/m), m);
		kr = sqrt(H)*B*B;
		kr = kr*(1.0 - krr) + krr;
	}
	else
	{
		kr = 1.0;
	}

	return kr;
}
//---------------------------------------------------------------------------
// SNES FUNCTIONS
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "FormResidual"
PetscErrorCode FormResidual(SNES snes, Vec x, Vec f, void *ctx)
{
	JacRes *jr;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	if(snes) snes = NULL;

	jr = (JacRes*)ctx;

	ierr = JacResGetRes(jr, x, f, 0); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "FormJacobian"
PetscErrorCode FormJacobian(SNES snes, Vec x, Mat Amat, Mat Pmat, void *ctx)
{
	JacRes *jr;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	if(snes) snes = NULL;

	jr = (JacRes*)ctx;

	ierr = JacResGetJac(jr, x, Pmat); CHKERRQ(ierr);

	// finalize assembly
	ierr = MatAssemblyBegin(Amat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd  (Amat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------


/*
	// apply borehole sources
	if(bc->nholes)
	{
		// access topography vector
		ierr = DMDAVecGetArray(surf->DA_SURF, surf->gtopo, &topo);  CHKERRQ(ierr);

		// get local index bounds
		ierr = DMDAGetCorners(fs->DA_COR, &sx,  &sy,  NULL, &nx,  &ny,  NULL); CHKERRQ(ierr);
		ierr = DMDAGetCorners(fs->DA_CEN, NULL, NULL, &sz,  NULL, NULL, &nz);  CHKERRQ(ierr);

		// scan boreholes
		for(ii = 0; ii < bc->nholes; ii++)
		{
			// get borehole bottom index
			iholes = bc->iholes + 3*ii;
			i      = iholes[0];
			j      = iholes[1];
			kk     = iholes[2];

			// check for local borehole
			if(i >= sx && i < sx+nx
			&& j >= sy && j < sy+ny)
			{
				// get flow rate per unit length
				q = bc->qholes[ii];

				// get surface elevation
//				zsurf = topo[level][j][i];
				zsurf = 0.0;

				// compute vertical integral
				for(k = sz; k < sz+nz; k++)
				{
					// skip cells below the bottom
					if(k < kk) continue;

					// get cell top and bottom coordinates
					zb = COORD_NODE(k,   sz, fs->dsz);
					zt = COORD_NODE(k+1, sz, fs->dsz);

					// skip cells locating above the free surface
					if(zb > zsurf) continue;

					// check producing cell type
					if(zt > zsurf)
					{
						// cell with bottom node producing
						Q           = q*(zsurf - zb);
						r[k][j][i] -= Q;

					}
					else
					{
						// cell with both nodes producing
						Q             = q*(zt - zb);
						r[k]  [j][i] -= Q/2.0;
						r[k+1][j][i] -= Q/2.0;
					}
				}
			}
		}

		// restore access
		ierr = DMDAVecRestoreArray(surf->DA_SURF, surf->gtopo, &topo);  CHKERRQ(ierr);
	}
*/
