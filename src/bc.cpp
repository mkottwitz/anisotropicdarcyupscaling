//---------------------------------------------------------------------------
//........................... BOUNDARY CONDITIONS ...........................
//---------------------------------------------------------------------------
#include "LaMEM.h"
#include "bc.h"
#include "JacRes.h"
#include "parsing.h"
#include "scaling.h"
#include "tssolve.h"
#include "fdstag.h"
#include "tools.h"
#include "surf.h"
//---------------------------------------------------------------------------
// vWell functions
//---------------------------------------------------------------------------

PetscScalar vWellGetH(vWell *well, PetscScalar t)
{
	PetscInt    jj, j_end;
	PetscScalar h0, hgrad, tgrad, hwell;

	// access context
	h0    = well->h0;
	j_end = well->nt - 1;

	// get current starting time
	for(jj = 0; jj < well->nt-1; jj++)
	{

		if(t <= well->t[jj]) break;

	}

	// linearly interpolate head between previous and current head
	if(t == 0) // consider the case were time is smaller than first increment -> use h0 for that
	{
		hgrad = well->h[jj]- h0;
		tgrad = well->t[jj];

		hwell = h0 + t*(hgrad/tgrad);
	}
	else
	{
		// check number of entries, if j_end does not exist we have only one entry and thus need to use h0 for head interpolation
		if(!j_end)
		{
			if(t < well->t[j_end]) // if current time is lower than last prescribed head
			{
				hgrad = well->h[jj] - h0;
				tgrad = well->t[jj];

				hwell = h0 + t*(hgrad/tgrad);
			}
			else // if current time is larger than last prescribed head, use last head
			{
				hwell = well->h[j_end];
			}
		}
		else // here we have multiple entries in the well head vector and treat them individually
		{
			if(t < well->t[j_end]) // if current time is lower than last prescribed head
			{
				hgrad = well->h[jj] - well->h[jj-1];
				tgrad = well->t[jj] - well->t[jj-1];

				hwell = well->h[jj-1] + (t - well->t[jj-1])*(hgrad/tgrad);
			}
			else // if current time is larger than last prescribed head, use last head
			{
				hwell = well->h[j_end];
			}
		}


	}

	return hwell;
}

//---------------------------------------------------------------------------
// BCCtx functions
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "BCCreate"
PetscErrorCode BCCreate(BCCtx *bc, FB *fb)
{
	Scaling     *scal;
	FreeSurf    *surf;
	char        dir[_str_len_];
	PetscInt    jj, ijk[3], ij[2];
	PetscScalar sweight;
	vWell       *well;
	obsWell     *obsWell;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	scal             = bc->scal;
	surf             = bc->jr->surf;
	sweight          = bc->jr->grav*bc->jr->rho;

	ierr = PetscMemzero(dir,         _str_len_); CHKERRQ(ierr);

	// set defaults
	bc->pdir = -1;

	//=====================
	// PRESSURE CONSTRAINTS
	//=====================

	ierr = getIntParam(fb, _OPTIONAL_, "pres_dir", &bc->pdir, 1, 3);   CHKERRQ(ierr);

	if(bc->pdir != -1)
	{
		ierr = getScalarParam(fb, _REQUIRED_, "pres_bot",  &bc->pbot, 1, 1.0); CHKERRQ(ierr);
		ierr = getScalarParam(fb, _REQUIRED_, "pres_top",  &bc->ptop, 1, 1.0); CHKERRQ(ierr);

		if(bc->pdir == 0)
		{
			//  top and bottom pressure boxes
			ierr = getScalarParam(fb, _REQUIRED_, "bot_box", bc->bot_box, 6, scal->length); CHKERRQ(ierr);
			ierr = getScalarParam(fb, _REQUIRED_, "top_box", bc->top_box, 6, scal->length); CHKERRQ(ierr);
		}
		else
		{
			ierr = getIntParam (fb, _OPTIONAL_, "lingrad", &bc->lingrad, 1, 1); CHKERRQ(ierr);
		}
	}

	//======
	// WELLS
	//======

	// setup block access mode
	ierr = FBFindBlocks(fb, _OPTIONAL_, "<WellStart>", "<WellEnd>"); CHKERRQ(ierr);

	if(fb->nblocks)
	{
		// error checking
		if(fb->nblocks > _max_num_wells_)
		{
			SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Too many wells! Max allowed: %lld", (LLD)_max_num_wells_);
		}

		// store actual number wells
		bc->nwells = fb->nblocks;

		// read each individual wells
		for(jj = 0; jj < fb->nblocks; jj++)
		{
			well = bc->wells + jj;

			ierr = getIntParam   (fb, _REQUIRED_, "ijk", ijk,      3,           0);                      CHKERRQ(ierr);
			ierr = getIntParam   (fb, _REQUIRED_, "nt", &well->nt, 1,          _max_periods_);           CHKERRQ(ierr);
			ierr = getScalarParam(fb, _REQUIRED_, "t",   well->t,  well->nt,   scal->time);              CHKERRQ(ierr);
			ierr = getScalarParam(fb, _REQUIRED_, "h",   well->h,  well->nt,   scal->length);            CHKERRQ(ierr);
			ierr = getScalarParam(fb, _REQUIRED_, "h0", &well->h0, 1,          scal->length);            CHKERRQ(ierr);

			well->i  = ijk[0];
			well->j  = ijk[1];
			well->k  = ijk[2];

			fb->blockID++;
		}
	}

	ierr = FBFreeBlocks(fb); CHKERRQ(ierr);

	//======
	// OBSERVATION WELLS
	//======

	// setup block access mode
	ierr = FBFindBlocks(fb, _OPTIONAL_, "<ObservationWellStart>", "<ObservationWellEnd>"); CHKERRQ(ierr);

	if(fb->nblocks)
	{
		// error checking
		if(fb->nblocks > _max_num_obs_wells_)
		{
			SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Too many wells! Max allowed: %lld", (LLD)_max_num_obs_wells_);
		}

		// store actual number wells
		bc->nObsWells = fb->nblocks;

		// read each individual wells
		for(jj = 0; jj < fb->nblocks; jj++)
		{
			obsWell = bc->obsWells + jj;

			ierr = getIntParam   (fb, _REQUIRED_, "ij",    ij,              2,          0);     CHKERRQ(ierr);
			ierr = getStringParam(fb, _REQUIRED_, "name",  obsWell->name,  "observationWell"); CHKERRQ(ierr);

			obsWell->i    = ij[0];
			obsWell->j    = ij[1];

			fb->blockID++;
		}
	}

	ierr = FBFreeBlocks(fb); CHKERRQ(ierr);

	// check boundary conditions
	if(surf->UseFreeSurf && bc->pdir != -1)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Free surface flow is incompatible with pressure boundary conditions (surf_use, pres_dir)");
	}

	if(!surf->UseFreeSurf && bc->nwells)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Saturated flow is incompatible with distributed sources (surf_use, nwells)");
	}
	if(!surf->UseFreeSurf && bc->pdir == -1)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Saturated flow requires pressure boundary conditions (surf_use, pres_dir)");
	}

	// print summary
	PetscPrintf(PETSC_COMM_WORLD, "Boundary condition parameters: \n");

	if(bc->pdir != -1)
	{
		PetscPrintf(PETSC_COMM_WORLD, "   Top boundary pressure                : %g %s \n", bc->ptop, scal->lbl_stress);
		PetscPrintf(PETSC_COMM_WORLD, "   Bottom boundary pressure             : %g %s \n", bc->pbot, scal->lbl_stress);

		if(bc->pdir == 0)
		{
			PetscPrintf(PETSC_COMM_WORLD, "   Activate box pressure                @ \n");

			// deactivate permeability computation
			bc->jr->getPermea = 0;
			bc->lingrad       = 0;

			PetscPrintf(PETSC_COMM_WORLD, "   WARNING! permeability computation is deactivated (box pressure)\n");
		}
		else
		{
			PetscPrintf(PETSC_COMM_WORLD, "   Pressure direction                   : %s \n", dir);

			if(bc->lingrad)
			{
				PetscPrintf(PETSC_COMM_WORLD, "   Activate linear gradient             @ \n");
			}
		}
	}

	if(bc->nwells)
	{
		PetscPrintf(PETSC_COMM_WORLD, "   Number of wells                      : %lld \n",  (LLD)bc->nwells);
	}


	PetscPrintf(PETSC_COMM_WORLD,"--------------------------------------------------------------------------\n");

	// nondimensionalize pressure, switch to pressure (hydraulic) head
	if(bc->ptop != -1.0)  { bc->ptop /= scal->stress; bc->ptop /= sweight; }
	if(bc->pbot != -1.0)  { bc->pbot /= scal->stress; bc->pbot /= sweight; }

	// allocate vectors and arrays
	ierr = BCCreateData(bc); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "BCCreateData"
PetscErrorCode BCCreateData(BCCtx *bc)
{
	FDSTAG   *fs;
	size_t    sz;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs = bc->fs;

	// create boundary conditions vectors
	ierr = DMCreateGlobalVector(fs->DA_COR, &bc->bcp);  CHKERRQ(ierr);

	// create well flux vector for storing fluxes in well
	if(bc->nwells)
	{
		// cell parameters storage
		sz = sizeof(PetscScalar)*(size_t)bc->nwells;
		ierr = PetscMalloc(sz, &bc->gq); CHKERRQ(ierr);
	}

	// create observation well head vector for storing heads in obseration wells
	if(bc->nObsWells)
	{
		// cell parameters storage
		sz = sizeof(PetscScalar)*(size_t)bc->nObsWells;
		ierr = PetscMalloc(sz, &bc->gp); CHKERRQ(ierr);
	}

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "BCDestroy"
PetscErrorCode BCDestroy(BCCtx *bc)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;

	// destroy boundary conditions vectors
	ierr = VecDestroy(&bc->bcp); CHKERRQ(ierr);

	if(bc->nwells)
	{
		ierr = PetscFree(bc->gq);  CHKERRQ(ierr);
	}

	if(bc->nObsWells)
	{
		ierr = PetscFree(bc->gp);  CHKERRQ(ierr);
	}

	ierr = PetscFree(bc->ibc); CHKERRQ(ierr);
	ierr = PetscFree(bc->idx); CHKERRQ(ierr);
	ierr = PetscFree(bc->val); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "BCGetLinGrad"
PetscErrorCode BCGetLinGrad(BCCtx *bc)
{
	// apply pressure constraints from boundary linear gradient

	FDSTAG       *fs;
	Discret1D    *ds;
	PetscScalar  pbot, ptop;
	PetscScalar  pbot_x1, pbot_x2, pbot_y1, pbot_y2, pbot_z1, pbot_z2;
	PetscScalar  ptop_x1, ptop_x2, ptop_y1, ptop_y2, ptop_z1, ptop_z2;
	PetscInt     mnx, mny, mnz;
	PetscScalar  cnx, cny, cnz;
	PetscInt     i, j, k, nx, ny, nz, sx, sy, sz, nnod;
	PetscScalar  cbot, ctop, *cnod, grad, *linGrad;
	PetscScalar  ***bcp;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs = bc->fs;

	// get boundary pressure
	pbot = bc->pbot;
	ptop = bc->ptop;

	// get node index bounds
	mnx = fs->dsx.tnods - 1;
	mny = fs->dsy.tnods - 1;
	mnz = fs->dsz.tnods - 1;

	// check whether constraint is activated
	if(bc->pdir == -1) PetscFunctionReturn(0);

	// precompute linear pressure gradient if requested
	if(bc->lingrad)
	{
		if(bc->pdir == 1) ds = &fs->dsx;
		if(bc->pdir == 2) ds = &fs->dsy;
		if(bc->pdir == 3) ds = &fs->dsz;

		cbot = ds->gcrdbeg;
		ctop = ds->gcrdend;
		cnod = ds->ncoor;
		nnod = ds->nnods;
		grad = (ptop - pbot)/(ctop - cbot);

		ierr = makeScalArray(&linGrad, NULL, nnod); CHKERRQ(ierr);

		for(i = 0; i < nnod; i++)
		{
			linGrad[i] = pbot + (cnod[i] - cbot)*grad;
		}
	}

	// access custom boundary values if requested
	if(bc->pdir == 0)
	{
		// get boundary region values
		pbot_x1 = bc->bot_box[0];
		pbot_x2 = bc->bot_box[1];
		pbot_y1 = bc->bot_box[2];
		pbot_y2 = bc->bot_box[3];
		pbot_z1 = bc->bot_box[4];
		pbot_z2 = bc->bot_box[5];

		ptop_x1 = bc->top_box[0];
		ptop_x2 = bc->top_box[1];
		ptop_y1 = bc->top_box[2];
		ptop_y2 = bc->top_box[3];
		ptop_z1 = bc->top_box[4];
		ptop_z2 = bc->top_box[5];
	}

	//-----------------------------------------------------
	// P points on corner nodes
	//-----------------------------------------------------

	ierr = DMDAVecGetArray(fs->DA_COR, bc->bcp, &bcp);  CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	// x - gradient
	if(bc->pdir == 1)
	{
		START_STD_LOOP
		{
			if(bc->lingrad)
			{
				if(j == 0)   { bcp[k][j][i] = linGrad[i - sx]; }
				if(j == mny) { bcp[k][j][i] = linGrad[i - sx]; }

				if(k == 0)   { bcp[k][j][i] = linGrad[i - sx]; }
				if(k == mnz) { bcp[k][j][i] = linGrad[i - sx]; }
			}

			if(i == 0)   { bcp[k][j][i] = pbot; }
			if(i == mnx) { bcp[k][j][i] = ptop; }

		}
		END_STD_LOOP
	}

	// y - gradient
	if(bc->pdir == 2)
	{
		START_STD_LOOP
		{
			if(bc->lingrad)
			{
				if(i == 0)   { bcp[k][j][i] = linGrad[j - sy]; }
				if(i == mnx) { bcp[k][j][i] = linGrad[j - sy]; }

				if(k == 0)   { bcp[k][j][i] = linGrad[j - sy]; }
				if(k == mnz) { bcp[k][j][i] = linGrad[j - sy]; }
			}

			if(j == 0)   { bcp[k][j][i] = pbot; }
			if(j == mny) { bcp[k][j][i] = ptop; }

		}
		END_STD_LOOP
	}

	// z - gradient
	if(bc->pdir == 3)
	{
		START_STD_LOOP
		{
			if(bc->lingrad)
			{
				if(i == 0)   { bcp[k][j][i] = linGrad[k - sz]; }
				if(i == mnx) { bcp[k][j][i] = linGrad[k - sz]; }

				if(j == 0)   { bcp[k][j][i] = linGrad[k - sz]; }
				if(j == mny) { bcp[k][j][i] = linGrad[k - sz]; }
			}

			if(k == 0)   { bcp[k][j][i] = pbot; }
			if(k == mnz) { bcp[k][j][i] = ptop; }

		}
		END_STD_LOOP
	}

	// custom boundaries
	if(bc->pdir == 0)
	{
		START_STD_LOOP
		{
			// get coordinates of the current node
			cnx = COORD_NODE(i,  sx, fs->dsx);
			cny = COORD_NODE(j,  sy, fs->dsy);
			cnz = COORD_NODE(k,  sz, fs->dsz);

			// filter for boundary regions
			if(cnx >= pbot_x1 && cnx <= pbot_x2 && cny >= pbot_y1 && cny <= pbot_y2 && cnz >= pbot_z1 && cnz <= pbot_z2)
			{
				bcp[k][j][i] = pbot;
			}
			if(cnx >= ptop_x1 && cnx <= ptop_x2 && cny >= ptop_y1 && cny <= ptop_y2 && cnz >= ptop_z1 && cnz <= ptop_z2)
			{
				bcp[k][j][i] = ptop;
			}

		}
		END_STD_LOOP
	}

	// restore access
	ierr = DMDAVecRestoreArray(fs->DA_COR, bc->bcp, &bcp);  CHKERRQ(ierr);

	if(bc->lingrad){ ierr = PetscFree(linGrad); CHKERRQ(ierr); }

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "BCGetSurf"
PetscErrorCode BCGetSurf(BCCtx *bc)
{
	// get free surface boundary constraints

	FDSTAG      *fs;
	FreeSurf    *surf;
	PetscScalar ***topo, ***bcp, bz, hp;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, level, mnx, mny;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs    = bc->fs;
	surf  = bc->jr->surf;
	level = (PetscInt)fs->dsz.rank;

	// check whether constraint is activated
	if(!surf->UseFreeSurf) PetscFunctionReturn(0);

	// get node index bounds
	mnx = fs->dsx.tnods - 1;
	mny = fs->dsy.tnods - 1;

	// get global box bounds
	ierr = FDSTAGGetGlobalBox(fs, NULL, NULL, &bz, NULL, NULL, NULL); CHKERRQ(ierr);

	ierr = DMDAVecGetArray(surf->DA_SURF, surf->gtopo, &topo);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs  ->DA_COR,  bc  ->bcp,   &bcp);   CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP
	{
		// get hydraulic head
		hp = topo[level][j][i] - bz;

		// fix hydraulic head at lateral boundaries
		if(i == 0)   { bcp[k][j][i] = hp; }
		if(i == mnx) { bcp[k][j][i] = hp; }
		if(j == 0)   { bcp[k][j][i] = hp; }
		if(j == mny) { bcp[k][j][i] = hp; }
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(fs  ->DA_COR,  bc  ->bcp,   &bcp);   CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(surf->DA_SURF, surf->gtopo, &topo);  CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "BCGetWells"
PetscErrorCode BCGetWells(BCCtx *bc)
{
	// get well constraints

	FDSTAG      *fs;
	vWell       *wells, *well;
	PetscScalar ***bcp, t;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, ii, nwells;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs       = bc->fs;
	nwells   = bc->nwells;
	wells    = bc->wells;
	t        = bc->ts->time;

	// check whether constraint is activated
	if(!nwells) PetscFunctionReturn(0);

	// get access
	ierr = DMDAVecGetArray(fs->DA_COR,    bc->bcp,     &bcp);   CHKERRQ(ierr);


	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP
	{
		for(ii = 0; ii < nwells; ii++)
		{
			well   = wells + ii;

			// constrain well node
			if(i == well->i && j == well->j && k >= well->k)
			{
				bcp[k][j][i] = vWellGetH(well, t);

			}
		}
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(fs->DA_COR,    bc->bcp,     &bcp);   CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "BCGetIntMat"
PetscErrorCode BCGetIntMat(BCCtx *bc)
{
	// apply internal pressure constraints for the matrix nodes

	FDSTAG      *fs;
	Vec         vmbc;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, iter;
	PetscScalar ***bcp, ***mbc, *peff;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// skip for free surface flow
	if(bc->jr->surf->UseFreeSurf) PetscFunctionReturn(0);

	// access context
	fs   = bc->fs;
	peff = bc->jr->peff;

	// copy effective permeability in local vector
	ierr = DMGetLocalVector(fs->DA_CEN, &vmbc); CHKERRQ(ierr);

	ierr = VecZeroEntries(vmbc); CHKERRQ(ierr);

	ierr = DMDAVecGetArray(fs->DA_CEN, vmbc, &mbc);  CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_CEN, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	iter = 0;

	START_STD_LOOP
	{
		mbc[k][j][i] = peff[iter++];
	}
	END_STD_LOOP

	ierr = DMDAVecRestoreArray(fs->DA_CEN, vmbc, &mbc);  CHKERRQ(ierr);

	// fill ghost points
	LOCAL_TO_LOCAL(fs->DA_CEN, vmbc)

	// set internal constraints
	ierr = DMDAVecGetArray(fs->DA_CEN, vmbc, &mbc);  CHKERRQ(ierr);

	ierr = DMDAVecGetArray(fs->DA_COR, bc->bcp, &bcp); CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP
	{
		if(mbc[k]  [j]  [i]
		|| mbc[k]  [j]  [i-1]
		|| mbc[k]  [j-1][i-1]
		|| mbc[k]  [j-1][i]
		|| mbc[k-1][j]  [i]
		|| mbc[k-1][j]  [i-1]
		|| mbc[k-1][j-1][i-1]
		|| mbc[k-1][j-1][i])
		{
			continue;
		}

		// constrain node fully surrounded by matrix cells
		bcp[k][j][i] = 0.0;
	}
	END_STD_LOOP

	ierr = DMDAVecRestoreArray(fs->DA_COR, bc->bcp, &bcp); CHKERRQ(ierr);

	ierr = DMDAVecRestoreArray(fs->DA_CEN, vmbc, &mbc); CHKERRQ(ierr);

	ierr = DMRestoreLocalVector(fs->DA_CEN, &vmbc); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "BCApply"
PetscErrorCode BCApply(BCCtx *bc, Vec x)
{
	FDSTAG      *fs;
	MatStencil  *ibc;
	PetscScalar ***bcp, *val, *h, *hn;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, nbc, iter, *idx;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs = bc->fs;

	// mark all variables unconstrained
	ierr = VecSet(bc->bcp, DBL_MAX); CHKERRQ(ierr);

	// get pressure boundary constraints
	ierr = BCGetLinGrad(bc); CHKERRQ(ierr);

	// get free surface boundary constraints
	ierr = BCGetSurf(bc); CHKERRQ(ierr);

	// get well constraints
	ierr = BCGetWells(bc); CHKERRQ(ierr);

	// get internal matrix constraints
	ierr = BCGetIntMat(bc); CHKERRQ(ierr);

	// count constrained DOF
	ierr = DMDAVecGetArray(fs->DA_COR, bc->bcp, &bcp); CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	nbc = 0;

	START_STD_LOOP
	{
		// skip unconstrained DOF
		if(bcp[k][j][i] == DBL_MAX) continue;

		// increment counter
		nbc++;
	}
	END_STD_LOOP

	ierr = PetscFree(bc->ibc); CHKERRQ(ierr);
	ierr = PetscFree(bc->idx); CHKERRQ(ierr);
	ierr = PetscFree(bc->val); CHKERRQ(ierr);

	// allocate space local indices, global stencils & values
	ierr = PetscMalloc(nbc*sizeof(MatStencil),  &ibc); CHKERRQ(ierr);
	ierr = PetscMalloc(nbc*sizeof(PetscInt),    &idx); CHKERRQ(ierr);
	ierr = PetscMalloc(nbc*sizeof(PetscScalar), &val); CHKERRQ(ierr);

	bc->nbc = nbc;
	bc->ibc = ibc;
	bc->idx = idx;
	bc->val = val;

	// collect local indices, global stencils & values
	nbc  = 0;
	iter = 0;

	START_STD_LOOP
	{
		// check constraint
		if(bcp[k][j][i] != DBL_MAX)
		{
			// get local index & value
			idx[nbc] = iter;
			val[nbc] = bcp[k][j][i];

			// get stencil
			ibc[nbc].k = k; ibc[nbc].j = j; ibc[nbc].i = i; ibc[nbc].c = 0;

			// increment counter
			nbc++;
		}

		iter++;
	}
	END_STD_LOOP

	ierr = DMDAVecRestoreArray(fs->DA_COR, bc->bcp, &bcp); CHKERRQ(ierr);

	// set boundary conditions in the solution vector and the previous head vector (hn)
	ierr = VecGetArray(x,          &h);  CHKERRQ(ierr);
	ierr = VecGetArray(bc->jr->hn, &hn); CHKERRQ(ierr);

	for(i = 0; i < nbc; i++) h[idx[i]]  = val[i];
	for(i = 0; i < nbc; i++) hn[idx[i]] = val[i];

	ierr = VecRestoreArray(x,          &h);  CHKERRQ(ierr);
	ierr = VecRestoreArray(bc->jr->hn, &hn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
