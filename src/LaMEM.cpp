#include "LaMEM.h"
#include "scaling.h"
#include "parsing.h"

//---------------------------------------------------------------------------
static char help[] = "Solves 3D Darcy equations using multigrid .\n\n";
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
{
	PetscErrorCode 	ierr;

	// Initialize PETSC
	ierr = PetscInitialize(&argc,&argv,(char *)0, help); CHKERRQ(ierr);

	// Forward simulation
	ierr = LaMEMLibMain(); CHKERRQ(ierr);

	// cleanup PETSC
	ierr = PetscFinalize(); CHKERRQ(ierr);

	return 0;
}
//--------------------------------------------------------------------------
