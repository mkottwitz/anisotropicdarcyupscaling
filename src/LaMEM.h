//---------------------------------------------------------------------------
//.........................   Main include file   ...........................
//---------------------------------------------------------------------------

#ifndef __LaMEM_h__
#define __LaMEM_h__

//-----------------------------------------------------------------------------
//   PREFERABLE VARIABLES
//
//   PetscInt    - for all indices                    (can be int or long long int)
//   PetscScalar - for all floating point variables   (can be float or double)
//   float       - for reduced size output
//   size_t      - for all sizes offsets & counters   (unsigned long long int)
//   PetscMPIInt - for passing service integer parameters to MPI functions (int)
//   MPIU_SCALAR - appropriate MPI Data Type for sending/receiving PetsScalar
//   MPIU_INT    - appropriate MPI Data Type for sending/receiving PetscInt
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SIZE LIMITS
//-----------------------------------------------------------------------------

// string length (two null characters are reserved in the end, i.e. 128)
#define _str_len_ 130

// maximum number of mesh segments in every direction
#define _max_num_segs_ 10

// maximum number of cells per mesh segment
#define _max_num_cells_ 4096

// maximum number of processes in every direction
#define _max_num_procs_ 1024

// maximum number of time steps
#define _max_num_steps_ 20000

// maximum number of components in the output vector (3D)
#define _max_num_comp_ 9

// maximum number of components in the output vector (surface)
#define _max_num_comp_surf_ 3

// length of scaling unit label
#define _lbl_sz_ 23

// maximum number of wells
#define _max_num_wells_ 7

// maximum number of observation wells
#define _max_num_obs_wells_ 20

// maximum number of constant well head periods
#define _max_periods_ 20

// cast macros
#define LLD long long int

//-----------------------------------------------------------------------------
// EXTERNAL INCLUDES
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <ctype.h>
#include <sys/stat.h>
#include <errno.h>
#include <petsc.h>
#include <map>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

//-----------------------------------------------------------------------------
// TYPE DEFINITIONS
//-----------------------------------------------------------------------------

typedef pair <PetscScalar, PetscInt> spair;
typedef pair <PetscInt,    PetscInt> ipair;

//-----------------------------------------------------------------------------
// PROTOTYPES
//-----------------------------------------------------------------------------

// LaMEM library main function

PetscErrorCode LaMEMLibMain();

//-----------------------------------------------------------------------------
#endif
