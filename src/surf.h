//---------------------------------------------------------------------------
//.............................. FREE SURFACE ...............................
//---------------------------------------------------------------------------
#ifndef __surf_h__
#define __surf_h__

//---------------------------------------------------------------------------

struct FB;
struct InterpFlags;
struct FDSTAG;
struct JacRes;

//---------------------------------------------------------------------------

// free surface grid

struct FreeSurf
{
	JacRes *jr;                  // global residual context
	DM      DA_SURF;             // free surface grid
	Vec     ltopo, gtopo, ptopo; // topography vectors (local and global and patch)

	// flags/parameters
	PetscInt    UseFreeSurf; // free surface activation flag
	PetscScalar level;       // initial level
	PetscScalar corners[4];  // corner elevation [left-front, front-right, right-back, back-left]
};

//---------------------------------------------------------------------------

PetscErrorCode FreeSurfCreate(FreeSurf *surf, FB *fb);

PetscErrorCode FreeSurfCreateData(FreeSurf *surf);

PetscErrorCode FreeSurfDestroy(FreeSurf *surf);

PetscErrorCode FreeSurfGetTopo(FreeSurf *surf);

PetscErrorCode FreeSurfSetTopoFromCorners(FreeSurf *surf);

PetscErrorCode FreeSurfSetTopoFromFile(FreeSurf *surf, const char *filename);

//---------------------------------------------------------------------------
#endif
