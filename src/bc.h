//---------------------------------------------------------------------------
//........................... BOUNDARY CONDITIONS ...........................
//---------------------------------------------------------------------------
#ifndef __bc_h__
#define __bc_h__
//---------------------------------------------------------------------------

struct FB;
struct Scaling;
struct TSSol;
struct FDSTAG;
struct Marker;
struct JacRes;

//---------------------------------------------------------------------------

// well structure
struct vWell
{
	PetscInt    i, j, k;          // indices
	PetscInt    nt;               // number of constant head periods
	PetscScalar h0;               // starting head
	PetscScalar hw;               // well head (approximated from flux-balance)
	PetscScalar t[_max_periods_]; // time delimiters (nt-1)
	PetscScalar h[_max_periods_]; // head values (-1.0 - unset)
};

// well structure
struct obsWell
{
	PetscInt    i, j;              // indices
	char        name[_str_len_];   // name of observation well (needed for output)

};

//---------------------------------------------------------------------------

PetscScalar vWellGetH(vWell *well, PetscScalar t);

//---------------------------------------------------------------------------

// boundary condition context
struct BCCtx
{
	//=====================================================================
	//
	// Boundary condition vectors contain prescribed DOF values:
	//
	//        DBL_MAX   - active DOF flag
	//        otherwise - prescribed DOF value
	//
	//=====================================================================

	FDSTAG   *fs;   // grid
	TSSol    *ts;   // time stepping parameters
	Scaling  *scal; // scaling parameters
	JacRes   *jr;   // Jacobian-residual context (CROSS-REFERENCE!)

	// boundary conditions vectors (global)
	Vec bcp;

	// local indices, global stencils & values of constrained DOF
	PetscInt     nbc;
	MatStencil  *ibc;
	PetscInt    *idx;
	PetscScalar *val;

	// pressure on top and bottom boundaries
	PetscInt     pdir;       // direction identifier & control flag
	PetscScalar  pbot, ptop; // top and bottom pressure head values
	PetscInt     lingrad;    // linear gradient flag on lateral boundaries
	PetscScalar  bot_box[6]; // x-y-z coordinate bounds of "bottom" box
	PetscScalar  top_box[6]; // x-y-z coordinate bounds of "top" box
	// pdir codes:
	// -1   - condition is switched off (default)
	//  0   - pressure in the boxes, lingrad must be deactivated
	//  1-3 - coordinate directions (x-z), lingrad can be activated

	// vertical wells indices & head values
	// each well is specified by i,j k indices of bottom node
	PetscInt     nwells;                 // number of wells
	vWell        wells[_max_num_wells_]; // well structures
	PetscScalar *gq;                     // store current volumetric flux in every well

	// vertical wells indices & head values
	// each well is specified by i,j k indices of bottom node
	PetscInt     nObsWells;                     // number of observation wells
	obsWell      obsWells[_max_num_obs_wells_]; // observation well structures
	PetscScalar *gp;                            // store current head in observation every well

};
//---------------------------------------------------------------------------

// create boundary condition context
PetscErrorCode BCCreate(BCCtx *bc, FB *fb);

// allocate internal vectors and arrays
PetscErrorCode BCCreateData(BCCtx *bc);

// destroy boundary condition context
PetscErrorCode BCDestroy(BCCtx *bc);

// get linear gradient boundary pressure
PetscErrorCode BCGetLinGrad(BCCtx *bc);

// get free surface boundary constraints
PetscErrorCode BCGetSurf(BCCtx *bc);

// get well constraints
PetscErrorCode BCGetWells(BCCtx *bc);

// get internal matrix constraints
PetscErrorCode BCGetIntMat(BCCtx *bc);

// apply ALL boundary conditions
PetscErrorCode BCApply(BCCtx *bc, Vec x);

//---------------------------------------------------------------------------
#endif
