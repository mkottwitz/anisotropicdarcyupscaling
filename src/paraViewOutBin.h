//---------------------------------------------------------------------------
//.................   FDSTAG PARAVIEW XML OUTPUT ROUTINES   .................
//---------------------------------------------------------------------------
// All output fields are interpolated onto the corner nodes before output.
// Each processor includes local number of nodes in every spatial direction
// plus one overlapping ghost node from the next processor. Last processor
// doesn't have a ghost node. Every output field is copied into single precision
// buffer defined on the local output nodes. Every components of the vector
// and tensor filed is scaled (if necessary) and arranged accordingly in the
// buffer: x, y, z components for the vector fields, and xx, yy, zz, xy, yz, xz
// components for the tensor filed (diagonal format). When buffer is arranged
// it is written to the output file. Prerequisite for the copying to the buffer
// is to have every component in the LOCAL corner vector, which is obtained
// by usual scattering from the GLOBAL corner vector. Interpolation to the GLOBAL
// corner vector is done from the LOCAL source vectors (center, edges, or faces).
// These vectors are also obtained by global-to-local scattering. Some vectors
// (velocity and momentum residual) are assumed to be available in LOCAL format.
// The overall scheme is as follows:
//    * loop over components (e.g. xx, yy, ... for stress tensor)
//       - copy field from context to global vector (only for center or edge)
//       - global-to-local scatter                  (only for center or edge)
//       - interpolate from local source vector to global corner vector
//       - scatter from global-to-local corner vectors
//       - scale and copy component to the buffer from local corner vector
//    * and of loop
//    * dump buffer to output file
//---------------------------------------------------------------------------
#ifndef __paraViewOutBin_h__
#define __paraViewOutBin_h__

//---------------------------------------------------------------------------

struct FB;
struct FDSTAG;
struct JacRes;
struct Discret1D;

//---------------------------------------------------------------------------
//...................... ParaView output driver object ......................
//---------------------------------------------------------------------------
struct PVOut
{
	JacRes    *jr;
	char      outfile[_str_len_]; // output file name
	float     *buff;              // direct output buffer
	long int  offset;             // pvd file offset
	PetscInt  outpvd;             // pvd file output flag
	PetscInt  velocity;           // Darcy velocity
	PetscInt  head;               // hydraulic head
	PetscInt  pressure;           // pore pressure
	PetscInt  saturation;         // pore saturation
	PetscInt  residual;           // Darcy residual
	PetscInt  permea;             // effective permeability (cell vector)
	PetscInt  poro;               // porosity (cell vector)
	PetscInt  courant;            // averaged courant number (cell vector)
	PetscInt  hpe_rf;             // hydraulic property evolutoion reduction factor (cell vector)
	PetscInt  node_sat;           // saturation interpolated from the integration points to the nodes
};

//---------------------------------------------------------------------------

// create ParaView output driver
PetscErrorCode PVOutCreate(PVOut *pvout, FB *fb);

// create output buffer and vectors
PetscErrorCode PVOutCreateData(PVOut *pvout);

// destroy ParaView output driver
PetscErrorCode PVOutDestroy(PVOut *pvout);

// write all time-step output files to disk (PVD, PVTR, VTR)
PetscErrorCode PVOutWriteTimeStep(PVOut *pvout, const char *dirName, PetscScalar ttime);

// write parallel PVTR file (called every time step on first processor)
// WARNING! this is potential bottleneck, get rid of writing every time-step
PetscErrorCode PVOutWritePVTR(PVOut *pvout, const char *dirName);

// write sequential VTR files on every processor (called every time step)
PetscErrorCode PVOutWriteVTR(PVOut *pvout, const char *dirName);

//---------------------------------------------------------------------------

PetscErrorCode PVOutWriteCoordDir(PVOut *pvout, Discret1D *ds, FILE *fp);

PetscErrorCode PVOutWriteVelocity(PVOut *pvout, FILE *fp);

PetscErrorCode PVOutWriteHead(PVOut *pvout, FILE *fp);

PetscErrorCode PVOutWritePressure(PVOut *pvout, FILE *fp);

PetscErrorCode PVOutWriteNodeSaturation(PVOut *pvout, FILE *fp);

PetscErrorCode PVOutWriteSaturation(PVOut *pvout, FILE *fp);

PetscErrorCode PVOutWriteResidual(PVOut *pvout, FILE *fp);

PetscErrorCode PVOutWritePermea(PVOut *pvout, FILE *fp);

PetscErrorCode PVOutWritePoro(PVOut *pvout, FILE *fp);

PetscErrorCode PVOutWriteCourant(PVOut *pvout, FILE *fp);

PetscErrorCode PVOutWriteHPE(PVOut *pvout, FILE *fp);

//---------------------------------------------------------------------------
//........................... Service Functions .............................
//---------------------------------------------------------------------------

// Add standard header to output file
void WriteXMLHeader(FILE *fp, const char *file_type);

// update PVD file (called every time step on first processor)
// WARNING! this is potential bottleneck, get rid of writing every time-step
PetscErrorCode UpdatePVDFile(
		const char *dirName, const char *outfile, const char *ext,
		long int &offset, PetscScalar ttime, PetscInt outpvd);

PetscErrorCode OutputBufferWrite(
		FILE     *fp,
		float    *buff,
		PetscInt  cn);

PetscErrorCode OutputBufferAddComp(
		FDSTAG      *fs,    // grid
		Vec          v,     // global vector storing component
		PetscScalar  cf,    // scaling coefficient
		float       *buff,  // output buffer
		PetscInt     ncomp, // number of components
		PetscInt     dir,   // component identifier
		PetscInt    &cn);   // storage counter

//---------------------------------------------------------------------------
#endif
