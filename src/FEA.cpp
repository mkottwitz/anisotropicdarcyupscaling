//---------------------------------------------------------------------------
//......................  FINITE ELEMENT FUNCTIONS   ..........................
//---------------------------------------------------------------------------
#include "LaMEM.h"
#include "FEA.h"

// get quadrature rule of element
#undef __FUNCT__
#define __FUNCT__ "getQuad"
PetscErrorCode getQuad(
	PetscScalar *px1, // x coordinate of quadrature points
	PetscScalar *px2, // y coordinate of quadrature points
	PetscScalar *px3, // z coordinate of quadrature points
	PetscScalar *pwt) // weights of quadrature points
{
	PetscScalar k(sqrt(1.0/3.0)), w(1.0);

	px1[0] = -k;   px2[0] = -k;   px3[0] = -k;   pwt[0] = w;
	px1[1] =  k;   px2[1] = -k;   px3[1] = -k;   pwt[1] = w;
	px1[2] =  k;   px2[2] =  k;   px3[2] = -k;   pwt[2] = w;
	px1[3] = -k;   px2[3] =  k;   px3[3] = -k;   pwt[3] = w;
	px1[4] = -k;   px2[4] = -k;   px3[4] =  k;   pwt[4] = w;
	px1[5] =  k;   px2[5] = -k;   px3[5] =  k;   pwt[5] = w;
	px1[6] =  k;   px2[6] =  k;   px3[6] =  k;   pwt[6] = w;
	px1[7] = -k;   px2[7] =  k;   px3[7] =  k;   pwt[7] = w;

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
// get linear shape functions and from element coordinates
#undef __FUNCT__
#define __FUNCT__ "getShape"
PetscErrorCode getShape(
			PetscScalar  x1, // integration point coordinate (x)
			PetscScalar  x2, // integration point coordinate (y)
			PetscScalar  x3, // integration point coordinate (z)
			PetscScalar *N)  // node shape functions
{
	PetscFunctionBegin;

	PetscScalar k  (0.125);
	PetscScalar px1(1.0 + x1), px2(1.0 + x2), px3(1.0 + x3);
	PetscScalar mx1(1.0 - x1), mx2(1.0 - x2), mx3(1.0 - x3);

	N[0] = k*mx1*mx2*mx3;  // N1
	N[1] = k*px1*mx2*mx3;  // N2
	N[2] = k*px1*px2*mx3;  // N3
	N[3] = k*mx1*px2*mx3;  // N4
	N[4] = k*mx1*mx2*px3;  // N5
	N[5] = k*px1*mx2*px3;  // N6
	N[6] = k*px1*px2*px3;  // N7
	N[7] = k*mx1*px2*px3;  // N8

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
// get global shape function derivatives & Jacobian determinant
#undef __FUNCT__
#define __FUNCT__ "getShapeDeriv"
PetscErrorCode getShapeDeriv(
		PetscScalar  x1,   // integration point coordinate (x)
		PetscScalar  x2,   // integration point coordinate (y)
		PetscScalar  x3,   // integration point coordinate (z)
		PetscScalar *cX1,  // node-coordinates of element  (x)
		PetscScalar *cX2,  // node-coordinates of element  (y)
		PetscScalar *cX3,  // node-coordinates of element  (z)
		PetscScalar *dX1,  // shape function derivatives   (x)
		PetscScalar *dX2,  // shape function derivatives   (y)
		PetscScalar *dX3,  // shape function derivatives   (z)
		PetscScalar &detJ) // Jacobian determinant
{
	PetscFunctionBegin;

	PetscInt    i;
	PetscScalar k (0.125);
	PetscScalar H1, H2, H3;
	PetscScalar px1(1.0 + x1), px2(1.0 + x2), px3(1.0 + x3);
	PetscScalar mx1(1.0 - x1), mx2(1.0 - x2), mx3(1.0 - x3);
	PetscScalar j11(0.0), j12(0.0), j13(0.0);
	PetscScalar j21(0.0), j22(0.0), j23(0.0);
	PetscScalar	j31(0.0), j32(0.0), j33(0.0);
	PetscScalar i11,      i12,      i13;
	PetscScalar i21,      i22,      i23;
	PetscScalar i31,      i32,      i33;

	// compute shape functions derivatives with respect to local coordinates:
	// d/dX1               d/dX2                   d/dX3
	dX1[0]= - k*mx2*mx3;   dX2[0] = - k*mx1*mx3;   dX3[0] = - k*mx1*mx2; // dN1
	dX1[1]=   k*mx2*mx3;   dX2[1] = - k*px1*mx3;   dX3[1] = - k*px1*mx2; // dN2
	dX1[2]=   k*px2*mx3;   dX2[2] =   k*px1*mx3;   dX3[2] = - k*px1*px2; // dN3
	dX1[3]= - k*px2*mx3;   dX2[3] =   k*mx1*mx3;   dX3[3] = - k*mx1*px2; // dN4
	dX1[4]= - k*mx2*px3;   dX2[4] = - k*mx1*px3;   dX3[4] =   k*mx1*mx2; // dN5
	dX1[5]=   k*mx2*px3;   dX2[5] = - k*px1*px3;   dX3[5] =   k*px1*mx2; // dN6
	dX1[6]=   k*px2*px3;   dX2[6] =   k*px1*px3;   dX3[6] =   k*px1*px2; // dN7
	dX1[7]= - k*px2*px3;   dX2[7] =   k*mx1*px3;   dX3[7] =   k*mx1*px2; // dN8

	// compute Jacoby matrix
	for(i = 0; i < 8; i++)
	{
		j11 += dX1[i] * cX1[i];
		j21 += dX2[i] * cX1[i];
		j31 += dX3[i] * cX1[i];
		j12 += dX1[i] * cX2[i];
		j22 += dX2[i] * cX2[i];
		j32 += dX3[i] * cX2[i];
		j13 += dX1[i] * cX3[i];
		j23 += dX2[i] * cX3[i];
		j33 += dX3[i] * cX3[i];
	}

	// get Jacobian determinant
	detJ = (j11*(j22*j33 - j23*j32)
	+       j12*(j23*j31 - j21*j33)
	+       j13*(j21*j32 - j22*j31));

	// compute inverse Jacoby matrix
	i11 = (j22 * j33 - j23 * j32)/detJ;
	i12 = (j13 * j32 - j12 * j33)/detJ;
	i13 = (j12 * j23 - j13 * j22)/detJ;
	i21 = (j23 * j31 - j21 * j33)/detJ;
	i22 = (j11 * j33 - j13 * j31)/detJ;
	i23 = (j13 * j21 - j11 * j23)/detJ;
	i31 = (j21 * j32 - j22 * j31)/detJ;
	i32 = (j12 * j31 - j11 * j32)/detJ;
	i33 = (j11 * j22 - j12 * j21)/detJ;

	// compute shape function derivatives w.r.t. global coordinates
	for(i = 0; i < 8; i++)
	{
		H1 = dX1[i];
		H2 = dX2[i];
		H3 = dX3[i];

		dX1[i] = i11*H1 + i12*H2 + i13*H3;
		dX2[i] = i21*H1 + i22*H2 + i23*H3;
		dX3[i] = i31*H1 + i32*H2 + i33*H3;
	}

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
