//---------------------------------------------------------------------------
//.............................. FREE SURFACE ...............................
//---------------------------------------------------------------------------
#include "LaMEM.h"
#include "surf.h"
#include "parsing.h"
#include "scaling.h"
#include "tssolve.h"
#include "fdstag.h"
#include "bc.h"
#include "JacRes.h"
#include "tools.h"
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "FreeSurfCreate"
PetscErrorCode FreeSurfCreate(FreeSurf *surf, FB *fb)
{
	Scaling     *scal;
	PetscScalar *corn;
	char        filename[_str_len_];
	PetscInt    num;

	// access context
	scal = surf->jr->scal;
	corn = surf->corners;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// check whether free surface flow is activated
	ierr = getIntParam(fb, _OPTIONAL_, "surf_use", &surf->UseFreeSurf, 1,  1); CHKERRQ(ierr);

	// print summary
	PetscPrintf(PETSC_COMM_WORLD, "Free surface parameters: \n");

	// free surface cases only
	if(!surf->UseFreeSurf)
	{
		PetscPrintf(PETSC_COMM_WORLD, "   Activate saturated flow              @ \n");
		PetscPrintf(PETSC_COMM_WORLD,"--------------------------------------------------------------------------\n");
		PetscFunctionReturn(0);
	}

	PetscPrintf(PETSC_COMM_WORLD, "   Activate free surface flow           @ \n");

	// set defaults
	surf->level = DBL_MAX;
	(*corn)     = DBL_MAX;

	// read from options
	ierr = getScalarParam(fb, _OPTIONAL_, "surf_level",    &surf->level, 1, 1.0); CHKERRQ(ierr);
	ierr = getScalarParam(fb, _OPTIONAL_, "surf_corners",   corn,        4, 1.0); CHKERRQ(ierr);
	ierr = getStringParam(fb, _OPTIONAL_, "surf_topo_file", filename,    NULL);   CHKERRQ(ierr);

	// check whether topography is specified uniquely
	num = 0;
	if(surf->level != DBL_MAX) num++;
	if((*corn)     != DBL_MAX) num++;
	if(strlen(filename))       num++;

	if(num > 1 || !num)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Free surface topography must be specified by (only) one parameter (surf_level, surf_corners, surf_topo_file)");
	}

	if(surf->level != DBL_MAX)
	{
		PetscPrintf(PETSC_COMM_WORLD, "   Initial surface level                : %g %s \n",  surf->level, scal->lbl_length);

		surf->level /= scal->length;
	}

	if((*corn) != DBL_MAX)
	{
		PetscPrintf(PETSC_COMM_WORLD, "   Surface corner elevation             : %g %g %g %g %s  \n",
				corn[0], corn[1], corn[2], corn[3], scal->lbl_length);

		corn[0] /= scal->length;
		corn[1] /= scal->length;
		corn[2] /= scal->length;
		corn[3] /= scal->length;
	}

	PetscPrintf(PETSC_COMM_WORLD,"--------------------------------------------------------------------------\n");

	// create structures
	ierr = FreeSurfCreateData(surf); CHKERRQ(ierr);

	// initialize free surface with flat level
	if(surf->level != DBL_MAX)
	{
		ierr = VecSet(surf->gtopo, surf->level); CHKERRQ(ierr);
	}

	// initialize free surface from corners
	ierr = FreeSurfSetTopoFromCorners(surf); CHKERRQ(ierr);

	// initialize free surface from file if provided
	ierr = FreeSurfSetTopoFromFile(surf, filename); CHKERRQ(ierr);

	// compute ghosted version of surface topography
	GLOBAL_TO_LOCAL(surf->DA_SURF, surf->gtopo, surf->ltopo);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "FreeSurfCreateData"
PetscErrorCode FreeSurfCreateData(FreeSurf *surf)
{
	FDSTAG         *fs;
	const PetscInt *lx, *ly;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs = surf->jr->fs;

	// get grid partitioning in X & Y directions
	ierr = DMDAGetOwnershipRanges(fs->DA_COR, &lx, &ly, NULL); CHKERRQ(ierr);

	// create redundant free surface DMDA
	ierr = DMDACreate3dSetUp(PETSC_COMM_WORLD,
		DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,
		DMDA_STENCIL_BOX,
		fs->dsx.tnods, fs->dsy.tnods, fs->dsz.nproc,
		fs->dsx.nproc, fs->dsy.nproc, fs->dsz.nproc,
		1, 1, lx, ly, NULL, &surf->DA_SURF); CHKERRQ(ierr);

	ierr = DMCreateLocalVector (surf->DA_SURF, &surf->ltopo);  CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(surf->DA_SURF, &surf->gtopo);  CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(surf->DA_SURF, &surf->ptopo);  CHKERRQ(ierr);

	PetscFunctionReturn(0);
}

//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "FreeSurfDestroy"
PetscErrorCode FreeSurfDestroy(FreeSurf *surf)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;

	// free surface cases only
	if(!surf->UseFreeSurf) PetscFunctionReturn(0);

	ierr = DMDestroy (&surf->DA_SURF); CHKERRQ(ierr);
	ierr = VecDestroy(&surf->ltopo);   CHKERRQ(ierr);
	ierr = VecDestroy(&surf->gtopo);   CHKERRQ(ierr);
	ierr = VecDestroy(&surf->ptopo);   CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "FreeSurfGetTopo"
PetscErrorCode FreeSurfGetTopo(FreeSurf *surf)
{
	// compute free surface topography from pressure field

	JacRes      *jr;
	FDSTAG      *fs;
	Discret1D   *dsz;
	Vec         lp;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, level, mnz;
	PetscScalar ***p, ***patch, *ptopo, *gtopo, pb, pe, zb, ze, d, z;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	jr    = surf->jr;
	fs    = jr->fs;
	dsz   = &fs->dsz;
	level = (PetscInt)dsz->rank;
	mnz   = fs->dsz.tnods - 1;

	// create column communicator
	ierr = Discret1DGetColumnComm(dsz); CHKERRQ(ierr);

	// clear topography patch
	ierr = VecZeroEntries(surf->ptopo); CHKERRQ(ierr);

	// access local pressure
	GET_INIT_LOCAL_VECTOR(fs->DA_COR, jr->pv, lp, p)

	// access topography patch
	ierr = DMDAVecGetArray(surf->DA_SURF, surf->ptopo, &patch);  CHKERRQ(ierr);

	// scan all free surface local points
	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP
	{
		// skip to level of nodes
		if(k == mnz) continue;

		// get coordinates and pressure
		zb = COORD_NODE(k,   sz, fs->dsz);
		ze = COORD_NODE(k+1, sz, fs->dsz);
		pb = p[k]  [j][i];
		pe = p[k+1][j][i];

		if(pb*pe <= 0.0)
		{
			// compute pressure gradient
			d = (pe - pb)/(ze - zb);

			// skip is gradient is zero
			if(!d) continue;

			// compute free surface coordinate
			z = zb - pb/d;

			// enforce bounds
			if(z < zb) z = zb;
			if(z > ze) z = ze;

			// skip if free surface is coincident with ghost node
			if(z == ze && k == sz + nz - 1) continue;

			// store free surface coordinate
			patch[level][j][i] = z;

		}
	}
	END_STD_LOOP

	ierr = DMDAVecRestoreArray(surf->DA_SURF, surf->ptopo, &patch);  CHKERRQ(ierr);

	RESTORE_LOCAL_VECTOR(fs->DA_COR, lp, p)

	// assemble topography patches
	if(dsz->nproc != 1)
	{
		ierr = VecGetArray(surf->ptopo, &ptopo); CHKERRQ(ierr);
		ierr = VecGetArray(surf->gtopo, &gtopo); CHKERRQ(ierr);

		ierr = MPI_Allreduce(ptopo, gtopo, (PetscMPIInt)(nx*ny), MPIU_SCALAR, MPI_SUM, dsz->comm); CHKERRQ(ierr);

		ierr = VecRestoreArray(surf->ptopo, &ptopo); CHKERRQ(ierr);
		ierr = VecRestoreArray(surf->gtopo, &gtopo); CHKERRQ(ierr);

	}
	else
	{
		ierr = VecCopy(surf->ptopo, surf->gtopo); CHKERRQ(ierr);
	}

	// compute ghosted topography
	GLOBAL_TO_LOCAL(surf->DA_SURF, surf->gtopo, surf->ltopo);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "FreeSurfSetTopoFromCorners"
PetscErrorCode FreeSurfSetTopoFromCorners(FreeSurf *surf)
{
	FDSTAG         *fs;
	PetscInt       i, j, nx, ny, sx, sy, level;
	PetscScalar    ***topo, *corn, xp, yp, bx, by, ex, ey, cxb, cyb, cxe, cye;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs    = surf->jr->fs;
	level = fs->dsz.rank;
	corn  = surf->corners;

	// relevant cases only
	if((*corn) == DBL_MAX) PetscFunctionReturn(0);

	// get global coordinate bounds
	ierr = FDSTAGGetGlobalBox(fs, &bx, &by, NULL, &ex, &ey, NULL); CHKERRQ(ierr);

	// access topography vector
	ierr = DMDAVecGetArray(surf->DA_SURF, surf->gtopo, &topo);  CHKERRQ(ierr);

	// scan all nodes
	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, NULL, &nx, &ny, NULL); CHKERRQ(ierr);

	START_PLANE_LOOP
	{
		// get lateral coordinates
		xp = COORD_NODE(i, sx, fs->dsx);
		yp = COORD_NODE(j, sy, fs->dsy);

		// get relative coordinates
		cxe = (xp - bx)/(ex - bx); cxb = 1.0 - cxe;
		cye = (yp - by)/(ey - by); cyb = 1.0 - cye;

		// interpolate free surface elevation
		topo[level][j][i] =
			corn[0]*cxb*cyb +
			corn[1]*cxe*cyb +
			corn[2]*cxe*cye +
			corn[3]*cxb*cye;
	}
	END_PLANE_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(surf->DA_SURF, surf->gtopo, &topo);  CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "FreeSurfSetTopoFromFile"
PetscErrorCode FreeSurfSetTopoFromFile(FreeSurf *surf, const char *filename)
{
	FDSTAG         *fs;
	int            fd;
	PetscLogDouble t;
	PetscViewer    view_in;
	PetscInt 	   nxTopo, nyTopo, Ix, Iy, GridSize;
	PetscInt       i, j, nx, ny, sx, sy, level;
	PetscScalar    ***topo, *Z, header, dim[2], start[2], spacing[2];
	PetscScalar    xp, yp, xpL, ypL, DX, DY, bx, by, ex, ey, leng, X1, Y1;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// relevant cases only
	if(!strlen(filename)) PetscFunctionReturn(0);

	PrintStart(&t, "Loading topography redundantly from", filename);

	// access context
	fs    = surf->jr->fs;
	level = fs->dsz.rank;
	leng  = surf->jr->scal->length;

	// open & read file
	ierr = PetscViewerBinaryOpen(PETSC_COMM_SELF, filename, FILE_MODE_READ, &view_in); CHKERRQ(ierr);
	ierr = PetscViewerBinaryGetDescriptor(view_in, &fd);                               CHKERRQ(ierr);

	// read (and ignore) the silent undocumented file header
	ierr = PetscBinaryRead(fd, &header, 1, PETSC_SCALAR); CHKERRQ(ierr);

	// read grid dimensions
	ierr = PetscBinaryRead(fd, &dim, 2,  PETSC_SCALAR); CHKERRQ(ierr);

	// read south-west corner coordinates
	ierr = PetscBinaryRead(fd, &start, 2,  PETSC_SCALAR); CHKERRQ(ierr);

	// read grid spacing
	ierr = PetscBinaryRead(fd, &spacing, 2,  PETSC_SCALAR); CHKERRQ(ierr);

	// compute grid size
	nxTopo = (PetscInt)dim[0];
	nyTopo = (PetscInt)dim[1];
	GridSize = nxTopo * nyTopo;

	// allocate space for entire file & initialize counter
	ierr = PetscMalloc((size_t)GridSize*sizeof(PetscScalar), &Z); CHKERRQ(ierr);
	
	// read entire file
	ierr = PetscBinaryRead(fd, Z, GridSize, PETSC_SCALAR); CHKERRQ(ierr);

	// destroy file handle
	ierr = PetscViewerDestroy(&view_in); CHKERRQ(ierr);

	// get input topography grid spacing
	DX = spacing[0]/leng;
	DY = spacing[1]/leng;

	// get input topography south-west corner
	X1 = start[0]/leng;
	Y1 = start[1]/leng;

	// get mesh extents
	ierr = FDSTAGGetGlobalBox(fs, &bx, &by, 0, &ex, &ey, 0); CHKERRQ(ierr);

	// check if input grid covers at least the entire LaMEM grid
	if(X1 > bx)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Topography input file does not cover western edge of the box!");
	}

	if(X1+(nxTopo-1)*DX < ex)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Topography input file does not cover eastern edge of the box!");
	}

	if(Y1 > by)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Topography input file does not cover southern edge of the box!");
	}

	if(Y1+(nyTopo-1)*DY < ey)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Topography input file does not cover northern edge of the box!");
	}

	// access topography vector
	ierr = DMDAVecGetArray(surf->DA_SURF, surf->gtopo,  &topo);  CHKERRQ(ierr);

	// scan all free surface local points
	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, NULL, &nx, &ny, NULL); CHKERRQ(ierr);

	// scan all nodes
	START_PLANE_LOOP
	{
		// get node coordinate
		xp = COORD_NODE(i, sx, fs->dsx);
		yp = COORD_NODE(j, sy, fs->dsy);

		// check in which element of the input grid the LaMEM node is
		Ix = (PetscInt)PetscFloorReal((xp-X1)/DX);
		Iy = (PetscInt)PetscFloorReal((yp-Y1)/DY);

		// in case the last LaMEM node is identical with the last input grid node
		if (Ix == nxTopo - 1) Ix = nxTopo - 2;
		if (Iy == nyTopo - 1) Iy = nyTopo - 2;

		// get relative coordinates of the LaMEM node in relation to SW node of inout grid element
		xpL = (PetscScalar)((xp-(X1+(Ix*DX)))/DX);
		ypL = (PetscScalar)((yp-(Y1+(Iy*DY)))/DY);

		// interpolate topography from input grid onto LaMEM nodes
		topo[level][j][i] = (
			(1.0-xpL) * (1.0-ypL) * Z[Ix   + Iy     * nxTopo] +
			(xpL)     * (1.0-ypL) * Z[Ix+1 + Iy     * nxTopo] +
			(xpL)     * (ypL)     * Z[Ix+1 + (Iy+1) * nxTopo] +
			(1.0-xpL) * (ypL)     * Z[Ix   + (Iy+1) * nxTopo])/leng;
	}
	END_PLANE_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(surf->DA_SURF, surf->gtopo, &topo);  CHKERRQ(ierr);

	// clear memory
	PetscFree(Z);

	PrintDone(t);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
