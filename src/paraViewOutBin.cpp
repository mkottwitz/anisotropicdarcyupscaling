//---------------------------------------------------------------------------
//.................   FDSTAG PARAVIEW XML OUTPUT ROUTINES   .................
//---------------------------------------------------------------------------
#include "LaMEM.h"
#include "paraViewOutBin.h"
#include "scaling.h"
#include "parsing.h"
#include "fdstag.h"
#include "JacRes.h"
#include "tools.h"
//---------------------------------------------------------------------------
//...................... ParaView output driver object ......................
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutCreate"
PetscErrorCode PVOutCreate(PVOut *pvout, FB *fb)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;

	// initialize
	pvout->outpvd   = 1;
	pvout->velocity = 1;
	pvout->pressure = 1;

	// read
	ierr = getStringParam(fb, _OPTIONAL_, "out_file_name",   pvout->outfile,   "output"); CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_pvd",        &pvout->outpvd,     1, 1);    CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_velocity",   &pvout->velocity,   1, 1);    CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_head",       &pvout->head,       1, 1);    CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_pressure",   &pvout->pressure,   1, 1);    CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_saturation", &pvout->saturation, 1, 1);    CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_residual",   &pvout->residual,   1, 1);    CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_permea",     &pvout->permea,     1, 1);    CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_poro",       &pvout->poro,       1, 1);    CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_courant",    &pvout->courant,    1, 1);    CHKERRQ(ierr);
	ierr = getIntParam   (fb, _OPTIONAL_, "out_hpe_rf",     &pvout->hpe_rf,     1, 1);    CHKERRQ(ierr);

	// print summary
	PetscPrintf(PETSC_COMM_WORLD, "Output parameters:\n");
	PetscPrintf(PETSC_COMM_WORLD, "   Output file name                     : %s \n", pvout->outfile);
	PetscPrintf(PETSC_COMM_WORLD, "   Write .pvd file                      : %s \n", pvout->outpvd ? "yes" : "no");

	if(pvout->velocity)       PetscPrintf(PETSC_COMM_WORLD, "   Velocity                             @ \n");
	if(pvout->head)           PetscPrintf(PETSC_COMM_WORLD, "   Hydraulic head                       @ \n");
	if(pvout->pressure)       PetscPrintf(PETSC_COMM_WORLD, "   Pressure                             @ \n");
	if(pvout->residual)       PetscPrintf(PETSC_COMM_WORLD, "   Residual                             @ \n");
	if(pvout->permea)         PetscPrintf(PETSC_COMM_WORLD, "   Permeability                         @ \n");
	if(pvout->saturation)     PetscPrintf(PETSC_COMM_WORLD, "   Saturation                           @ \n");
	if(pvout->poro)           PetscPrintf(PETSC_COMM_WORLD, "   Porosity                             @ \n");
	if(pvout->courant)        PetscPrintf(PETSC_COMM_WORLD, "   Coruant number                       @ \n");
	if(pvout->hpe_rf)         PetscPrintf(PETSC_COMM_WORLD, "   HPE reduction factor                 @ \n");

	PetscPrintf(PETSC_COMM_WORLD, "--------------------------------------------------------------------------\n");

	// create output buffer and vectors
	ierr = PVOutCreateData(pvout); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutCreateData"
PetscErrorCode PVOutCreateData(PVOut *pvout)
{
	FDSTAG   *fs;
	PetscInt rx, ry, rz, sx, sy, sz, nx, ny, nz;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	fs = pvout->jr->fs;

	// get local output grid sizes
	GET_OUTPUT_RANGE(rx, nx, sx, fs->dsx)
	GET_OUTPUT_RANGE(ry, ny, sy, fs->dsy)
	GET_OUTPUT_RANGE(rz, nz, sz, fs->dsz)

	// allocate output buffer
	ierr = PetscMalloc((size_t)(_max_num_comp_*nx*ny*nz)*sizeof(float), &pvout->buff); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutDestroy"
PetscErrorCode PVOutDestroy(PVOut *pvout)
{
	PetscFunctionBegin;

	// output buffer
	PetscFree(pvout->buff);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWriteTimeStep"
PetscErrorCode PVOutWriteTimeStep(PVOut *pvout, const char *dirName, PetscScalar ttime)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;

	// update .pvd file if necessary
	ierr = UpdatePVDFile(dirName, pvout->outfile, "pvtr", pvout->offset, ttime, pvout->outpvd); CHKERRQ(ierr);

	// write parallel data .pvtr file
	ierr = PVOutWritePVTR(pvout, dirName); CHKERRQ(ierr);

	// write sub-domain data .vtr files
	ierr = PVOutWriteVTR(pvout, dirName); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWritePVTR"
PetscErrorCode PVOutWritePVTR(PVOut *pvout, const char *dirName)
{
	FILE        *fp;
	FDSTAG      *fs;
	Scaling     *scal;
	char        *fname;
	PetscInt     rx, ry, rz;
	PetscMPIInt  nproc, iproc;

	PetscFunctionBegin;

	// only first process generates this file (WARNING! Bottleneck!)
	if(!ISRankZero(PETSC_COMM_WORLD)) PetscFunctionReturn(0);

	// access context
	fs   = pvout->jr->fs;
	scal = pvout->jr->scal;

	// open outfile.pvtr file in the output directory (write mode)
	asprintf(&fname, "%s/%s.pvtr", dirName, pvout->outfile);
	fp = fopen(fname,"w");
	if(fp == NULL) SETERRQ1(PETSC_COMM_SELF, 1,"cannot open file %s", fname);
	free(fname);

	// write header
	WriteXMLHeader(fp, "PRectilinearGrid");

	// open rectilinear grid data block (write total grid size)
	fprintf(fp, "\t<PRectilinearGrid GhostLevel=\"0\" WholeExtent=\"%lld %lld %lld %lld %lld %lld\">\n",
		1LL, (LLD)fs->dsx.tnods,
		1LL, (LLD)fs->dsy.tnods,
		1LL, (LLD)fs->dsz.tnods);

	// write coordinate block
	fprintf(fp, "\t\t<PCoordinates>\n");
	fprintf(fp, "\t\t\t<PDataArray type=\"Float32\" Name=\"Coordinates_X\" NumberOfComponents=\"1\" format=\"appended\"/>\n");
	fprintf(fp, "\t\t\t<PDataArray type=\"Float32\" Name=\"Coordinates_Y\" NumberOfComponents=\"1\" format=\"appended\"/>\n");
	fprintf(fp, "\t\t\t<PDataArray type=\"Float32\" Name=\"Coordinates_Z\" NumberOfComponents=\"1\" format=\"appended\"/>\n");
	fprintf(fp, "\t\t</PCoordinates>\n");

	// write cell data block
	fprintf(fp, "\t\t<PCellData>\n");

	if(pvout->permea)
	{
		fprintf(fp,"\t\t\t<PDataArray type=\"Float32\" Name=\"permeability %s\" NumberOfComponents=\"1\" format=\"appended\"/>\n", scal->lbl_area_si);
	}

	if(pvout->poro)
	{
		fprintf(fp,"\t\t\t<PDataArray type=\"Float32\" Name=\"porosity [ ]\" NumberOfComponents=\"1\" format=\"appended\"/>\n");
	}

	if(pvout->saturation)
	{
		fprintf(fp,"\t\t\t<PDataArray type=\"Float32\" Name=\"saturation [ ]\" NumberOfComponents=\"1\" format=\"appended\"/>\n");
	}

	if(pvout->courant)
	{
		fprintf(fp,"\t\t\t<PDataArray type=\"Float32\" Name=\"courant number [ ]\" NumberOfComponents=\"1\" format=\"appended\"/>\n");
	}

	if(pvout->hpe_rf)
	{
		fprintf(fp,"\t\t\t<PDataArray type=\"Float32\" Name=\"hpe reduction factor [ ]\" NumberOfComponents=\"1\" format=\"appended\"/>\n");
	}

	fprintf(fp, "\t\t</PCellData>\n");

	// write node data block
	fprintf(fp, "\t\t<PPointData>\n");

	if(pvout->velocity)
	{
		fprintf(fp,"\t\t\t<PDataArray type=\"Float32\" Name=\"velocity %s\" NumberOfComponents=\"3\" format=\"appended\"/>\n", scal->lbl_velocity);
	}
	if(pvout->head)
	{
		fprintf(fp,"\t\t\t<PDataArray type=\"Float32\" Name=\"head %s\" NumberOfComponents=\"1\" format=\"appended\"/>\n", scal->lbl_length);
	}
	if(pvout->pressure)
	{
		fprintf(fp,"\t\t\t<PDataArray type=\"Float32\" Name=\"pressure %s\" NumberOfComponents=\"1\" format=\"appended\"/>\n", scal->lbl_stress);
	}
	if(pvout->residual)
	{
		fprintf(fp,"\t\t\t<PDataArray type=\"Float32\" Name=\"residual %s\" NumberOfComponents=\"1\" format=\"appended\"/>\n", scal->lbl_volumetric_rate);
	}

	fprintf(fp, "\t\t</PPointData>\n");

	// get total number of sub-domains
	MPI_Comm_size(PETSC_COMM_WORLD, &nproc);

	// write local grid sizes (extents) and data file names for all sub-domains
	for(iproc = 0; iproc < nproc; iproc++)
	{
		// get sub-domain ranks in all coordinate directions
		getLocalRank(&rx, &ry, &rz, iproc, fs->dsx.nproc, fs->dsy.nproc);

		// write data
		fprintf(fp, "\t\t<Piece Extent=\"%lld %lld %lld %lld %lld %lld\" Source=\"%s_p%1.8lld.vtr\"/>\n",
			(LLD)(fs->dsx.starts[rx] + 1), (LLD)(fs->dsx.starts[rx+1] + 1),
			(LLD)(fs->dsy.starts[ry] + 1), (LLD)(fs->dsy.starts[ry+1] + 1),
			(LLD)(fs->dsz.starts[rz] + 1), (LLD)(fs->dsz.starts[rz+1] + 1), pvout->outfile, (LLD)iproc);
	}

	// close rectilinear grid data block
	fprintf(fp, "\t</PRectilinearGrid>\n");
	fprintf(fp, "</VTKFile>\n");

	// close file
	fclose(fp);
	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWriteVTR"
PetscErrorCode PVOutWriteVTR(PVOut *pvout, const char *dirName)
{
	FILE         *fp;
	FDSTAG       *fs;
	JacRes       *jr;
	Scaling      *scal;
	char         *fname;
	PetscInt      rx, ry, rz, sx, sy, sz, nx, ny, nz;
	PetscMPIInt   rank;
	size_t        offset = 0;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// get global sub-domain rank
	ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank); CHKERRQ(ierr);

	// access context
	jr   = pvout->jr;
	fs   = jr->fs;
	scal = jr->scal;

	// get sizes of output grid
	GET_OUTPUT_RANGE(rx, nx, sx, fs->dsx)
	GET_OUTPUT_RANGE(ry, ny, sy, fs->dsy)
	GET_OUTPUT_RANGE(rz, nz, sz, fs->dsz)

	// open outfile_p_XXXXXX.vtr file in the output directory (write mode)
	asprintf(&fname, "%s/%s_p%1.8lld.vtr", dirName, pvout->outfile, (LLD)rank);
	fp = fopen(fname,"w");
	if(fp == NULL) SETERRQ1(PETSC_COMM_SELF, 1,"cannot open file %s", fname);
	free(fname);

	// write header
	WriteXMLHeader(fp, "RectilinearGrid");

	// open rectilinear grid data block (write total grid size)
	fprintf(fp, "\t<RectilinearGrid WholeExtent=\"%lld %lld %lld %lld %lld %lld\">\n",
		(LLD)(fs->dsx.starts[rx] + 1), (LLD)(fs->dsx.starts[rx+1] + 1),
		(LLD)(fs->dsy.starts[ry] + 1), (LLD)(fs->dsy.starts[ry+1] + 1),
		(LLD)(fs->dsz.starts[rz] + 1), (LLD)(fs->dsz.starts[rz+1] + 1));

	// open sub-domain (piece) description block
	fprintf(fp, "\t\t<Piece Extent=\"%lld %lld %lld %lld %lld %lld\">\n",
		(LLD)(fs->dsx.starts[rx] + 1), (LLD)(fs->dsx.starts[rx+1] + 1),
		(LLD)(fs->dsy.starts[ry] + 1), (LLD)(fs->dsy.starts[ry+1] + 1),
		(LLD)(fs->dsz.starts[rz] + 1), (LLD)(fs->dsz.starts[rz+1] + 1));

	// write coordinate block
	fprintf(fp, "\t\t\t<Coordinates>\n");

	fprintf(fp, "\t\t\t\t<DataArray type=\"Float32\" Name=\"Coordinates_X\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n", (LLD)offset);
	offset += sizeof(int) + sizeof(float)*(size_t)nx;

	fprintf(fp, "\t\t\t\t<DataArray type=\"Float32\" Name=\"Coordinates_Y\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n", (LLD)offset);
	offset += sizeof(int) + sizeof(float)*(size_t)ny;

	fprintf(fp, "\t\t\t\t<DataArray type=\"Float32\" Name=\"Coordinates_Z\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n", (LLD)offset);
	offset += sizeof(int) + sizeof(float)*(size_t)nz;

	fprintf(fp, "\t\t\t</Coordinates>\n");

	// write cell data block
	fprintf(fp, "\t\t\t<CellData>\n");

	if(pvout->permea)
	{
		fprintf(fp,"\t\t\t<DataArray type=\"Float32\" Name=\"permeability %s\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n",
				scal->lbl_area_si, (LLD)offset);

		offset += sizeof(int) + sizeof(float)*(size_t)fs->nCells;
	}
	if(pvout->poro)
	{
		fprintf(fp,"\t\t\t<DataArray type=\"Float32\" Name=\"porosity [ ]\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n",
				(LLD)offset);

		offset += sizeof(int) + sizeof(float)*(size_t)fs->nCells;
	}
	if(pvout->saturation)
	{
		fprintf(fp,"\t\t\t<DataArray type=\"Float32\" Name=\"saturation [ ]\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n",
				(LLD)offset);

		offset += sizeof(int) + sizeof(float)*(size_t)fs->nCells;
	}
	if(pvout->courant)
	{
		fprintf(fp,"\t\t\t<DataArray type=\"Float32\" Name=\"courant number [ ]\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n",
				(LLD)offset);

		offset += sizeof(int) + sizeof(float)*(size_t)fs->nCells;
	}
	if(pvout->hpe_rf)
	{
		fprintf(fp,"\t\t\t<DataArray type=\"Float32\" Name=\"hpe reduction factor [ ]\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n",
				(LLD)offset);

		offset += sizeof(int) + sizeof(float)*(size_t)fs->nCells;
	}


	fprintf(fp, "\t\t\t</CellData>\n");

	// write node data block
	fprintf(fp, "\t\t\t<PointData>\n");

	if(pvout->velocity)
	{
		fprintf(fp,"\t\t\t<DataArray type=\"Float32\" Name=\"velocity %s\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%lld\"/>\n",
				scal->lbl_velocity, (LLD)offset);

		offset += sizeof(int) + sizeof(float)*(size_t)(nx*ny*nz*3);
	}
	if(pvout->head)
	{
		fprintf(fp,"\t\t\t<DataArray type=\"Float32\" Name=\"head %s\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n",
				scal->lbl_length, (LLD)offset);

		offset += sizeof(int) + sizeof(float)*(size_t)(nx*ny*nz);
	}
	if(pvout->pressure)
	{
		fprintf(fp,"\t\t\t<DataArray type=\"Float32\" Name=\"pressure %s\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n",
				scal->lbl_stress, (LLD)offset);

		offset += sizeof(int) + sizeof(float)*(size_t)(nx*ny*nz);
	}
	if(pvout->residual)
	{
		fprintf(fp,"\t\t\t<DataArray type=\"Float32\" Name=\"residual %s\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%lld\"/>\n",
				scal->lbl_volumetric_rate, (LLD)offset);

		offset += sizeof(int) + sizeof(float)*(size_t)(nx*ny*nz);
	}

	fprintf(fp, "\t\t\t</PointData>\n");

	// close sub-domain and grid blocks
	fprintf(fp, "\t\t</Piece>\n");
	fprintf(fp, "\t</RectilinearGrid>\n");

	// write appended data section
	fprintf(fp, "\t<AppendedData encoding=\"raw\">\n");
	fprintf(fp,"_");

	// coordinate vectors
	ierr = PVOutWriteCoordDir(pvout, &fs->dsx, fp); CHKERRQ(ierr);
	ierr = PVOutWriteCoordDir(pvout, &fs->dsy, fp); CHKERRQ(ierr);
	ierr = PVOutWriteCoordDir(pvout, &fs->dsz, fp); CHKERRQ(ierr);

	// write output vectors
	if(pvout->permea)     { ierr = PVOutWritePermea    (pvout, fp); CHKERRQ(ierr); }
	if(pvout->poro)       { ierr = PVOutWritePoro      (pvout, fp); CHKERRQ(ierr); }
	if(pvout->saturation) { ierr = PVOutWriteSaturation(pvout, fp); CHKERRQ(ierr); }
	if(pvout->courant)    { ierr = PVOutWriteCourant   (pvout, fp); CHKERRQ(ierr); }
	if(pvout->hpe_rf)     { ierr = PVOutWriteHPE       (pvout, fp); CHKERRQ(ierr); }
	if(pvout->velocity)   { ierr = PVOutWriteVelocity  (pvout, fp); CHKERRQ(ierr); }
	if(pvout->head)       { ierr = PVOutWriteHead      (pvout, fp); CHKERRQ(ierr); }
	if(pvout->pressure)   { ierr = PVOutWritePressure  (pvout, fp); CHKERRQ(ierr); }
	if(pvout->residual)   { ierr = PVOutWriteResidual  (pvout, fp); CHKERRQ(ierr); }

	// close appended data section and file
	fprintf(fp, "\n\t</AppendedData>\n");
	fprintf(fp, "</VTKFile>\n");

	// close file
	fclose(fp);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWriteCoordDir"
PetscErrorCode PVOutWriteCoordDir(PVOut *pvout, Discret1D *ds, FILE *fp)
{
	// write coordinate vector to file
	PetscInt    i, r, n, s;
	float       *buff;
	PetscScalar *ncoor, cf;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// get context
	buff  = pvout->buff;
	ncoor = ds->ncoor;
	cf    = pvout->jr->scal->length;

	// get number of node points for output
	GET_OUTPUT_RANGE(r, n, s, (*ds))

	// scale & copy to buffer
	for(i = 0; i < n; i++) buff[i] = (float)(cf*ncoor[i]);

	ierr = OutputBufferWrite(fp, buff, n); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWriteVelocity"
PetscErrorCode PVOutWriteVelocity(PVOut *pvout, FILE *fp)
{
	PetscInt cn(0);

	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = OutputBufferAddComp(
			pvout->jr->fs,
			pvout->jr->vx,
			pvout->jr->scal->velocity,
			pvout->buff,
			3, 0, cn); CHKERRQ(ierr);

	ierr = OutputBufferAddComp(
			pvout->jr->fs,
			pvout->jr->vy,
			pvout->jr->scal->velocity,
			pvout->buff,
			3, 1, cn); CHKERRQ(ierr);

	ierr = OutputBufferAddComp(
			pvout->jr->fs,
			pvout->jr->vz,
			pvout->jr->scal->velocity,
			pvout->buff,
			3, 2, cn); CHKERRQ(ierr);

	ierr = OutputBufferWrite(fp, pvout->buff, cn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWriteHead"
PetscErrorCode PVOutWriteHead(PVOut *pvout, FILE *fp)
{
	PetscInt cn(0);

	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = OutputBufferAddComp(
			pvout->jr->fs,
			pvout->jr->hn,
			pvout->jr->scal->length,
			pvout->buff,
			1, 0, cn); CHKERRQ(ierr);

	ierr = OutputBufferWrite(fp, pvout->buff, cn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWritePressure"
PetscErrorCode PVOutWritePressure(PVOut *pvout, FILE *fp)
{
	PetscInt cn(0);

	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = OutputBufferAddComp(
			pvout->jr->fs,
			pvout->jr->pv,
			pvout->jr->scal->stress,
			pvout->buff,
			1, 0, cn); CHKERRQ(ierr);

	ierr = OutputBufferWrite(fp, pvout->buff, cn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWriteResidual"
PetscErrorCode PVOutWriteResidual(PVOut *pvout, FILE *fp)
{
	PetscInt cn(0);

	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = OutputBufferAddComp(
			pvout->jr->fs,
			pvout->jr->rv,
			pvout->jr->scal->volumetric_rate,
			pvout->buff,
			1, 0, cn); CHKERRQ(ierr);

	ierr = OutputBufferWrite(fp, pvout->buff, cn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWritePermea"
PetscErrorCode PVOutWritePermea(PVOut *pvout, FILE *fp)
{
	float       *buff;
	PetscInt    i, cn;
	PetscScalar *peff, cf;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	buff = pvout->buff;
	peff = pvout->jr->peff;
	cf   = pvout->jr->scal->area_si;
	cn   = pvout->jr->fs->nCells;

	// scale & copy to buffer
	for(i = 0; i < cn; i++) buff[i] = (float)(cf*peff[i]);

	ierr = OutputBufferWrite(fp, buff, cn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWritePoro"
PetscErrorCode PVOutWritePoro(PVOut *pvout, FILE *fp)
{
	float       *buff;
	PetscInt    i, cn, numPar;
	PetscScalar *parm;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	buff   = pvout->buff;
	parm   = pvout->jr->parm;
	cn     = pvout->jr->fs->nCells;
	numPar = pvout->jr->numPar;

	// copy to buffer
	if(numPar == 7)
	{
		for(i = 0; i < cn; i++) buff[i] = (float)(parm[i*numPar + 6]);
	}
	else
	{
		for(i = 0; i < cn; i++) buff[i] = 0.0;
	}

	ierr = OutputBufferWrite(fp, buff, cn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWriteSaturation"
PetscErrorCode PVOutWriteSaturation(PVOut *pvout, FILE *fp)
{
	float       *buff;
	PetscInt    i, cn, numPar;
	PetscScalar *sat;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	buff   = pvout->buff;
	sat    = pvout->jr->sat;
	cn     = pvout->jr->fs->nCells;
	numPar = pvout->jr->numPar;

	// copy to buffer
	if(numPar == 7)
	{
		for(i = 0; i < cn; i++) buff[i] = (float)(sat[i]);
	}
	else
	{
		for(i = 0; i < cn; i++) buff[i] = 1.0;
	}

	ierr = OutputBufferWrite(fp, buff, cn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWriteCourant"
PetscErrorCode PVOutWriteCourant(PVOut *pvout, FILE *fp)
{
	float       *buff;
	PetscInt    i, cn, hpe_use;
	PetscScalar *courant;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	buff     = pvout->buff;
	courant  = pvout->jr->courant;
	cn       = pvout->jr->fs->nCells;
	hpe_use  = pvout->jr->hpe_use;

	// check whether hpe is activated
	if(hpe_use)
	{
		for(i = 0; i < cn; i++) buff[i] = (float)(courant[i]);
	}
	else
	{
		for(i = 0; i < cn; i++) buff[i] = 0.0;
	}

	ierr = OutputBufferWrite(fp, buff, cn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "PVOutWriteHPE"
PetscErrorCode PVOutWriteHPE(PVOut *pvout, FILE *fp)
{
	float       *buff;
	PetscInt    i, cn, hpe_use;
	PetscScalar *hpe_rf;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	buff     = pvout->buff;
	hpe_rf   = pvout->jr->hpe_rf;
	cn       = pvout->jr->fs->nCells;
	hpe_use  = pvout->jr->hpe_use;

	// check whether hpe is activated
	if(hpe_use)
	{
		for(i = 0; i < cn; i++) buff[i] = (float)(hpe_rf[i]);
	}
	else
	{
		for(i = 0; i < cn; i++) buff[i] = 0.0;
	}

	ierr = OutputBufferWrite(fp, buff, cn); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
//........................... Service Functions .............................
//---------------------------------------------------------------------------
void WriteXMLHeader(FILE *fp, const char *file_type)
{
	// write standard header to ParaView XML file
	fprintf(fp,"<?xml version=\"1.0\"?>\n");
#ifdef PETSC_WORDS_BIGENDIAN
	fprintf(fp,"<VTKFile type=\"%s\" version=\"0.1\" byte_order=\"BigEndian\">\n", file_type);
#else
	fprintf(fp,"<VTKFile type=\"%s\" version=\"0.1\" byte_order=\"LittleEndian\">\n", file_type);
#endif
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "UpdatePVDFile"
PetscErrorCode UpdatePVDFile(
		const char *dirName, const char *outfile, const char *ext,
		long int &offset, PetscScalar ttime, PetscInt outpvd)
{
	FILE        *fp;
	char        *fname;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// check whether pvd is requested
	if(!outpvd) PetscFunctionReturn(0);

	// only first process generates this file (WARNING! Bottleneck!)
	if(!ISRankZero(PETSC_COMM_WORLD)) PetscFunctionReturn(0);

	// open outfile.pvd file (write or update mode)
	asprintf(&fname, "%s.pvd", outfile);
	if(!offset) fp = fopen(fname,"w");
	else        fp = fopen(fname,"r+");

	if(fp == NULL) SETERRQ1(PETSC_COMM_SELF, 1,"cannot open file %s", fname);

	free(fname);

	if(!offset)
	{
		// write header
		WriteXMLHeader(fp, "Collection");

		// open time step collection
		fprintf(fp,"<Collection>\n");
	}
	else
	{
		// put the file pointer on the next entry
		ierr = fseek(fp, offset, SEEK_SET); CHKERRQ(ierr);
	}

	// add entry to .pvd file
	fprintf(fp,"\t<DataSet timestep=\"%1.6e\" file=\"%s/%s.%s\"/>\n",
		ttime, dirName, outfile, ext);

	// store current position in the file
	offset = ftell(fp);

	// close time step collection
	fprintf(fp,"</Collection>\n");
	fprintf(fp,"</VTKFile>\n");

	// close file
	fclose(fp);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "OutputBufferWrite"
PetscErrorCode OutputBufferWrite(
		FILE     *fp,
		float    *buff,
		PetscInt  cn)
{
	// dump output buffer contents to disk
	int    nbytes;
	size_t cnt;

	PetscFunctionBegin;

	if(!cn) PetscFunctionReturn(0);

	// compute number of bytes
	nbytes = (int)cn*(int)sizeof(float);

	// dump number of bytes
	cnt = fwrite(&nbytes, sizeof(int), 1, fp);

	if(!cnt)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_FILE_WRITE, "Cannot write to output file");
	}

	// dump buffer contents
	cnt = fwrite(buff, sizeof(float), (size_t)cn, fp);

	if(!cnt)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_FILE_WRITE, "Cannot write to output file");
	}

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "OutputBufferAddComp"
PetscErrorCode OutputBufferAddComp(
		FDSTAG      *fs,    // grid
		Vec          v,     // global vector storing component
		PetscScalar  cf,    // scaling coefficient
		float       *buff,  // output buffer
		PetscInt     ncomp, // number of components
		PetscInt     dir,   // component identifier
		PetscInt    &cn)    // storage counter
{
	// put component of a vector to output buffer
	Vec         lv;
	PetscScalar ***a;
	PetscInt    i, j, k, rx, ry, rz, sx, sy, sz, nx, ny, nz, cnt;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	GET_INIT_LOCAL_VECTOR(fs->DA_COR, v, lv, a)

	// get output loop bounds
	GET_OUTPUT_RANGE(rx, nx, sx, fs->dsx)
	GET_OUTPUT_RANGE(ry, ny, sy, fs->dsy)
	GET_OUTPUT_RANGE(rz, nz, sz, fs->dsz)

	// set counter
	cnt = dir;

	// copy vector component to buffer
	START_STD_LOOP
	{
		// write
		buff[cnt] = (float)(cf*a[k][j][i]);

		// update counter
		cnt += ncomp;
	}
	END_STD_LOOP

	RESTORE_LOCAL_VECTOR(fs->DA_COR, lv, a)

	// update number of elements in the buffer
	cn += nx*ny*nz;

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
