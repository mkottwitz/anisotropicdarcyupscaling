//---------------------------------------------------------------------------
//...................   FDSTAG JACOBIAN AND RESIDUAL  .......................
//---------------------------------------------------------------------------
#ifndef __JacRes_h__
#define __JacRes_h__

struct FB;
struct Scaling;
struct TSSol;
struct FDSTAG;
struct FreeSurf;
struct BCCtx;

//---------------------------------------------------------------------------
//.............. FDSTAG Jacobian and residual evaluation context ............
//---------------------------------------------------------------------------

struct JacRes
{
	// external handles
	Scaling  *scal;  // scaling
	TSSol    *ts;    // time-stepping parameters
	FDSTAG   *fs;    // grid layout
	FreeSurf *surf;  // free surface
	BCCtx    *bc;    // boundary condition context

	// basic parameters
	PetscScalar grav;      // gravity acceleration
	PetscScalar eta;       // fluid viscosity
	PetscScalar rho;       // fluid density
	PetscScalar beta;      // fluid compressibility
	PetscScalar phi;       // matrix compressibility

	// Van-Genuchten model parameters
	PetscScalar alpha;     // pre-factor
	PetscScalar l;         // exponent
	PetscScalar theta_r;   // residual fluid content
	PetscScalar krr;       // residual relative permeability
	PetscScalar k_ar;      // absolute residual permeability of the vadose zone
	PetscScalar dh;        // fluid content differentiation step

	// steady-state sopping parameters
	PetscInt    check_ss;      // stop after steady state is reached activation flag
	PetscScalar check_ss_tol;  // tolerance, below which the difference of current head to previous head is considered as neglible, i.e. steady state is reached

	// dynamic VG-parameter assignment
	PetscInt    vg_dyn_use;    // dynamic assignment of VG alpha and l parameter, based on empirical relation to hydraulic conductivity, see: https://www.sciencedirect.com/science/article/pii/S1674775522000610
	PetscScalar vg_dyn_a_p;    // prefactor of dynamic alpha model (power law model of the form: b + p*X^n)
	PetscScalar vg_dyn_a_b;    // base      of dynamic alpha model (power law model of the form: b + p*X^n)
	PetscScalar vg_dyn_a_n;    // exponent  of dynamic alpha model (power law model of the form: b + p*X^n)
	PetscScalar vg_dyn_l_p;    // prefactor of dynamic exponent model (power law model of the form: b + p*X^n)
	PetscScalar vg_dyn_l_b;    // base      of dynamic exponent model (power law model of the form: b + p*X^n)
	PetscScalar vg_dyn_l_n;    // exponent  of dynamic exponent model (power law model of the form: b + p*X^n)

	// Hydraulic Property Evolution (HPE) parameters (parameters for smoothed piecewise linear function)
	PetscInt    hpe_use;   // activation flag
	PetscScalar hpe_a;     // max value
	PetscScalar hpe_r;     // orders of magnitude bandwith of linear increase from 0 to hpe_a (in logarithmic space)
	PetscScalar hpe_beta;  // smoothing or approximation degree to piecwiese linear function (high values == piecwise linear, small values == smeared out)
	PetscScalar hpe_r50;   // log(c) value (x-axis location) of 50% of the linear increase from 0 to hpe_a

	// flags
	PetscInt    actGrav;   // gravity activation flag (fixed direction is z)
	PetscInt    getPermea; // effective permeability computation activation flag

	//========================
	// Darcy solver parameters
	//========================

	Mat MF; // finite-difference matrix-free Jacobian

	Vec hn;    // hydraulic head history (output)
	Vec hdiff; // vector holding the difference between hn and current h (steady state check)
	Vec pv;    // pressure               (output)
	Vec rv;    // residual               (output)

	Vec vx, vy, vz; // Darcy velocities (global)

	//================
	// cell parameters
	//================
	PetscInt     numPar;    // number of cell parameters
	PetscScalar *parm;      // cell parameters (permeabilty, porosity)
	PetscScalar *peff;      // permeability tensor norm
	PetscScalar *sat;       // cell saturation (output)
	PetscScalar *courant;   // courant number (output)
	PetscScalar *hpe_rf;    // hpe reduction factor (output)
	PetscScalar  gvel[3];   // velocity volume integrals
	PetscScalar  g_wvol;    // global water volume
	PetscScalar  g_wvol_n;  // global water volume of previous timestep

};

// create residual & Jacobian evaluation context
PetscErrorCode JacResCreate(JacRes *jr, FB *fb);

// allocate vectors
PetscErrorCode JacResCreateData(JacRes *jr);

// destroy residual & Jacobian evaluation context
PetscErrorCode JacResDestroy(JacRes *jr);

// read cell parameters from files in parallel
PetscErrorCode JacResReadParm(JacRes *jr, FB *fb);

// compute effective permeability
PetscErrorCode JacResGetEffPerm(JacRes *jr);

// initialize head history
PetscErrorCode JacResInitGuess(JacRes *jr);

//// initialize saturation per cell
//PetscErrorCode JacResInitSaturation(JacRes *jr);

// assemble Jacobian
PetscErrorCode JacResGetJac(JacRes *jr, Vec x, Mat Pmat);

// assemble residual
PetscErrorCode JacResGetRes(JacRes *jr, Vec x, Vec f, PetscInt getFlux);

// get well fluxes
PetscErrorCode JacResGetWellFlux(JacRes *jr, Vec f);

// get observation well heads
PetscErrorCode JacResGetObservationHeads(JacRes *jr, PetscInt init);

// check for steady-state
PetscErrorCode JacResCheckSteadyState(JacRes *jr, Vec x);

// compute element-average Darcy velocities
PetscErrorCode JacResGetDarcyVel(JacRes *jr, Vec x);

// compute effective permeability
PetscErrorCode JacResGetPermea(JacRes *jr, char *outfile);

// compute pressure from hydraulic head
PetscErrorCode JacResGetPress(JacRes *jr, Vec x);

// compute pressure from hydraulic head
PetscErrorCode JacResTruncateHead(JacRes *jr, Vec x);

// integrate cell saturation from nodal hydraulic head
PetscErrorCode JacResGetSaturation(JacRes *jr, Vec x);

// update cell permeability and porosity based on velocity throughput for hydraulic property evolution (HPE)
PetscErrorCode JacResUpdatePoroPerm(JacRes *jr);

//---------------------------------------------------------------------------
// Van Genuchten functions
//---------------------------------------------------------------------------

// compute Van-Genuchten fluid content
PetscScalar getFluidConent(JacRes *jr, PetscScalar h, PetscScalar z, PetscScalar n, PetscScalar K_eff);

// compute Van-Genuchten relative permeability
PetscScalar getRelPerm(JacRes *jr, PetscScalar theta, PetscScalar n, PetscScalar k_eff, PetscScalar K_eff);

//---------------------------------------------------------------------------
// hydraulic property evolution (HPE) functions
//---------------------------------------------------------------------------

// compute poro-perm reduction factor based on the cells courant number
PetscScalar getReductionFactor(JacRes *jr, PetscScalar c);

//---------------------------------------------------------------------------
// SNES FUNCTIONS
//---------------------------------------------------------------------------

// compute residual vector
PetscErrorCode FormResidual(SNES snes, Vec x, Vec f, void *ctx);

// compute Jacobian matrix and preconditioner
PetscErrorCode FormJacobian(SNES snes, Vec x, Mat Amat, Mat Pmat, void *ctx);

//---------------------------------------------------------------------------

#endif
