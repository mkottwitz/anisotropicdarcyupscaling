//---------------------------------------------------------------------------
//......................  FINITE ELEMENT FUNCTIONS   ..........................
//---------------------------------------------------------------------------
#ifndef __FEA_h__
#define __FEA_h__

// get quadrature rule of element
PetscErrorCode getQuad(
			PetscScalar *px1,  // x coordinate of quadrature points
			PetscScalar *px2,  // y coordinate of quadrature points
			PetscScalar *px3,  // z coordinate of quadrature points
			PetscScalar *pwt); // weights of quadrature points

// get linear shape functions and from element coordinates
PetscErrorCode getShape(
			PetscScalar  x1, // integration point coordinate (x)
			PetscScalar  x2, // integration point coordinate (y)
			PetscScalar  x3, // integration point coordinate (z)
			PetscScalar *N); // node shape functions

// get global shape function derivatives & Jacobian determinant
PetscErrorCode getShapeDeriv(
		PetscScalar  x1,    // integration point coordinate (x)
		PetscScalar  x2,    // integration point coordinate (y)
		PetscScalar  x3,    // integration point coordinate (z)
		PetscScalar *cX1,   // node-coordinates of element  (x)
		PetscScalar *cX2,   // node-coordinates of element  (y)
		PetscScalar *cX3,   // node-coordinates of element  (z)
		PetscScalar *dX1,   // shape function derivatives   (x)
		PetscScalar *dX2,   // shape function derivatives   (y)
		PetscScalar *dX3,   // shape function derivatives   (z)
		PetscScalar &detJ); // Jacobian determinant

//---------------------------------------------------------------------------
#endif
