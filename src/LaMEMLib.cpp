//---------------------------------------------------------------------------
// LAMEM LIBRARY MODE ROUTINE
//---------------------------------------------------------------------------
#include "LaMEM.h"
#include "parsing.h"
#include "scaling.h"
#include "tssolve.h"
#include "tools.h"
#include "fdstag.h"
#include "bc.h"
#include "JacRes.h"
#include "surf.h"
#include "paraViewOutBin.h"
#include "paraViewOutSurf.h"
#include "LaMEMLib.h"
//---------------------------------------------------------------------------

// git clone https://bitbucket.org/mkottwitz/anisotropicdarcyupscaling.git

#undef __FUNCT__
#define __FUNCT__ "LaMEMLibMain"
PetscErrorCode LaMEMLibMain(void)
{
	LaMEMLib       lm;
	RunMode        mode;
	PetscBool      found;
	char           str[_str_len_];
	PetscLogDouble cputime_start, cputime_end;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// start code
	ierr = PetscTime(&cputime_start); CHKERRQ(ierr);

	PetscPrintf(PETSC_COMM_WORLD,"-------------------------------------------------------------------------- \n");
	PetscPrintf(PETSC_COMM_WORLD,"                           DARCY FLOW SOLVER                               \n");
	PetscPrintf(PETSC_COMM_WORLD,"     Compiled: Date: %s - Time: %s 	    \n",__DATE__,__TIME__ );
	PetscPrintf(PETSC_COMM_WORLD,"-------------------------------------------------------------------------- \n");
	PetscPrintf(PETSC_COMM_WORLD,"                     FINITE ELEMENT IMPLEMENTATION                         \n");
	PetscPrintf(PETSC_COMM_WORLD,"-------------------------------------------------------------------------- \n");

	// read run mode
	mode = _NORMAL_;

	ierr = PetscOptionsGetCheckString("-mode", str, &found); CHKERRQ(ierr);

	if(found)
	{
		if     (!strcmp(str, "normal"))    mode = _NORMAL_;
		else if(!strcmp(str, "dry_run"))   mode = _DRY_RUN_;
		else if(!strcmp(str, "save_grid")) mode = _SAVE_GRID_;
		else SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Incorrect run mode type: %s", str);
	}

	//===========
	// INITIALIZE
	//===========

	// clear
	ierr = PetscMemzero(&lm, sizeof(LaMEMLib)); CHKERRQ(ierr);

	// setup cross-references between library objects
	ierr = LaMEMLibSetLinks(&lm); CHKERRQ(ierr);

	if(mode == _SAVE_GRID_)
	{
		// save grid & exit
		ierr = LaMEMLibSaveGrid(&lm); CHKERRQ(ierr);

		PetscFunctionReturn(0);
	}
	if(mode == _NORMAL_ || mode == _DRY_RUN_)
	{
		// create library objects
		ierr = LaMEMLibCreate(&lm); CHKERRQ(ierr);
	}

	//======
	// SOLVE
	//======

	if(mode == _DRY_RUN_)
	{
		// compute initial residual, output & stop
		ierr = LaMEMLibDryRun(&lm); CHKERRQ(ierr);
	}
	else if(mode == _NORMAL_)
	{
		// solve coupled nonlinear equations
		ierr = LaMEMLibSolve(&lm); CHKERRQ(ierr);
	}

	// destroy library objects
	ierr = LaMEMLibDestroy(&lm); CHKERRQ(ierr);

	PetscTime(&cputime_end);

	PetscPrintf(PETSC_COMM_WORLD, "Total solution time : %g (sec) \n", cputime_end - cputime_start);
	PetscPrintf(PETSC_COMM_WORLD, "--------------------------------------------------------------------------\n");

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "LaMEMLibCreate"
PetscErrorCode LaMEMLibCreate(LaMEMLib *lm)
{
	FB *fb;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// load input file
	ierr = FBLoad(&fb, PETSC_TRUE); CHKERRQ(ierr);

	// create scaling object
	ierr = ScalingCreate(&lm->scal, fb); CHKERRQ(ierr);

	// create parallel grid
	ierr = FDSTAGCreate(&lm->fs, fb); CHKERRQ(ierr);

	// create free surface grid
	ierr = FreeSurfCreate(&lm->surf, fb); CHKERRQ(ierr);

	// create residual & Jacobian evaluation context
	ierr = JacResCreate(&lm->jr, fb); CHKERRQ(ierr);

	// create time stepping object
	ierr = TSSolCreate(&lm->ts, fb); CHKERRQ(ierr);

	// create boundary condition context
	ierr = BCCreate(&lm->bc, fb); CHKERRQ(ierr);

	// create output object for all requested output variables
	ierr = PVOutCreate(&lm->pvout, fb); CHKERRQ(ierr);

	// create output object for the free surface
	ierr = PVSurfCreate(&lm->pvsurf, fb); CHKERRQ(ierr);

	// destroy file buffer
	ierr = FBDestroy(&fb); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "LaMEMLibSaveGrid"
PetscErrorCode LaMEMLibSaveGrid(LaMEMLib *lm)
{
	FB *fb;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// load input file
	ierr = FBLoad(&fb, PETSC_TRUE); CHKERRQ(ierr);

	// create scaling object
	ierr = ScalingCreate(&lm->scal, fb); CHKERRQ(ierr);

	// create parallel grid
	ierr = FDSTAGCreate(&lm->fs, fb); CHKERRQ(ierr);

	// save processor partitioning
	ierr = FDSTAGSaveGrid(&lm->fs); CHKERRQ(ierr);

	// destroy parallel grid
	ierr = FDSTAGDestroy(&lm->fs); CHKERRQ(ierr);

	// destroy file buffer
	ierr = FBDestroy(&fb); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "LaMEMLibDestroy"
PetscErrorCode LaMEMLibDestroy(LaMEMLib *lm)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = FDSTAGDestroy   (&lm->fs);     CHKERRQ(ierr);
	ierr = FreeSurfDestroy (&lm->surf);   CHKERRQ(ierr);
	ierr = BCDestroy       (&lm->bc);     CHKERRQ(ierr);
	ierr = JacResDestroy   (&lm->jr);     CHKERRQ(ierr);
	ierr = PVOutDestroy    (&lm->pvout);  CHKERRQ(ierr);
	ierr = PVSurfDestroy   (&lm->pvsurf); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "LaMEMLibSetLinks"
PetscErrorCode LaMEMLibSetLinks(LaMEMLib *lm)
{
	//======================================================================
	// LaMEM library object initialization sequence
	//
	//                         Scaling
	//                            |
	//                          TSSol
	//                            |
	//                         FDSTAG
	//                            |
	//                         FreeSurf
	//                            |
	//                          BCCtx
	//                            |
	//                          JacRes
	//                            |
	//                        ---------
	//                        |       |
	//                     PVOut   PVSurf
	//======================================================================

	// setup cross-references between library objects

	// ... This is the house that Jack built ...

	PetscFunctionBegin;
	// TSSol
	lm->ts.scal     = &lm->scal;
	lm->ts.surf     = &lm->surf;
	// FDSTAG
	lm->fs.scal     = &lm->scal;
	// FreeSurf
	lm->surf.jr     = &lm->jr;
	// BCCtx
	lm->bc.scal     = &lm->scal;
	lm->bc.ts       = &lm->ts;
	lm->bc.fs       = &lm->fs;
	lm->bc.jr       = &lm->jr;
	// JacRes
	lm->jr.scal     = &lm->scal;
	lm->jr.ts       = &lm->ts;
	lm->jr.fs       = &lm->fs;
	lm->jr.surf     = &lm->surf;
	lm->jr.bc       = &lm->bc;
	// PVOut
	lm->pvout.jr    = &lm->jr;
	// PVSurf
	lm->pvsurf.surf = &lm->surf;

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "LaMEMLibSaveOutput"
PetscErrorCode LaMEMLibSaveOutput(LaMEMLib *lm)
{
	//==================
	// Save data to disk
	//==================

	Scaling        *scal;
	TSSol          *ts;
	PetscScalar    time;
	PetscInt       step;
	char           *dirName;
	PetscLogDouble t;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	scal = &lm->scal;
	ts   = &lm->ts;

	if(!TSSolIsOutput(ts)) PetscFunctionReturn(0);

	PetscPrintf(PETSC_COMM_WORLD,"--------------------------------------------------------------------------\n");

	PrintStart(&t, "Saving output", NULL);

	time    = ts->time*scal->time;
	step    = ts->istep;

	// create directory (encode current time & step number)
	asprintf(&dirName, "Timestep_%1.8lld_%1.8e", (LLD)step, time);

	// create output directory
	ierr = DirMake(dirName); CHKERRQ(ierr);

	// grid ParaView output
	ierr = PVOutWriteTimeStep(&lm->pvout, dirName, time); CHKERRQ(ierr);

	// free surface ParaView output
	ierr = PVSurfWriteTimeStep(&lm->pvsurf, dirName, time); CHKERRQ(ierr);

	// compute and output effective permeability
	ierr = JacResGetPermea(&lm->jr, lm->pvout.outfile); CHKERRQ(ierr);

	// clean up
	free(dirName);

	PrintDone(t);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "LaMEMLibSolve"
PetscErrorCode LaMEMLibSolve(LaMEMLib *lm)
{
	JacRes         *jr;
	FDSTAG         *fs;
	SNES           snesp, snesn;
	KSP            ksp;
	Mat            Pmat;
	Vec            x;
	PetscLogDouble t;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	jr = &lm->jr;
	fs = &lm->fs;

	// create matrix
	ierr = DMCreateMatrix(fs->DA_COR, &Pmat);                                    CHKERRQ(ierr);
	ierr = MatSetOption(Pmat, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_TRUE);       CHKERRQ(ierr);
	ierr = MatSetOption(Pmat, MAT_NEW_NONZERO_LOCATION_ERR, PETSC_TRUE);         CHKERRQ(ierr);
	ierr = MatSetOption(Pmat, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);             CHKERRQ(ierr);

	// setup nonlinear solver (Picard)
	ierr = SNESCreate(PETSC_COMM_WORLD, &snesp);                                  CHKERRQ(ierr);
	ierr = SNESSetDM(snesp, fs->DA_COR);                                          CHKERRQ(ierr);
	ierr = SNESSetFunction(snesp, NULL, &FormResidual, jr);                       CHKERRQ(ierr);
	ierr = SNESSetJacobian(snesp, Pmat, Pmat, &FormJacobian,  jr);                CHKERRQ(ierr);
	ierr = SNESSetOptionsPrefix(snesp, "p_");                                     CHKERRQ(ierr);
	ierr = SNESSetFromOptions(snesp);                                             CHKERRQ(ierr);

	// setup linear solver (Picard)
	ierr = SNESGetKSP(snesp, &ksp);                                              CHKERRQ(ierr);
	ierr = KSPSetDMActive(ksp, PETSC_FALSE);                                     CHKERRQ(ierr);
	ierr = KSPSetOptionsPrefix(ksp, "ps_");                                      CHKERRQ(ierr);
	ierr = KSPSetFromOptions(ksp);                                               CHKERRQ(ierr);

	// setup nonlinear solver (Newton)
	ierr = SNESCreate(PETSC_COMM_WORLD, &snesn);                                 CHKERRQ(ierr);
	ierr = SNESSetDM(snesn, fs->DA_COR);                                         CHKERRQ(ierr);
	ierr = SNESSetFunction(snesn, NULL, &FormResidual, jr);                      CHKERRQ(ierr);
	ierr = SNESSetJacobian(snesn, Pmat, Pmat, &FormJacobian,  jr);               CHKERRQ(ierr);
	ierr = SNESSetOptionsPrefix(snesn, "n_");                                    CHKERRQ(ierr);
	ierr = SNESSetFromOptions(snesn);                                            CHKERRQ(ierr);

	// setup linear solver (Newton)
	ierr = SNESGetKSP(snesn, &ksp);                                              CHKERRQ(ierr);
	ierr = KSPSetDMActive(ksp, PETSC_FALSE);                                     CHKERRQ(ierr);
	ierr = KSPSetOptionsPrefix(ksp, "ps_");                                      CHKERRQ(ierr);
	ierr = KSPSetFromOptions(ksp);                                               CHKERRQ(ierr);

	// compute/set initial guess
	ierr = JacResInitGuess(jr);	                                                 CHKERRQ(ierr);

	// save initial output (t=0) for free surface inspection
    ierr = LaMEMLibSaveOutput(lm);                                               CHKERRQ(ierr);

	// setup solution vector
	ierr = DMCreateGlobalVector(fs->DA_COR, &x);                                 CHKERRQ(ierr);

	// initialize solution vector
	ierr = VecCopy(jr->hn, x);                                                   CHKERRQ(ierr);

	//===============
	// TIME STEP LOOP
	//===============

	while(!TSSolIsDone(&lm->ts))
	{
		PetscTime(&t);

		// apply ALL boundary conditions
		ierr = BCApply(&lm->bc, x); CHKERRQ(ierr);

		// solve nonlinear problem
		ierr = SNESSolve(snesp, NULL, x); CHKERRQ(ierr);

		// print analyze convergence/divergence reason & iteration count
		ierr = SNESPrintConvergedReason(snesp, t); CHKERRQ(ierr);

		// solve nonlinear problem
		ierr = SNESSolve(snesn, NULL, x); CHKERRQ(ierr);

		// print analyze convergence/divergence reason & iteration count
		ierr = SNESPrintConvergedReason(snesn, t); CHKERRQ(ierr);

		// evaluate final residual & well fluxes
		ierr = JacResGetRes(jr, x, jr->rv, 1); CHKERRQ(ierr);

		// compute Darcy velocities & integrals
		ierr = JacResGetDarcyVel(jr, x); CHKERRQ(ierr);

		// update time stamp and counter
		ierr = TSSolStepForward(&lm->ts); CHKERRQ(ierr);

		// compute saturation
		ierr = JacResGetSaturation(jr, x); CHKERRQ(ierr);

		// update permeabilities and porosities, if activated
		ierr = JacResUpdatePoroPerm(jr); CHKERRQ(ierr);

		// upate time step
		ierr = TSSolGetStep(&lm->ts); CHKERRQ(ierr);

		//==================
		// Save data to disk
		//==================
	
		// save output
		ierr = LaMEMLibSaveOutput(lm); CHKERRQ(ierr);

		//==================
		// Stop simulation if steady state is reached
		//==================

		// check if steady state is reached and stop simulation
		ierr = JacResCheckSteadyState(jr, x); CHKERRQ(ierr);

		// update solution history after check for steady state
		ierr = VecCopy(x, jr->hn); CHKERRQ(ierr);

	}

	//======================
	// END OF TIME STEP LOOP
	//======================

	ierr = SNESDestroy(&snesp);  CHKERRQ(ierr);
	ierr = SNESDestroy(&snesn);  CHKERRQ(ierr);
	ierr = VecDestroy (&x);      CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "LaMEMLibDryRun"
PetscErrorCode LaMEMLibDryRun(LaMEMLib *lm)
{
	JacRes *jr;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	jr = &lm->jr;

	// apply ALL boundary conditions
	ierr = BCApply(&lm->bc, jr->hn); CHKERRQ(ierr);

	// evaluate initial residual
	ierr = JacResGetRes(jr, jr->hn, jr->rv, 1); CHKERRQ(ierr);

	// compute Darcy velocities & integrals
	ierr = JacResGetDarcyVel(jr, jr->hn); CHKERRQ(ierr);

	// compute saturation of nodes for output
	ierr = JacResGetSaturation(jr, jr->hn); CHKERRQ(ierr);

	// save output
	ierr = LaMEMLibSaveOutput(lm); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "SNESPrintConvergedReason"
PetscErrorCode SNESPrintConvergedReason(SNES snes, PetscLogDouble t_beg)
{
	PetscLogDouble      t_end;
	SNESConvergedReason reason;
	PetscInt            its;
	KSP                 ksp;
	KSPConvergedReason  ksp_reason;
	PetscInt            div_severe;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// set flag for severe divergence
	div_severe = 0;

	PetscTime(&t_end);

	ierr = SNESGetIterationNumber(snes, &its);    CHKERRQ(ierr);
	ierr = SNESGetConvergedReason(snes, &reason);  CHKERRQ(ierr);

	PetscPrintf(PETSC_COMM_WORLD, "--------------------------------------------------------------------------\n");

	if(reason < 0)
	{
		PetscPrintf(PETSC_COMM_WORLD, "**************   NONLINEAR SOLVER FAILED TO CONVERGE!   ****************** \n");
		PetscPrintf(PETSC_COMM_WORLD, "--------------------------------------------------------------------------\n");
	}

	if(reason == SNES_CONVERGED_FNORM_ABS)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Convergence Reason : ||F|| < atol \n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_CONVERGED_FNORM_RELATIVE)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Convergence Reason : ||F|| < rtol*||F_initial|| \n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_CONVERGED_SNORM_RELATIVE)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Convergence Reason : Newton computed step size small; || delta x || < stol || x ||\n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_CONVERGED_ITS)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Convergence Reason : maximum iterations reached\n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_CONVERGED_TR_DELTA)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Convergence Reason : SNES_CONVERGED_TR_DELTA\n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_CONVERGED_ITERATING)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Convergence Reason : SNES_CONVERGED_ITERATING\n"); CHKERRQ(ierr);
	}

	// DIVERGENCE

	else if(reason == SNES_DIVERGED_FUNCTION_DOMAIN)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Divergence Reason  : the new x location passed the function is not in the domain of F\n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_DIVERGED_FUNCTION_COUNT)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Divergence Reason  : too many function evaluations\n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_DIVERGED_LINEAR_SOLVE)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Divergence Reason  : the linear solve failed\n"); CHKERRQ(ierr);

		// detect severe divergence reason
		ierr = SNESGetKSP(snes, &ksp);                  CHKERRQ(ierr);
		ierr = KSPGetConvergedReason(ksp, &ksp_reason); CHKERRQ(ierr);

		if(ksp_reason == KSP_DIVERGED_BREAKDOWN
		|| ksp_reason == KSP_DIVERGED_INDEFINITE_PC
		|| ksp_reason == KSP_DIVERGED_NANORINF
		|| ksp_reason == KSP_DIVERGED_INDEFINITE_MAT)
		{
			div_severe = 1;
		}
	}
	else if(reason == SNES_DIVERGED_FNORM_NAN)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Divergence Reason  : residual norm is NAN\n"); CHKERRQ(ierr);

		div_severe = 1;
	}
	else if(reason == SNES_DIVERGED_MAX_IT)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Divergence Reason  : maximum iterations reached\n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_DIVERGED_LINE_SEARCH)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Divergence Reason  : the line search failed\n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_DIVERGED_INNER)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Divergence Reason  : the inner solve failed\n"); CHKERRQ(ierr);
	}
	else if(reason == SNES_DIVERGED_LOCAL_MIN)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD, "SNES Divergence Reason  : || J^T b || is small, implies converged to local minimum of F\n"); CHKERRQ(ierr);
	}

	// stop if severe divergence reason detected
	if(div_severe)
	{
		SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Severe divergence reason detected (see above)");
	}

	PetscPrintf(PETSC_COMM_WORLD, "Number of iterations    : %lld\n", (LLD)its);

	PetscPrintf(PETSC_COMM_WORLD, "SNES solution time      : %g (sec)\n", t_end - t_beg);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
