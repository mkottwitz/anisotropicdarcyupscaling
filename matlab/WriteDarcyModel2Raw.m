%% Convert CT-scans from .tif images to 3D raw image data

clc, clear all , close all

%% Add important paths

% lamem matlab path
addpath('/home/anton/PROG/LaMEM/matlab')

% data path

pathToWrite = ('/home/oskar/LaMEM/TestData');

%% Define parameters

% element domain
nex = 4;
ney = 4;
nez = 4;

SizeOfVoxel = 3.528e-6;     % micrometers

filename   = [pathToWrite,'/aniPermea3D'];     % file-name to store voxel-based data set

Is64BIT        = 0;

%==========================================================================

%% generate peremability tensor for every element, incorporate import of matrix file or from lamem output

permeabilities = zeros(nex,ney,nez,7);

%fill permeability array with 6 values (symmetrix permeability tensor) - currently set to isotropic permeability
for k = 1:nez
    for j = 1:ney
        for i = 1:nex
            
%             if i ~= j
%                 % k_xx
%                 permeabilities(i,j,k,1) = 0;%rand(1);
%                 % k_yy
%                 permeabilities(i,j,k,2) = 0;%rand(1);
%                 % k_zz
%                 permeabilities(i,j,k,3) = 0;%rand(1);
% 
%                 % k_xy, k_yz
%                 permeabilities(i,j,k,4) = 0;%rand(1)*0.01;
%                 % k_xz, k_zx
%                 permeabilities(i,j,k,5) = 0;%rand(1)*0.01;
%                 % k_yz, k_zy
%                 permeabilities(i,j,k,6) = 0;%rand(1)*0.01;
%             else
                
            %fprintf('constrained cell \n')
            permeabilities(i,j,k,1) = 1e-5;
            % k_yy
            permeabilities(i,j,k,2) = 1e-5;
            % k_zz
            permeabilities(i,j,k,3) = 1e-5;

            % k_xy, k_yz
            permeabilities(i,j,k,4) = 0;
            % k_xz, k_zx
            permeabilities(i,j,k,5) = 0;
            % k_yz, k_zy
            permeabilities(i,j,k,6) = 0;
                
%             end
            
            % add sources
            permeabilities(i,j,k,7) = 0;
        end
    end
end


%% write raw file

fprintf('Dumping raw image to disk ... \n')

fp = fopen(strcat(filename, '-', num2str(nex), 'x', num2str(ney), 'x', num2str(nez), '.raw'), 'w');

if Is64BIT
    fwrite(fp, permeabilities, 'float64');
else
    fwrite(fp, permeabilities, 'float32');
end

fclose(fp);

% compute and print model size for creating partitioning file
R = SizeOfVoxel*((nex)/2);
S = SizeOfVoxel*((ney)/2);
T = SizeOfVoxel*((nez)/2);


fprintf('Please create partitioning file with the following domain sizes: \n')

fprintf('coord_x = %g %g\n', -R, R);
fprintf('coord_y = %g %g\n', -S, S);
fprintf('coord_z = %g %g\n', -T, T);
