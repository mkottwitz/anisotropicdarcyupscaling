%% Generate parallel constraint cells flags from 3D raw image data

clc, clear all , close all

%% Add important paths

% lamem matlab path
addpath('/home/oskar/LaMEM/LaMEM_Darcy/matlab')

% path that holds data
addpath('/home/oskar/LaMEM/TestData')

%% Defining Parameters
mesh_file      = 'ProcessorPartitioning_1cpu_1.1.1.bin';
permea_file     = 'aniPermea3D-4x4x4.raw';
npx            = 4;        % x-pixel grid size
npy            = 4;        % y-pixel grid size
npz            = 4;        % z-pixel grid size
SizeOfVoxel    = 3.528e-6;   % pixel physical resolution
Is64BIT        = 0;

%==========================================================================

%% LOAD DATA

% Read raw iamge data file
fp  = fopen(permea_file, 'r');

if Is64BIT
    P3D = fread(fp,'float64');
else
    P3D = fread(fp,'float32');
end

P3D = reshape(P3D, npx,npy,npz,7);


% Read processor partitioning
[P] = GetProcessorPartitioning(mesh_file, Is64BIT);

% get grid data
nx     = P.nnodx-1;
ny     = P.nnody-1;
nz     = P.nnodz-1;

% check whether resolution is too high (silly)
if(nx > npx || ny > npy || nz > npz)
    error('Grid resolution cannot exceed pixel resolution');
end


%% STORE FILES

if ~isdir('darcy')
    mkdir darcy
end

if ~isdir('bc')
    mkdir bc
end


% get partitioning data
Nprocx = P.Nprocx;
Nprocy = P.Nprocy;
Nprocz = P.Nprocz;
ix     = P.ix;
iy     = P.iy;
iz     = P.iz;

% initialize subdomain ID
num = 0;

for k = 1:Nprocz
    for j = 1:Nprocy
        for i = 1:Nprocx
            
            % get cell subset of current subdomain
            %buff = P3D(:,:,:,ki);
            P3D_kxx = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1,1);
            P3D_kyy = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1,2);
            P3D_kzz = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1,3);
            P3D_kxy = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1,4);
            P3D_kxz = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1,5);
            P3D_kyz = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1,6);
            P3D_s = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1,7);
            
            [lx, ly, lz] = size(P3D_s);

            fname = sprintf('./darcy/cd.%1.8d.txt', num);

            fileID = fopen(fname,'w');

            %write header
            fprintf(fileID, 'ii\bji\bki\bk_xxi\bk_yyi\bk_zzi\bk_xyi\bk_xzi\bk_yzi\bsi\b\n');


            for kk = 1:lz
                for jj = 1:ly
                    for ii = 1:lx
                        kxx=P3D_kxx(i,j,k);
                        kyy=P3D_kyy(i,j,k);
                        kzz=P3D_kzz(i,j,k);
                        kxy=P3D_kxy(i,j,k);
                        kxz=P3D_kxz(i,j,k);
                        kyz=P3D_kyz(i,j,k);
                        s=P3D_s(i,j,k);
                        
                        fprintf(fileID, '%i\b%i\b%i\b%.8e\b%.8e\b%.8e\b%.8e\b%.8e\b%.8e\b%.8e\n', ii-1,jj-1,kk-1,kxx,kyy,kzz,kxy,kxz,kyz,s);
                    end
                end
            end
            
            fclose(fileID);
         
            % write boundary flags -> 1 = matrix (impermeable), 0 =
            % fracture (permeable)
            [sx, sy, sz, ~] = size(P3D);
            
            P3DF = zeros(sx,sy,sz);
            
            for kk = 1:sz
                for jj = 1:sy
                    for ii = 1:sx
                        a=0;
                        
                        if sum(any(P3D(i,j,k,:))) ~= 0
                            P3DF(i,j,k) = 0;
                        else
                            P3DF(i,j,k) = 1;
                        end
                        
                        
                    end
                end
            end
            
            % open file
            fname = sprintf('./bc/cdb.%1.8d.dat', num);
            
            disp(['Writing file -> ', fname])
            
            fp = fopen(fname, 'w');
            
            % dump data on disk
            fwrite(fp, P3DF, 'uint8');
            
            fclose(fp);
            
            clear P3DS fname fp P3DF
            
            % update processor ID
            num = num + 1;
            
        end
    end
end


