import scipy.io as sio
import ipdb
import csv
import sys,os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

import h5py
import hdf5storage
import vtk
from vtk.util.numpy_support import vtk_to_numpy




filename    = "Timestep_00000100_1.00000000e+07/permea_surf.pvts"

#read pvtr output file from lamem 
reader=vtk.vtkXMLGenericDataObjectReader()
reader.SetFileName(filename)#"Timestep_00000001_1.00000000e+00/permea.pvtr"
reader.Update()
coordinates     = reader.GetOutput().GetPoints().GetData()

## probe data
coordinates_np  = vtk_to_numpy(coordinates)
x,y,z           = coordinates_np[:,0] , coordinates_np[:,1] , coordinates_np[:,2]
sliceInds = np.where((y == 0))
xplot = x[sliceInds]
zplot = z[sliceInds]

## predefine constants
g          = 9.81      # gravitational acceleration [m/s^2]
rho        = 997.0     # fluid density for depth-dependent density model [kg/m^3]
eta        = 8.9e-4    # dynamic viscosity of the fluid [Pa]
k          = 1e-13     # intrinsic permeability

K  = rho*g/eta*k        # hydraulic conductivity           
D  = 26                 # aquifer thickness [m]
R  = 16                 # max. horizontal distance from well [m]
hR = 10                  # reference head at max distance [m]

## read flux data
with open("flux.dat", 'r') as file:
    reader_f = csv.reader(file,delimiter=' ')

    #skip header
    next(reader_f)

    # read through file to find flux of timestep 5e6
    for row in reader_f:
        row_list = row
        time = float(row_list[0])
        flux = float(row_list[3])

        if time == 5e6:
            Q = flux        # flux [m^3/s]
            #print('Flux for param-comb '+foldername+': '+str(Q))
            break

# get THIEMS prediction
x_thiem = list()
surf_thiem = list()
diff_thiem = list()
for ii in xplot:

    #check for center point (where the well lays)
    if ii == 0:
        continue
    else:
        r = abs(ii)
        hThiem = -Q/(2 * np.pi* D * K) * np.log(r/R) + hR
        # see https://doi.org/10.5194/hess-20-1655-2016 for formula

        # store for plotting
        x_thiem.append(ii)
        surf_thiem.append(hThiem)

        ## compute absolute difference and append to list
        diffInd = np.where((xplot == ii))[0]
        diff_thiem.append((np.abs(zplot[diffInd] - hThiem)/hThiem)[0] )
        

plt.close('all')
#plt.ion()

## compute RMS of absolut differences
rms = np.sqrt(np.mean(np.array(diff_thiem)**2))


plt.figure(1)
plt.plot(xplot, zplot, marker="x", label=r'$Drawdown \quad Curve$')
plt.scatter(x_thiem, surf_thiem,color = "r", label=r'$Thiem \quad Prediction$')
plt.title(r'RMS_{error}: '+str(rms))
# plt.plot(h, kr, label=r'$K_r=K_r(\psi)$')
# plt.plot([xmin, xmax], [thr,  thr],  '--', label=r'$\theta_r$')
# plt.plot([xmin, xmax], [ths,  ths],  '--', label=r'$\theta_s$')
# plt.plot([xmin, xmax], [kmin, kmin], '--', label=r'$K_{min}$')
plt.xlabel(r'$x [m]$')
plt.ylabel(r'$h [m]$')
plt.legend()

## save plot to directory
plt.savefig('thiem_benchmark.pdf')




