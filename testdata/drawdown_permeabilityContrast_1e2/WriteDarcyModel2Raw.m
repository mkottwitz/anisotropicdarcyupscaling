%% Convert CT-scans from .tif images to 3D raw image data

clc, clear all , close all

%% Add important paths

% lamem matlab path
addpath('../../matlab')

% data path

pathToWrite = pwd;

%% Define parameters

% element domain
nex = 32;
ney = 32;
nez = 32;

SizeOfVoxel = 1/32;     % micrometers

filename   = [pathToWrite,'/aniPermea3D_Por'];     % file-name to store voxel-based data set

Is64BIT        = 0;

%==========================================================================

%% generate peremability tensor and porosity for every element

permeabilities = zeros(nex,ney,nez,7);

%fill permeability array with 6 values (symmetrix permeability tensor) - currently set to isotropic permeability
for k = 1:nez
    for j = 1:ney
        for i = 1:nex
            
            if i >= 16 && i <= 17
                k_xx = 1e-11;
                k_yy = 1e-11;
                k_zz = 1e-11;
                poro = 0.45;
            elseif j >= 16 && j <= 17
                k_xx = 1e-11;
                k_yy = 1e-11;
                k_zz = 1e-11;
                poro = 0.45;
            else
                k_xx = 1e-13;
                k_yy = 1e-13;
                k_zz = 1e-13;
                poro = 0.3;
            end

            
            
            % k_xx
            permeabilities(i,j,k,1) = k_xx;%*rand(1)
            % k_yy
            permeabilities(i,j,k,2) = k_yy;
            % k_zz
            permeabilities(i,j,k,3) = k_zz;
            
            % k_xy, k_yz
            permeabilities(i,j,k,4) = 0;
            % k_xz, k_zx
            permeabilities(i,j,k,5) = 0;
            % k_yz, k_zy
            permeabilities(i,j,k,6) = 0;
            
            
            % add porosity
            permeabilities(i,j,k,7) = poro;
            
        end
    end
end


%% write raw file

fprintf('Dumping raw image to disk ... \n')

fp = fopen(strcat(filename, '-', num2str(nex), 'x', num2str(ney), 'x', num2str(nez), '.raw'), 'w');

if Is64BIT
    fwrite(fp, permeabilities, 'float64');
else
    fwrite(fp, permeabilities, 'float32');
end

fclose(fp);

% compute and print model size for creating partitioning file
R = SizeOfVoxel*((nex)/2);
S = SizeOfVoxel*((ney)/2);
T = SizeOfVoxel*((nez)/2);


fprintf('Please create partitioning file with the following domain sizes: \n')

fprintf('coord_x = %g %g\n', -R, R);
fprintf('coord_y = %g %g\n', -S, S);
fprintf('coord_z = %g %g\n', -T, T);
