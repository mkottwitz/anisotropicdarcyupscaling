%% Convert CT-scans from .tif images to 3D raw image data

clc, clear all , close all

%% Add important paths

% lamem matlab path
addpath('../../matlab')

% data path

pathToWrite = pwd;

%% Define parameters

% element domain
nex = 64;
ney = 64;
nez = 64;

SizeOfVoxel = 30/64;     % micrometers

filename   = [pathToWrite,'/aniPermea3D_Por'];     % file-name to store voxel-based data set

Is64BIT        = 0;

%==========================================================================

%% generate peremability tensor and porosity for every element

permeabilities = zeros(nex,ney,nez,7);
permea_min = 1e-17;
permea_max = 1e-12;
%fill permeability array with 6 values (symmetrix permeability tensor) - currently set to isotropic permeability
for k = 1:nez
    for j = 1:ney
        for i = 1:nex
            
%             % prescrbie random perm and poro
%             randIsoPerm = 10.^((log10(permea_max)-log10(permea_min)).*rand(1)+log10(permea_min));
% 
%             % rough porosity estimate after https://journals.aps.org/pre/pdf/10.1103/PhysRevE.80.021304, eq. 15
%             avg_D = 100e-6;                 %100 microns avg grain diamaeter HARD CODED
%             poro =  ((randIsoPerm./(avg_D^2))./0.11).^(1/5.6);
% 
%             if poro > 0.5
%                 poro = 0.5;
%             end
            randIsoPerm = 1e-13;
            poro        = 0.3;
            % k_xx
            permeabilities(i,j,k,1) = randIsoPerm;
            % k_yy
            permeabilities(i,j,k,2) = randIsoPerm;
            % k_zz
            permeabilities(i,j,k,3) = randIsoPerm;           
                        
            % k_xy, k_yz
            permeabilities(i,j,k,4) = 0;
            % k_xz, k_zx
            permeabilities(i,j,k,5) = 0;
            % k_yz, k_zy
            permeabilities(i,j,k,6) = 0;           
            
            % add porosity
            permeabilities(i,j,k,7) = poro;
            
        end
    end
end


%% write raw file

fprintf('Dumping raw image to disk ... \n')

fp = fopen(strcat(filename, '-', num2str(nex), 'x', num2str(ney), 'x', num2str(nez), '.raw'), 'w');

if Is64BIT
    fwrite(fp, permeabilities, 'float64');
else
    fwrite(fp, permeabilities, 'float32');
end

fclose(fp);

% compute and print model size for creating partitioning file
R = SizeOfVoxel*((nex)/2);
S = SizeOfVoxel*((ney)/2);
T = SizeOfVoxel*((nez)/2);


fprintf('Please create partitioning file with the following domain sizes: \n')

fprintf('coord_x = %g %g\n', -R, R);
fprintf('coord_y = %g %g\n', -S, S);
fprintf('coord_z = %g %g\n', -T, T);
