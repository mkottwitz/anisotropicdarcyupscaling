clear all; close all; clc;

%add path of Matlab Functions
addpath('/media/oskar/DATA/DATA/PERMEA/MatlabFunctions')
addpath('/media/oskar/DATA/DATA/PERMEA/MatlabFunctions/DFN_Codes')
addpath('/media/oskar/DATA/DATA/PERMEA/MatlabFunctions/DFN_Codes/ADFNE_SourceCode')

addpath('/media/oskar/DATA/DATA/PERMEA/DarcySolver/anisotropicdarcyupscaling_seepage/matlab')

nel_x = 2;
nel_y = 2;
nel_z = 2;

coord_x = [-0.5 0.5];
coord_y = [-0.5 0.5];
coord_z = [-0.5 0.5];

dx = (coord_x(2)-coord_x(1))/nel_x;
dy = (coord_y(2)-coord_y(1))/nel_y;




% fit surface to lamem input grid
xfit = linspace(coord_x(1),coord_x(2),nel_x+1);
yfit = linspace(coord_y(1),coord_y(2),nel_y+1);

%     xfit = linspace(xS-xS,xE-xS,200);
%     yfit = linspace(yS-yS,yE-yS,200);
[xq,yq] = meshgrid(xfit,yfit);
surface = peaks(length(xfit)).*0.125;
%surface = griddata(x_f,y_f,gw,xq,yq,'linear');

h = surf(xfit,yfit,surface);
shading flat
set(h, 'EdgeColor', 'none');
cb = colorbar;
title(cb,'[m]')
axis('equal')

%% write as LAMEM topo-input

%% Setup topography

% %from wiki: import your topography data
% % Create example data:
% Topo        = peaks(301);
% Topo        = Topo/4;
% Easting     = (0:1:300);
% Northing    = (0:1:300);
%

% % compute grid spacing
% dx = (max(Easting) - min(Easting))/(length(Easting)-1);
% dy = (max(Northing) - min(Northing))/(length(Northing)-1);
%
% % possibly add offset in data
% Easting     = Easting - 200;
% Northing    = Northing -100;
%
%
% Topo    = Topo';
%
% % write binary to be read by LaMEM
% % (FILENAME,[# of points in x-dir, # of points in y-dir, x-coord of SW corner, y_coord of SW corner,
% % grid spacing in x-dir, grid spacing in y-dir, TopoMatrix in vector form])

% % For orientation
% % surface(1,1):     SW
% % surface(1,end):   SE
% % surface(end,1):   NW
% % surface(end,end): NE
%

% transpose Topo to be read correctly by LaMEM
%surface = surface';
PetscBinaryWrite('InitialWaterlevel.dat', [size(surface,1); size(surface,2); min(xfit); min(yfit); dx; dy; surface(:)]);
%



