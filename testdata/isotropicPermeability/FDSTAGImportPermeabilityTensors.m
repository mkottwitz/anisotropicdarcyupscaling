%% Generate parallel constraint cells flags from 3D raw image data

clc, clear all , close all

%% Add important paths

% lamem matlab path
addpath('../matlab')

% path that holds data
%addpath('/home/oskar/LaMEM/TestData')

%% Defining Parameters
mesh_file      = 'ProcessorPartitioning_1cpu_1.1.1.bin';
permea_file     = 'aniPermea3D-4x4x4.raw';
npx            = 4;        % x-pixel grid size
npy            = 4;        % y-pixel grid size
npz            = 4;        % z-pixel grid size
SizeOfVoxel    = 3.528e-6;   % pixel physical resolution
Is64BIT        = 0;

%==========================================================================

%% LOAD DATA

% Read raw iamge data file
fp  = fopen(permea_file, 'r');

if Is64BIT
    P3D = fread(fp,'float64');
else
    P3D = fread(fp,'float32');
end

P3D = reshape(P3D, npx,npy,npz,7);


% Read processor partitioning
[P] = GetProcessorPartitioning(mesh_file, Is64BIT);

% get grid data
nx     = P.nnodx-1;
ny     = P.nnody-1;
nz     = P.nnodz-1;

% check whether resolution is too high (silly)
if(nx > npx || ny > npy || nz > npz)
    error('Grid resolution cannot exceed pixel resolution');
end


%% STORE FILES

if ~isdir('darcy')
    mkdir darcy
end

if ~isdir('bc')
    mkdir bc
end


% get partitioning data
Nprocx = P.Nprocx;
Nprocy = P.Nprocy;
Nprocz = P.Nprocz;
ix     = P.ix;
iy     = P.iy;
iz     = P.iz;

% initialize subdomain ID
num = 0;

for k = 1:Nprocz
    for j = 1:Nprocy
        for i = 1:Nprocx
            
            % get cell subset of current subdomain
            P3D_kxx = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1, 1);
            P3D_kyy = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1, 2);
            P3D_kzz = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1, 3);
            P3D_kxy = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1, 4);
            P3D_kxz = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1, 5);
            P3D_kyz = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1, 6);
            
            % store to buffer in interlaced format
            buff       = zeros(6, numel(P3D_kxx));
            buff(1, :) = P3D_kxx(:)';
            buff(2, :) = P3D_kyy(:)';
            buff(3, :) = P3D_kzz(:)';
            buff(4, :) = P3D_kxy(:)';
            buff(5, :) = P3D_kxz(:)';
            buff(6, :) = P3D_kyz(:)';
            buff       = buff(:);
            
             % open file
            fname = sprintf('./bc/perm.%1.8d.dat', num);
            
            disp(['Writing file -> ', fname])

            fp = fopen(fname, 'w');
            
            % dump data on disk
            fwrite(fp, buff, 'double');

            fclose(fp);
    
            % update processor ID
            num = num + 1;

        end
    end
end


