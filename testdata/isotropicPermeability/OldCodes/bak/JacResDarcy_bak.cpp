/*@ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 **
 **    Copyright (c) 2011-2015, JGU Mainz, Anton Popov, Boris Kaus
 **    All rights reserved.
 **
 **    This software was developed at:
 **
 **         Institute of Geosciences
 **         Johannes-Gutenberg University, Mainz
 **         Johann-Joachim-Becherweg 21
 **         55128 Mainz, Germany
 **
 **    project:    LaMEM
 **    filename:   JacResTemp.c
 **
 **    LaMEM is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published
 **    by the Free Software Foundation, version 3 of the License.
 **
 **    LaMEM is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 **    See the GNU General Public License for more details.
 **
 **    You should have received a copy of the GNU General Public License
 **    along with LaMEM. If not, see <http://www.gnu.org/licenses/>.
 **
 **
 **    Contact:
 **        Boris Kaus       [kaus@uni-mainz.de]
 **        Anton Popov      [popov@uni-mainz.de]
 **
 **
 **    Main development team:
 **         Anton Popov      [popov@uni-mainz.de]
 **         Boris Kaus       [kaus@uni-mainz.de]
 **         Tobias Baumann
 **         Adina Pusok
 **         Arthur Bauville
 **
 ** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ @*/
//---------------------------------------------------------------------------
//......................   DARCY SOLVER FUNCTIONS   ..........................
//---------------------------------------------------------------------------
#include "LaMEM.h"
#include "JacRes.h"
#include "phase.h"
#include "scaling.h"
#include "fdstag.h"
#include "tssolve.h"
#include "bc.h"
#include "matrix.h"
#include "surf.h"

//---------------------------------------------------------------------------

#define SCATTER_FIELD(da, vec, FIELD) \
	ierr = DMDAGetCorners (da, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr); \
	ierr = DMDAVecGetArray(da, vec, &buff); CHKERRQ(ierr); \
	iter = 0; \
	START_STD_LOOP \
		FIELD \
	END_STD_LOOP \
	ierr = DMDAVecRestoreArray(da, vec, &buff); CHKERRQ(ierr); \
	LOCAL_TO_LOCAL(da, vec)

#define GET_KC \
	ierr = JacResGetTempParam(jr, jr->svCell[iter++].phRat, &kc, NULL, NULL); CHKERRQ(ierr); \
	buff[k][j][i] = kc;

#define GET_HRXY buff[k][j][i] = jr->svXYEdge[iter++].svDev.Hr;
#define GET_HRXZ buff[k][j][i] = jr->svXZEdge[iter++].svDev.Hr;
#define GET_HRYZ buff[k][j][i] = jr->svYZEdge[iter++].svDev.Hr;


////---------------------------------------------------------------------------
//// Read permeabilities of every cell: CONTNUE IMPLEMENTING, HOW TO LINK WITH parsing.cpp
////---------------------------------------------------------------------------
//
//#undef __FUNCT__
//#define __FUNCT__ "ReadCellPermeabilities"
//PetscErrorCode ReadCellPermeabilities(JacRes *jr, FB *fb)
//{
//	FILE           *fp;
//	PetscLogDouble  t;
//	PetscMPIInt     rank;
//	struct          stat sb;
//	char           *filename, file[_str_len_];
//
//	PetscErrorCode ierr;
//	PetscFunctionBegin;
//
//
//	// get file name
//	ierr = getStringParam(fb, _OPTIONAL_, "permea_cell_file", file, "./bc/cdk"); CHKERRQ(ierr);
//
//	PrintStart(&t, "Loading fixed cell flags in parallel from", file);
//
//	// compile input file name with extension
//	MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
//
//	asprintf(&filename, "%s.%1.8lld.dat", file, (LLD)rank);
//
//	// open file
//	fp = fopen(filename, "rb");
//
//	if(fp == NULL)
//	{
//		SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Cannot open input file %s\n", filename);
//	}
//
//	// check file size
//	stat(filename, &sb);
//
//	if((PetscInt)sb.st_size != bc->fs->nCells)
//	{
//		SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Wrong fixed cell file size %s\n", filename);
//	}
//
//	// read flags
//	fread(bc->fixCellFlag, (size_t)bc->fs->nCells, 1, fp);
//
//	// close file
//	fclose(fp);
//
//	PrintDone(t);
//
//	PetscFunctionReturn(0);
//}


//---------------------------------------------------------------------------
// Finite ELement Helper Functions
//---------------------------------------------------------------------------

// linear quadrature rule with 8 integration points and weights in 3D
#undef __FUNCT__
#define __FUNCT__ "FinElGetQuad"
PetscErrorCode FinElGetQuad(
			PetscScalar *px1,
			PetscScalar *px2,
			PetscScalar *px3,
			PetscScalar *pwt
			)
{
	// k = sqrt(1/3)
	PetscScalar k(0.577350269189626), w(1.0);
// old numbering
//	px1[0] =  k;   px2[0] =  k;   px3[0] = -k;   pwt[0] = w;
//	px1[1] =  k;   px2[1] = -k;   px3[1] = -k;   pwt[1] = w;
//	px1[2] = -k;   px2[2] =  k;   px3[2] = -k;   pwt[2] = w;
//	px1[3] = -k;   px2[3] = -k;   px3[3] = -k;   pwt[3] = w;
//	px1[4] =  k;   px2[4] =  k;   px3[4] =  k;   pwt[4] = w;
//	px1[5] =  k;   px2[5] = -k;   px3[5] =  k;   pwt[5] = w;
//	px1[6] = -k;   px2[6] =  k;   px3[6] =  k;   pwt[6] = w;
//	px1[7] = -k;   px2[7] = -k;   px3[7] =  k;   pwt[7] = w;

	// new numbering to align with shape functions and derivatives
	px1[0] = -k;   px2[0] = -k;   px3[0] = -k;   pwt[0] = w;
	px1[1] =  k;   px2[1] = -k;   px3[1] = -k;   pwt[1] = w;
	px1[2] =  k;   px2[2] =  k;   px3[2] = -k;   pwt[2] = w;
	px1[3] = -k;   px2[3] =  k;   px3[3] = -k;   pwt[3] = w;
	px1[4] = -k;   px2[4] = -k;   px3[4] =  k;   pwt[4] = w;
	px1[5] =  k;   px2[5] = -k;   px3[5] =  k;   pwt[5] = w;
	px1[6] =  k;   px2[6] =  k;   px3[6] =  k;   pwt[6] = w;
	px1[7] = -k;   px2[7] =  k;   px3[7] =  k;   pwt[7] = w;


// old approach
//	PetscInt i,j,k,intp=0;
//	PetscScalar point1D[n];
//	PetscScalar weight1D[n];
//
//	PetscFunctionBegin;
//
//    if (n == 1)
//    {
//		point1D[0]    = 0.0;
//		weight1D[0]   = 2.0;
//    }
//    if (n == 2)
//    {
//		point1D[0]    = -sqrt(1.0/3.0);
//		point1D[1]    = sqrt(1.0/3.0);
//		weight1D[0]   = 1.0;
//		weight1D[1]   = 1.0;
//    }
//    if (n == 3)
//    {
//		point1D[0]    = -sqrt(3.0/5.0);
//		point1D[1]    = 0.0;
//		point1D[2]    = sqrt(3.0/5.0);
//		weight1D[0]   = 5.0/9.0;
//		weight1D[1]   = 8.0/9.0;
//		weight1D[2]   = 5.0/9.0;
//    }
//
//    for(k = 0; k < n; k++)
//    	{	for(j = 0; j < n; j++)
//    		{	for(i = 0; i < n; i++)
//    			{
//    			point[0][intp] = point1D[i];
//    			point[1][intp] = point1D[j];
//    			point[2][intp] = point1D[k];
//
//    			weight[intp] = weight1D[i]*weight1D[j]*weight1D[k];
//    			intp++;
//    			}
//    		}
//    	}

    PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
// Creates trilinear Shape Functions and, i.e. order 1
// x1,x2,x3 are local integrationpoint coordinates in x,y and z direction respectively, N holds the shape functions
#undef __FUNCT__
#define __FUNCT__ "FinElGetShape"
PetscErrorCode FinElGetShape(
			PetscScalar x1,
			PetscScalar x2,
			PetscScalar x3,
			PetscScalar *N
			)
{
	PetscFunctionBegin;


	// Define constants:
	PetscScalar k  (0.125    );
	PetscScalar px1(1.0 + x1 ), px2(1.0 + x2 ), px3(1.0 + x3 );
	PetscScalar mx1(1.0 - x1 ), mx2(1.0 - x2 ), mx3(1.0 - x3 );

	// Compute nodal shape functions in the space:
	N[0] = k*mx1*mx2*mx3;  // N1
	N[1] = k*px1*mx2*mx3;  // N2
	N[2] = k*px1*px2*mx3;  // N3
	N[3] = k*mx1*px2*mx3;  // N4
	N[4] = k*mx1*mx2*px3;  // N5
	N[5] = k*px1*mx2*px3;  // N6
	N[6] = k*px1*px2*px3;  // N7
	N[7] = k*mx1*px2*px3;  // N8


    PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
// Creates trilinear Shape Function derivatives, i.e. order 1
// x1,x2,x3 are local integrationpoint coordinates in x,y and z direction respectively,
// dX1, dX2, dX3 holds the shape function dervivatives in x,y,z direction respetively
#undef __FUNCT__
#define __FUNCT__ "FinElGetShapeDeriv"
PetscErrorCode FinElGetShapeDeriv(
			PetscScalar x1,
			PetscScalar x2,
			PetscScalar x3,
			PetscScalar *dX1,
			PetscScalar *dX2,
			PetscScalar *dX3
			)
{
	PetscFunctionBegin;

	PetscScalar k  (0.125    );
	PetscScalar px1(1.0 + x1 ), px2(1.0 + x2 ), px3(1.0 + x3 );
	PetscScalar mx1(1.0 - x1 ), mx2(1.0 - x2 ), mx3(1.0 - x3 );

	/* Compute shape functions derivatives with respect to local coordinates:
	d/dX1                d/dX2                 d/dX3                 */
	dX1[0]= - k*mx2*mx3; dX2[0] = - k*mx1*mx3; dX3[0] = - k*mx1*mx2; // dN1
	dX1[1]=   k*mx2*mx3; dX2[1] = - k*px1*mx3; dX3[1] = - k*px1*mx2; // dN2
	dX1[2]=   k*px2*mx3; dX2[2] =   k*px1*mx3; dX3[2] = - k*px1*px2; // dN3
	dX1[3]= - k*px2*mx3; dX2[3] =   k*mx1*mx3; dX3[3] = - k*mx1*px2; // dN4
	dX1[4]= - k*mx2*px3; dX2[4] = - k*mx1*px3; dX3[4] =   k*mx1*mx2; // dN5
	dX1[5]=   k*mx2*px3; dX2[5] = - k*px1*px3; dX3[5] =   k*px1*mx2; // dN6
	dX1[6]=   k*px2*px3; dX2[6] =   k*px1*px3; dX3[6] =   k*px1*px2; // dN7
	dX1[7]= - k*px2*px3; dX2[7] =   k*mx1*px3; dX3[7] =   k*mx1*px2; // dN8


    PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
// computes inverse jacoby matrix from shape function derivatives and corner coordniates
#undef __FUNCT__
#define __FUNCT__ "FinElGetGlobalShapeDeriv"
PetscErrorCode FinElGetGlobalShapeDeriv(
			PetscScalar dX1,
			PetscScalar dX2,
			PetscScalar dX3,
			PetscScalar cX1,
			PetscScalar cX2,
			PetscScalar cX3,
			PetscScalar *dNdX1,
			PetscScalar *dNdX2,
			PetscScalar *dNdX3,
			PetscScalar *detJ
			)
{
	PetscFunctionBegin;

	PetscScalar j11, j12, j13, j21, j22, j23, j31, j32, j33;
	PetscScalar i11, i12, i13, i21, i22, i23, i31, i32, i33;
	PetscInt iter;

	// compute Jacoby matrix
    for(iter=0; iter<8; iter++)
    {
    	j11 += dX1[iter] * cX1[iter];
    	j21 += dX2[iter] * cX1[iter];
    	j31 += dX3[iter] * cX1[iter];
    	j12 += dX1[iter] * cX2[iter];
    	j22 += dX2[iter] * cX2[iter];
    	j32 += dX3[iter] * cX2[iter];
    	j13 += dX1[iter] * cX3[iter];
    	j23 += dX2[iter] * cX3[iter];
    	j33 += dX3[iter] * cX3[iter];
    }

	// Compute Jacoby matrix determinant:
	(*detJ) = (j11*(j22*j33 - j23*j32)
	+      j12*(j23*j31 - j21*j33)
	+      j13*(j21*j32 - j22*j31));

	// Compute inverse Jacoby matrix:
	i11 = (j22 * j33 - j23 * j32)/detJ;
	i12 = (j13 * j32 - j12 * j33)/detJ;
	i13 = (j12 * j23 - j13 * j22)/detJ;
	i21 = (j23 * j31 - j21 * j33)/detJ;
	i22 = (j11 * j33 - j13 * j31)/detJ;
	i23 = (j13 * j21 - j11 * j23)/detJ;
	i31 = (j21 * j32 - j22 * j31)/detJ;
	i32 = (j12 * j31 - j11 * j32)/detJ;
	i33 = (j11 * j22 - j12 * j21)/detJ;

	// compute shape function derivatives w.r.t. global coordinates
    for(iter=0; iter<8; iter++)
    {
    	dNdX1[iter]= i11*dX1[iter] + i12*dX2[iter] + i13*dX3[iter];
    	dNdX2[iter]= i21*dX1[iter] + i22*dX2[iter] + i23*dX3[iter];
    	dNdX3[iter]= i31*dX1[iter] + i32*dX2[iter] + i33*dX3[iter];
    }

    PetscFunctionReturn(0);
}




////---------------------------------------------------------------------------
//// Pressure parameter functions, CONTINUE IMPLEMENTATION!!!!
////---------------------------------------------------------------------------
//#undef __FUNCT__
//#define __FUNCT__ "JacResGetPressParam"
//PetscErrorCode JacResGetPressParam(
//		JacRes      *jr,
//		PetscScalar *k_xx,      // xx-conductivity
//		PetscScalar *k_yy,      // yy-conductivity
//		PetscScalar *k_zz,      // zz-conductivity
//		PetscScalar *k_xy,      // xy-conductivity
//		PetscScalar *k_xz,      // xz-conductivity
//		PetscScalar *k_yz,      // yz-conductivity
//		PetscScalar *s)         // sources
//{
//	// compute effective energy parameters in the cell
//
//	PetscInt    i, numPhases, AirPhase;
//    Material_t  *phases, *M;
//
//	PetscScalar cf, k, rho, rho_Cp, rho_A, density;
//
//	PetscFunctionBegin;
//
//	// initialize
//	k         = 0.0;
//	rho_Cp    = 0.0;
//	rho_A     = 0.0;
//	numPhases = jr->dbm->numPhases;
//	phases    = jr->dbm->phases;
//	density   = jr->scal->density;
//	AirPhase  = jr->surf->AirPhase;
//
//	// average all phases
//	for(i = 0; i < numPhases; i++)
//	{
//		M       = &phases[i];
//		cf      =  phRat[i];
//		rho     =  M->rho;
//
//		// override air phase density
//		if(AirPhase != -1 && i == AirPhase)
//		{
//			rho = 1.0/density;
//		}
//
//		k      +=  cf*M->k;
//		rho_Cp +=  cf*M->Cp*rho;
//		rho_A  +=  cf*M->A*rho;
//	}
//
//	// store
//	if(k_)      (*k_)      = k;
//    if(rho_Cp_) (*rho_Cp_) = rho_Cp;
//	if(rho_A_)  (*rho_A_)  = rho_A;
//
//	PetscFunctionReturn(0);
//}
//---------------------------------------------------------------------------
//#undef __FUNCT__
//#define __FUNCT__ "JacResCheckPressParam"
//PetscErrorCode JacResCheckPressParam(JacRes *jr)
//{
//	// check whether thermal material parameters are properly defined
//
//    Material_t  *phases, *M;
//	PetscInt    i, numPhases, AirPhase;
//
//	PetscFunctionBegin;
//
//	// temperature diffusion cases only
//	if(!jr->ctrl.actTemp) PetscFunctionReturn(0);
//
//	// initialize
//	numPhases = jr->dbm->numPhases;
//	phases    = jr->dbm->phases;
//	AirPhase  = jr->surf->AirPhase;
//
//	// check all phases
//	for(i = 0; i < numPhases; i++)
//	{
//		M = &phases[i];
//
//		// check density of the rock phases
//		if((AirPhase != -1 && i != AirPhase) || AirPhase == -1)
//		{
//			if(M->rho == 0.0) SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Define density of phase %lld\n", (LLD)i);
//		}
//			if(M->k   == 0.0) SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Define conductivity of phase %lld\n", (LLD)i);
//			if(M->Cp  == 0.0) SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Define heat capacity of phase %lld\n", (LLD)i);
//	}
//
//	PetscFunctionReturn(0);
//}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResCreatePressParam"
PetscErrorCode JacResCreatePressParam(JacRes *jr)
{
	// setup pressure parameters

	FDSTAG *fs;
	const PetscInt *lx, *ly, *lz;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	fs = jr->fs;

	// create local pressure vector using box-stencil central DMDA
	ierr = DMCreateLocalVector(fs->DA_COR, &jr->lP); CHKERRQ(ierr);

	// get cell center grid partitioning
	ierr = DMDAGetOwnershipRanges(fs->DA_COR, &lx, &ly, &lz); CHKERRQ(ierr);

	// create pressure DMDA
	ierr = DMDACreate3dSetUp(PETSC_COMM_WORLD,
		DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,
		DMDA_STENCIL_BOX,
		fs->dsx.tnods, fs->dsy.tnods, fs->dsz.tnods,
		fs->dsx.nproc, fs->dsy.nproc, fs->dsz.nproc,
		1, 1, lx, ly, lz, &jr->DA_P); CHKERRQ(ierr);

	// create pressure preconditioner matrix
	ierr = DMCreateMatrix(jr->DA_P, &jr->Atp); CHKERRQ(ierr);

	// set matrix options (development)
	ierr = MatSetOption(jr->Atp, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_TRUE); CHKERRQ(ierr);
	ierr = MatSetOption(jr->Atp, MAT_NEW_NONZERO_LOCATION_ERR, PETSC_TRUE);   CHKERRQ(ierr);
	ierr = MatSetOption(jr->Atp, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);       CHKERRQ(ierr);
	ierr = MatSetOption(jr->Atp, MAT_NO_OFF_PROC_ZERO_ROWS, PETSC_TRUE);      CHKERRQ(ierr);

	// pressure solution vector
	ierr = DMCreateGlobalVector(jr->DA_P, &jr->dP); CHKERRQ(ierr);

	// pressure residual vector
	ierr = DMCreateGlobalVector(jr->DA_P, &jr->gp); CHKERRQ(ierr);

	// create temperature diffusion solver
	ierr = KSPCreate(PETSC_COMM_WORLD, &jr->pksp); CHKERRQ(ierr);
	ierr = KSPSetOptionsPrefix(jr->pksp,"ts_");    CHKERRQ(ierr);
	ierr = KSPSetFromOptions(jr->pksp);            CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResDestroyPressParam"
PetscErrorCode JacResDestroyPressParam(JacRes *jr)
{
	// destroy pressure parameters

	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = VecDestroy(&jr->lP);   CHKERRQ(ierr);

	// temperature parameters
	ierr = DMDestroy (&jr->DA_P); CHKERRQ(ierr);

	ierr = MatDestroy(&jr->Atp);  CHKERRQ(ierr);

	ierr = VecDestroy(&jr->dP);   CHKERRQ(ierr);

	ierr = VecDestroy(&jr->gp);   CHKERRQ(ierr);

	ierr = KSPDestroy(&jr->pksp); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResInitPress"
PetscErrorCode JacResInitPress(JacRes *jr)
{
	// initialize pressure from boundary conditions

	FDSTAG      *fs;
	BCCtx       *bc;
	PetscScalar ***lP, ***bcP, P;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz, iter;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access context
	fs = jr->fs;
	bc = jr->bc;

	ierr = VecZeroEntries(jr->lP); CHKERRQ(ierr);

	ierr = DMDAVecGetArray(fs->DA_COR, jr->lP,  &lP);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, bc->bcP_c, &bcP); CHKERRQ(ierr);

	iter = 0;

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP
	{
		P = bcP[k][j][i];

		lP[k][j][i] = P;

		iter++;
	}
	END_STD_LOOP

	ierr = DMDAVecRestoreArray(fs->DA_COR, jr->lP,  &lP);  CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, bc->bcP_c, &bcP); CHKERRQ(ierr);

	// apply two-point constraints
	ierr = JacResApplyPressBC(jr); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResUpdatePress"
PetscErrorCode JacResUpdatePress(JacRes *jr)
{
	// correct pressure for diffusion (Newton update)

	FDSTAG      *fs;
	PetscScalar ***lP, ***dP;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	fs = jr->fs;

	ierr = DMDAVecGetArray(fs->DA_COR, jr->lP, &lP); CHKERRQ(ierr);
	ierr = DMDAVecGetArray(jr->DA_P,   jr->dP, &dP); CHKERRQ(ierr);

	ierr = DMDAGetCorners(fs->DA_COR, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	// changed from -= to +=
	START_STD_LOOP
	{
		lP[k][j][i] += dP[k][j][i];
	}
	END_STD_LOOP

	ierr = DMDAVecRestoreArray(fs->DA_COR, jr->lP, &lP); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(jr->DA_P,   jr->dP, &dP); CHKERRQ(ierr);

	// apply two-point constraints
	ierr = JacResApplyPressBC(jr); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResApplyPressBC"
PetscErrorCode JacResApplyPressBC(JacRes *jr)
{
	// apply temperature two-point constraints

	FDSTAG      *fs;
	BCCtx       *bc;
	PetscScalar pmdof;
	PetscScalar ***lP, ***bcP;
	PetscInt    mcx, mcy, mcz;
	PetscInt    I, J, K, fi, fj, fk;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz;

	PetscErrorCode ierr;
	PetscFunctionBegin;

	fs  =  jr->fs;
	bc  =  jr->bc;

	// initialize maximal index in all directions
	mcx = fs->dsx.tcels - 1;
	mcy = fs->dsy.tcels - 1;
	mcz = fs->dsz.tcels - 1;

	// exchange internal ghost points
	LOCAL_TO_LOCAL(fs->DA_COR, jr->lP)

	// access local solution & boundary constraints
	ierr = DMDAVecGetArray(fs->DA_COR, jr->lP,  &lP);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, bc->bcP_c, &bcP); CHKERRQ(ierr);

	GET_CELL_RANGE_GHOST_INT(nx, sx, fs->dsx)
	GET_CELL_RANGE_GHOST_INT(ny, sy, fs->dsy)
	GET_CELL_RANGE_GHOST_INT(nz, sz, fs->dsz)

	START_STD_LOOP
	{
		pmdof = lP[k][j][i];

		I = i; fi = 0;
		J = j; fj = 0;
		K = k; fk = 0;

		if(i == 0)   { fi = 1; I = i-1; SET_TPC(bcP, lP, k, j, I, pmdof) }
		if(i == mcx) { fi = 1; I = i+1; SET_TPC(bcP, lP, k, j, I, pmdof) }
		if(j == 0)   { fj = 1; J = j-1; SET_TPC(bcP, lP, k, J, i, pmdof) }
		if(j == mcy) { fj = 1; J = j+1; SET_TPC(bcP, lP, k, J, i, pmdof) }
		if(k == 0)   { fk = 1; K = k-1; SET_TPC(bcP, lP, K, j, i, pmdof) }
		if(k == mcz) { fk = 1; K = k+1; SET_TPC(bcP, lP, K, j, i, pmdof) }

		if(fi && fj)       SET_EDGE_CORNER(n, lP, k, J, I, k, j, i, pmdof)
		if(fi && fk)       SET_EDGE_CORNER(n, lP, K, j, I, k, j, i, pmdof)
		if(fj && fk)       SET_EDGE_CORNER(n, lP, K, J, i, k, j, i, pmdof)
		if(fi && fj && fk) SET_EDGE_CORNER(n, lP, K, J, I, k, j, i, pmdof)
	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(fs->DA_COR, jr->lP,  &lP);  CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, bc->bcP_c, &bcP); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}
//---------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "JacResGetPressMatAndRes"
PetscErrorCode JacResGetPressMatAndRes(JacRes *jr)
{
	FDSTAG     *fs;
	BCCtx      *bc;

	PetscInt    iter, num, *list, *e;
	PetscInt    i, j, k, nx, ny, nz, sx, sy, sz;
 	PetscScalar dx, dy, dz;

 	// pyhsical properties (conductivity tensor and sources)
 	PetscScalar ***lkxx, ***lkyy, ***lkzz, ***lkxy, ***lkxz, *lkyz, ***ls;

 	// global residual vector and local pressure for residual computation
	PetscScalar ***gr, ***lP;


	PetscErrorCode ierr;
	PetscFunctionBegin;

	// access residual context variables TODO: IMPLEMENT BOUNDARY CONDITIONS
	fs    = jr->fs;
	bc    = jr->bc;
	num   = bc->pNumSPC;
	list  = bc->pSPCList;

//	// initialize maximum cell index in all directions
//	mx = fs->dsx.tcels - 1;
//	my = fs->dsy.tcels - 1;
//	mz = fs->dsz.tcels - 1;
//
//	// compute inverse time step
//	if(dt) invdt = 1.0/dt;
//	else   invdt = 0.0;

	// use this later to calculate conductivities from permeabilities with density and viscosity
//	SCATTER_FIELD(fs->DA_CEN, jr->ldxx, GET_KC)
//	SCATTER_FIELD(fs->DA_XY,  jr->ldxy, GET_HRXY)
//	SCATTER_FIELD(fs->DA_XZ,  jr->ldxz, GET_HRXZ)
//	SCATTER_FIELD(fs->DA_YZ,  jr->ldyz, GET_HRYZ)

	// access work vectors
	ierr = DMDAVecGetArray(jr->DA_P,   jr->gr,   &gr);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_COR, jr->lP,   &lP);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_CEN, jr->lkxx, &lkxx);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_CEN, jr->lkyy, &lkyy);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_CEN, jr->lkzz, &lkzz);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_CEN, jr->lkxy, &lkxy);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_CEN, jr->lkxz, &lkxz);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_CEN, jr->lkyz, &lkyz);  CHKERRQ(ierr);
	ierr = DMDAVecGetArray(fs->DA_CEN, jr->ls,   &ls);  CHKERRQ(ierr);

	// clear matrix coefficients
	ierr = MatZeroEntries(jr->Atp); CHKERRQ(ierr);

	// get quadrature rule /vectors of size nip (8)
	PetscScalar px1[8], px2[8], px3[8], pwt[8];

	ierr = FinElGetQuad(&px1, &px2, &px3, &pwt);  CHKERRQ(ierr);

	//---------------
	// central points
	//---------------
	iter = 0;
	ierr = DMDAGetCorners(fs->DA_CEN, &sx, &sy, &sz, &nx, &ny, &nz); CHKERRQ(ierr);

	START_STD_LOOP
	{
		// initialize arrays
		PetscScalar F[8], KL[8][8];					// local source vector and local stiffness matrix
		PetscScalar kX1[3], kX2[3], kX3[3];			// conductivity tensor rows
		PetscScalar cP[8], cX1[8], cX2[8], cX3[8];	// corner pressure and coordinates
		PetscScalar s, res;
		PetscInt 	ip, nip=8;

		// access corner pressures
		cP[0]  = lP[k][j][i];
		cP[1]  = lP[k][j][i+1];
		cP[2]  = lP[k][j+1][i+1];
		cP[3]  = lP[k][j+1][i];
		cP[4]  = lP[k+1][j][i];
		cP[5]  = lP[k+1][j][i+1];
		cP[6]  = lP[k+1][j+1][i+1];
		cP[7]  = lP[k+1][j+1][i];

		// get mesh steps
		dx = SIZE_CELL(i, sx, fs->dsx);
		dy = SIZE_CELL(j, sy, fs->dsy);
		dz = SIZE_CELL(k, sz, fs->dsz);

		// get corner coordinates of cell
		cX1[0] = i*dx, 	 	cX2[0] = j*dy, 	  	cX3[0] = k*dz;
		cX1[1] = (i+1)*dx, 	cX2[1] = j*dy, 	  	cX3[1] = k*dz;
		cX1[2] = (i+1)*dx, 	cX2[2] = (j+1)*dy, 	cX3[2] = k*dz;
		cX1[3] = i*dx, 	 	cX2[3] = (j+1)*dy, 	cX3[3] = k*dz;
		cX1[4] = i*dx, 	 	cX2[4] = j*dy, 	  	cX3[4] = (k+1)*dz;
		cX1[5] = (i+1)*dx, 	cX2[5] = j*dy, 	  	cX3[5] = (k+1)*dz;
		cX1[6] = (i+1)*dx, 	cX2[6] = (j+1)*dy, 	cX3[6] = (k+1)*dz;
		cX1[7] = i*dx, 	 	cX2[7] = (j+1)*dy, 	cX3[7] = (k+1)*dz;

		// get conductivity tensor
		kX1[0] = lkxx[k][j][i], kX2[0] = lkxy[k][j][i], kX3[0] = lkxz[k][j][i];
		kX1[1] = lkxy[k][j][i], kX2[1] = lkyy[k][j][i], kX3[1] = lkyz[k][j][i];
		kX1[2] = lkxz[k][j][i], kX2[2] = lkyz[k][j][i], kX3[2] = lkzz[k][j][i];

		// get source value
		s = ls[k][j][i];

		// integrationpoint loop (ip-loop)
		for(ip = 0; ip < nip; ip++)
		{

			// initialize shape fundcions, shape function derivatives and shape function derivatives w.r.t. global coordinates
			PetscScalar N[8], dX1[8], dX2[8], dX3[8],  dNdX1[8], dNdX2[8], dNdX3[8];
			PetscScalar detJ;		// determinatn of local jacoby matrix

			// get shape functions and derivatives
			ierr = FinElGetShape(px1[ip],px2[ip],px3[ip], &N);  CHKERRQ(ierr);

			ierr = FinElGetShapeDeriv(px1[ip],px2[ip],px3[ip], &dX1, &dX2, &dX3); CHKERRQ(ierr);

			// get shape fucntions with respect to global coordinates (computed with Jacoby Matrix)
			ierr = FinElGetGlobalShapeDeriv(dX1, dX2, dX3, cX1, cX2, cX3, &dNdX1, &dNdX2, &dNdX3, &detJ); CHKERRQ(ierr);

		    // update local F
		    F[0] += N[0] * s * pwt[ip] * detJ;
		    F[1] += N[1] * s * pwt[ip] * detJ;
		    F[2] += N[2] * s * pwt[ip] * detJ;
		    F[3] += N[3] * s * pwt[ip] * detJ;
		    F[4] += N[4] * s * pwt[ip] * detJ;
		    F[5] += N[5] * s * pwt[ip] * detJ;
		    F[6] += N[6] * s * pwt[ip] * detJ;
		    F[7] += N[7] * s * pwt[ip] * detJ;

		    // update local KL
		    PetscScalar d1, d2, d3, kscal;
		    PetscInt iter1, iter2;
		    for(iter1=0; iter1<8; iter1++)
			{
		    	// dNdX'[8][3] * kappa[3][3] * dNdX[3][8]
				d1 = dNdX1[iter1]*kX1[0] +  dNdX2[iter1]*kX1[1] +  dNdX3[iter1]*kX1[3];
				d2 = dNdX1[iter1]*kX2[0] +  dNdX2[iter1]*kX2[1] +  dNdX3[iter1]*kX2[3];
				d3 = dNdX1[iter1]*kX3[0] +  dNdX2[iter1]*kX3[1] +  dNdX3[iter1]*kX3[3];

				for (iter2=0; iter2<8; iter2++)
				{
					kscal = d1*dNdX1[iter2] + d2*dNdX2[iter2] + d3*dNdX3[iter2];

					KL[iter1][iter2] += kscal * pwt[ip] * detJ;
				}
			}

		}

		// compute residual "on the fly"
		PetscInt r8, c8;
	    for(r8=0; r8<8; r8++)
	    {
	        for(c8=0; c8<8; c8++)
			{
				res[r8] += -KL[r8][c8]*cP[c8];
			}
	        res[r8] -= F[r8];
	    }

		// assign to residual vector
		gr[k][j][i]			+= res[0];
		gr[k][j][i+1]		+= res[1];
		gr[k][j+1][i+1]		+= res[2];
		gr[k][j+1][i]		+= res[3];
		gr[k+1][j][i]		+= res[4];
		gr[k+1][j][i+1]		+= res[5];
		gr[k+1][j+1][i+1]	+= res[6];
		gr[k+1][j+1][i]		+= res[7];

		// stencil for global stiffness matrix assignment
		MatStencil  row[1], col[8], v[8];
		PetscInt rk[8], rj[8], ri[8], iter;

		// initalize assignment indices for rows in MatSetValuesStencil
		rk[0] = k; 		rj[0] = j;		ri[0] = i;
		rk[1] = k; 		rj[1] = j;		ri[1] = i+1;
		rk[2] = k; 		rj[2] = j+1;    ri[2] = i+1;
		rk[3] = k; 		rj[3] = j+1; 	ri[3] = i;
		rk[4] = k+1;	rj[4] = j;		ri[4] = i;
		rk[5] = k+1;	rj[5] = j;		ri[5] = i+1;
		rk[6] = k+1;	rj[6] = j+1;	ri[6] = i+1;
		rk[7] = k+1;	rj[7] = j+1;	ri[7] = i;


		// assign to global stiffnes matrix
	    for(iter=0; iter<8; iter++)
	    {
			// set corresponding row indices
			row[0].k = rk[iter];   row[0].j = rj[iter];   row[0].i = ri[iter];   row[0].c = 0;

			// set column indices
			col[0].k = k,   col[0].j = j,   col[0].i = i,   col[1].c = 0;
			col[1].k = k,   col[1].j = j,   col[1].i = i+1, col[1].c = 0;
			col[2].k = k,   col[2].j = j+1, col[2].i = i+1, col[2].c = 0;
			col[3].k = k,   col[3].j = j+1, col[3].i = i,   col[3].c = 0;
			col[4].k = k+1, col[4].j = j,   col[4].i = i,   col[4].c = 0;
			col[5].k = k+1, col[5].j = j,   col[5].i = i+1, col[5].c = 0;
			col[6].k = k+1, col[6].j = j+1, col[6].i = i+1, col[6].c = 0;
			col[7].k = k+1, col[7].j = j+1, col[7].i = i,   col[7].c = 0;

			// set values from local jacobian matrix
			v[0] = KL[iter][0];
			v[1] = KL[iter][1];
			v[2] = KL[iter][2];
			v[3] = KL[iter][3];
			v[4] = KL[iter][4];
			v[5] = KL[iter][5];
			v[6] = KL[iter][6];
			v[7] = KL[iter][7];

			// set matrix coefficients
			ierr = MatSetValuesStencil(jr->Atp, 1, row, 8, col, v, ADD_VALUES); CHKERRQ(ierr);

	    }



	}
	END_STD_LOOP

	// restore access
	ierr = DMDAVecRestoreArray(jr->DA_P,   jr->gr,   &gr);   CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_COR, jr->lP,   &lP);   CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_CEN, jr->lkxx, &lkxx); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_CEN, jr->lkyy, &lkyy); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_CEN, jr->lkzz, &lkzz); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_CEN, jr->lkxy, &lkxy); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_CEN, jr->lkxz, &lkxz); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_CEN, jr->lkyz, &lkyz); CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(fs->DA_CEN, jr->ls,   &ls);   CHKERRQ(ierr);



	// TODO: IMPLEMENT BOUNDARY CONDITIONS
	// impose boundary constraints -> set boundaries to zero
	ierr = VecGetArray(jr->gr, &e); CHKERRQ(ierr);

	for(i = 0; i < num; i++) e[list[i]] = 0.0;

	ierr = VecRestoreArray(jr->gr, &e); CHKERRQ(ierr);

	// assemble temperature matrix -> one on the diagonal for boundary values
	ierr = MatAIJAssemble(jr->Atp, num, list, 1.0); CHKERRQ(ierr);

	PetscFunctionReturn(0);
}

