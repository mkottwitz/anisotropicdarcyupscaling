%% Generate parallel constraint cells flags from 3D raw image data

clc, clear all , close all

%% Add important paths

% lamem matlab path
addpath('/home/oskar/LaMEM/LaMEM_Darcy/matlab')

% path that holds data
addpath('/home/oskar/LaMEM/TestData')

%% Defining Parameters
mesh_file      = 'ProcessorPartitioning_1cpu_1.1.1.bin';
permea_file     = 'aniPermea3D-4x4x4.raw';
npx            = 4;        % x-pixel grid size
npy            = 4;        % y-pixel grid size
npz            = 4;        % z-pixel grid size
SizeOfVoxel    = 3.528e-6;   % pixel physical resolution
Is64BIT        = 0;

%==========================================================================

%% LOAD DATA

% Read raw iamge data file
fp  = fopen(permea_file, 'r');

if Is64BIT
    P3D = fread(fp,'float64');
else
    P3D = fread(fp,'float32');
end

P3D = reshape(P3D, npx,npy,npz,7);


% Read processor partitioning
[P] = GetProcessorPartitioning(mesh_file, Is64BIT);

% get grid data
nx     = P.nnodx-1;
ny     = P.nnody-1;
nz     = P.nnodz-1;

% check whether resolution is too high (silly)
if(nx > npx || ny > npy || nz > npz)
    error('Grid resolution cannot exceed pixel resolution');
end


%% STORE FILES

if ~isdir('k_xx')
    mkdir k_xx
end

if ~isdir('k_yy')
    mkdir k_yy
end

if ~isdir('k_zz')
    mkdir k_zz
end

if ~isdir('k_xy')
    mkdir k_xy
end

if ~isdir('k_xz')
    mkdir k_xz
end

if ~isdir('k_yz')
    mkdir k_yz
end

if ~isdir('bc')
    mkdir bc
end

if ~isdir('s')
    mkdir s
end

% get partitioning data
Nprocx = P.Nprocx;
Nprocy = P.Nprocy;
Nprocz = P.Nprocz;
ix     = P.ix;
iy     = P.iy;
iz     = P.iz;

% initialize subdomain ID
num = 0;

for k = 1:Nprocz
    for j = 1:Nprocy
        for i = 1:Nprocx
            
            for ki = 1:7
                
                % get cell subset of current subdomain
                %buff = P3D(:,:,:,ki);
                P3DS = P3D(ix(i):ix(i+1)-1, iy(j):iy(j+1)-1, iz(k):iz(k+1)-1,ki);
                
                % open file
                if ki == 1
                    fname = sprintf('./k_xx/cdk.%1.8d.dat', num);
                elseif ki == 2
                    fname = sprintf('./k_yy/cdk.%1.8d.dat', num);
                elseif ki == 3
                    fname = sprintf('./k_zz/cdk.%1.8d.dat', num);
                elseif ki == 4
                    fname = sprintf('./k_xy/cdk.%1.8d.dat', num);
                elseif ki == 5
                    fname = sprintf('./k_xz/cdk.%1.8d.dat', num);
                elseif ki == 6
                    fname = sprintf('./k_yz/cdk.%1.8d.dat', num);
                elseif ki == 7
                    fname = sprintf('./s/cds.%1.8d.dat', num);
                end
  
                disp(['Writing file -> ', fname])
                
                fp = fopen(fname, 'w');
                
                % dump data on disk
                if Is64BIT
                    fwrite(fp, P3DS, 'float64');
                else
                    fwrite(fp, P3DS, 'float32');
                end
                
                fclose(fp);
            end
            
            % write boundary flags -> 1 = matrix (impermeable), 0 =
            % fracture (permeable)
            [sx, sy, sz, ~] = size(P3D);
            
            P3DF = zeros(sx,sy,sz);
            
            for kk = 1:sz
                for jj = 1:sy
                    for ii = 1:sx
                        a=0;
                        
                        if sum(any(P3D(i,j,k,:))) ~= 0
                            P3DF(i,j,k) = 0;
                        else
                            P3DF(i,j,k) = 1;
                        end
                        
                        
                    end
                end
            end
            
            % open file
            fname = sprintf('./bc/cdb.%1.8d.dat', num);
            
            disp(['Writing file -> ', fname])
            
            fp = fopen(fname, 'w');
            
            % dump data on disk
            fwrite(fp, P3DF, 'uint8');
            
            fclose(fp);
            
            clear P3DS fname fp P3DF
            
            % update processor ID
            num = num + 1;
            
        end
    end
end


