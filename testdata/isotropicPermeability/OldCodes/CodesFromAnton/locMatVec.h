/*---------------------------------------------------------------------------*
 *....................... ELEMENT MATRICES & VECTORS  .......................*
 *---------------------------------------------------------------------------*/
#ifndef locMatVecH
#define locMatVecH
//---------------------------------------------------------------------------
class Tensor2RS;
class Tensor4RS;
class Shapes;
/*---------------------------------------------------------------------------*
 * .................. NUMBER OF NODES IN THE ELEMENT ........................*
 *---------------------------------------------------------------------------*/
const int MAXDOFSIZE(3*8);
/*---------------------------------------------------------------------------*
 *...........................   D.O.F. LIST   ...............................*
 *---------------------------------------------------------------------------*/
struct match
{
	int lr;
	int gr;
};
//---------------------------------------------------------------------------
bool operator < (const match &a, const match &b);
/*---------------------------------------------------------------------------*
 * NOTES on structure "match":                                               *
 *                                                                           *
 * lr - local matrix row index                                               *
 * gr - global matrix row index                                              *
 *                                                                           *
 * Structure "match" defines the correspondence between local matrix row     *
 * index and global matrix row index. To allow addition of local matrix      *
 * rows to global matrix rows in the increasing order of global row index    *
 * the "<" operator is provided.                                             *
 *---------------------------------------------------------------------------*/
class DOFList
{
 public:
	int   num;             // number of nodes in element
	int   dim;             // dimension of the list
	match dof[MAXDOFSIZE]; // Elemental Degree Of Freedom list
	// element degree of freedom list computation
	void elem(int * inodes, int eNumNod, int ndof); // node list, num. nodes & dofs
};
/*---------------------------------------------------------------------------*
 *.......................   GENERAL LOCAL MATRIX   ..........................*
 *---------------------------------------------------------------------------*/
class LocMatrix
{
 public:
	int      num;     // number of nodes in element
	int      dim;     // Row or column dimension of matrix
	match  * dof;     // Elemental Degree Of Freedom array
	double   s [(MAXDOFSIZE*(MAXDOFSIZE+1))/2];  // Upper triangular matrix
	double   bf[MAXDOFSIZE*MAXDOFSIZE];          // Square matrix
/*---------------------------------------------------------------------------*
 * Elemental Degree Of Freedom array "dof" contains global matrix            *
 * row indices of correspondent rows of local matrix and                     *
 * it is sorted in increasing order of global row index.                     *
 *---------------------------------------------------------------------------*/
	// initialization
	void setup(DOFList & d); // Element degree of freedom list
	// upper triangular to square matrix transformation
	void square(); // "s" -> "bf"
	// internal stiffness matrix
	void stressInt(Tensor4RS & C,   // stiffness tensor
	/* */          double    * dX1, // shape function gradients
	/* */          double    * dX2,
	/* */          double    * dX3,
	/* */          double      wf); // weight factor
	// external stiffness matrix
	void stressExt(double   W,   // derivative of mean stress w.r.t dz
	/* */          double * N,   // shape functions
	/* */          double * xa,  // surface tangent vectors
	/* */          double * xb,
	/* */          double   wf); // weight factor
	// internal thermal matrix
	void powerInt(double   Cp,   // specific heat
	/* */         double   Ro,   // density
	/* */         double   step, // time step
	/* */         double   k,    // thermal conductivity
	/* */         double * N,    // shape functions
	/* */         double * dX1,  // shape function gradients
	/* */         double * dX2,
	/* */         double * dX3,
	/* */         double   wf);  // weight factor
};
/*---------------------------------------------------------------------------*
 *...........................   LOCAL VECTOR   ..............................*
 *---------------------------------------------------------------------------*/
class LocVector
{
 public:
	int      num;           // number of nodes in element
	int      dim;           // Dimension of local vector
	match  * dof;           // Elemental Degree Of Freedom array
	double   s[MAXDOFSIZE]; // Element vector
	// initialization
	void setup(DOFList & d);    // Element degree of freedom list
	// internal stress vector
	void stressInt(Tensor2RS & S,   // stress tensor
	/* */          double    * dX1, // shape function gradients
	/* */          double    * dX2,
	/* */          double    * dX3,
	/* */          double      wf); // weight factor
	// external stress vector
	void stressExt(Tensor2RS & S,   // external stress tensor
	/* */          double    * N,   // shape functions
	/* */          double    * xa,  // tangent vectors
	/* */          double    * xb,
	/* */          double      wf); // weight factor
	// external force vector (gravity weight)
	void weightExt(double   Ro,    // material density
	/* */          double * g,     // weight load components
	/* */          double * N,     // shape functions
	/* */          double   wf);   // weight factor
	// internal power vector
	void  powerInt(double   Ro,  // density
	/* */          double   U,   // rate of change of internal energy
	/* */          double * q,   // internal heat flow vector
	/* */          double * N,   // shape functions
	/* */          double * dX1, // shape function gradients
	/* */          double * dX2,
	/* */          double * dX3,
	/* */          double   wf); // weight factor
	// external power vector (volume heat sources)
	void powerExt(double   Hr,   // heat sources per unit volume
	/* */         double * N,    // shape functions
	/* */         double   wf);  // weight factor
};
//---------------------------------------------------------------------------
// Local & global vector summation operation:
void operator += (double * G, LocVector & L);
//---------------------------------------------------------------------------
#endif














