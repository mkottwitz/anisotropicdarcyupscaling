/*---------------------------------------------------------------------------*
 *....................... ELEMENT MATRICES & VECTORS  .......................*
 *---------------------------------------------------------------------------*/
#include <algorithm>
using namespace std;
//---------------------------------------------------------------------------
#pragma hdrstop
#include "locMatVec.h"
#include "tensMatHist.h"
/*---------------------------------------------------------------------------*
 *.............................   DOF LIST   ................................*
 *---------------------------------------------------------------------------*/
void DOFList::elem(int * inodes, int eNumNod, int ndof)
{
	// put number of nodes
	num = eNumNod;
	// Compute elemental DOF array:
	dim = 0;
	for(int i(0); i < num; i++)
	{	// Obtain first global row index of this node:
		int row_ind(ndof * inodes[i]);
		// set dof:
		for(int k(0); k < ndof; k++)
		{	dof[dim].gr = row_ind;
			dof[dim].lr = dim;
			row_ind++;
			dim++;
		}
	}
	// Sort in increasing order of global row index:
	sort(dof, dof + dim);
}
/*---------------------------------------------------------------------------*
 *...........................   LOCAL MATRIX   ..............................*
 *---------------------------------------------------------------------------*/
void LocMatrix::setup(DOFList & d)
{
	num = d.num;
	dim = d.dim;
	dof = d.dof;
	int m((dim * dim + dim)/2);
	for(int i(0); i < m; s[i] = 0.0, i++);
}
//---------------------------------------------------------------------------
void LocMatrix::square()
{
	// Transform upper triangular to square matrix:
	int s_len(0), bf_row(0), bf_col;
	for(int i(0); i < dim; i++)
	{	bf_row     += i;
		bf_col      = bf_row + dim;
		bf[bf_row++] = s[s_len++];
		for(int j(i+1); j < dim; j++)
		{	double term(s[s_len++]);
			bf[bf_row++] = term;
			bf[bf_col]   = term;
			bf_col      += dim;
		}
	}
}
//---------------------------------------------------------------------------
// internal stiffness matrix
void LocMatrix::stressInt(Tensor4RS & C,   // stiffness tensor
/* */                     double    * dX1, // shape function gradients
/* */                     double    * dX2,
/* */                     double    * dX3,
/* */                     double      wf)  // weight factor
{
	//------------------------
	// INTERNAL TERM: "+" SIGN
	//------------------------
	// Initialize third row iterator (r3) & increment:
	int inc(dim), r1, r2, r3(0);
	double   H1, H2, H3, h1, h2, h3;
	double   p11, p12, p13, p21, p22, p23, p31, p32, p33;
	// Compute upper triangular matrix integral (external loop):
	for(int i(0); i < num; i++)
	{	// Update iterators:
		r1 = r3;
		r2 = r1+inc;
		r3 = r2+inc-1;
		inc -= 3;
		// Retrieve outer set of derivatives:
		H1 = dX1[i];
		H2 = dX2[i];
		H3 = dX3[i];
		// Internal loop:
		for(int j(i); j < num; j++)
		{	// Retrieve inner set of derivatives:
			h1 = dX1[j];
			h2 = dX2[j];
			h3 = dX3[j];
			// Premultiply derivatives combinations:
			p11 = H1*h1; p12 = H1*h2; p13 = H1*h3;
			p21 = H2*h1; p22 = H2*h2; p23 = H2*h3;
			p31 = H3*h1; p32 = H3*h2; p33 = H3*h3;
			// Compute lower triangular parts of submatrix
			// !!! (+)
			if(i != j)
			{	// second row:
				s[r2++] += (p21*C._12 + p11*C._14 + p31*C._16
				/**/    +   p22*C._24 + p12*C._44 + p32*C._46
				/**/    +   p23*C._25 + p13*C._45 + p33*C._56)*wf;
				// third row:
				s[r3++] += (p31*C._13 + p11*C._15 + p21*C._16
				/**/    +   p32*C._34 + p12*C._45 + p22*C._46
				/**/    +   p33*C._35 + p13*C._55 + p23*C._56)*wf;
				s[r3++] += (p32*C._23 + p12*C._25 + p22*C._26
				/**/    +   p31*C._34 + p11*C._45 + p21*C._46
				/**/    +   p33*C._36 + p13*C._56 + p23*C._66)*wf;
			}
			// Compute upper triangular & dagonal parts of submatrix:
			// first row:
			s[r1++]    += (p11*C._11 + p21*C._14 + p31*C._15
			/**/       +   p12*C._14 + p22*C._44 + p32*C._45
			/**/       +   p13*C._15 + p23*C._45 + p33*C._55)*wf;
			s[r1++]    += (p12*C._12 + p22*C._24 + p32*C._25
			/**/       +   p11*C._14 + p21*C._44 + p31*C._45
			/**/       +   p13*C._16 + p23*C._46 + p33*C._56)*wf;
			s[r1++]    += (p13*C._13 + p23*C._34 + p33*C._35
			/**/       +   p11*C._15 + p21*C._45 + p31*C._55
			/**/       +   p12*C._16 + p22*C._46 + p32*C._56)*wf;
			// second row:
			s[r2++]    += (p22*C._22 + p12*C._24 + p32*C._26
			/**/       +   p21*C._24 + p11*C._44 + p31*C._46
			/**/       +   p23*C._26 + p13*C._46 + p33*C._66)*wf;
			s[r2++]    += (p23*C._23 + p13*C._34 + p33*C._36
			/**/       +   p21*C._25 + p11*C._45 + p31*C._56
			/**/       +   p22*C._26 + p12*C._46 + p32*C._66)*wf;
			// third row:
			s[r3++]    += (p33*C._33 + p13*C._35 + p23*C._36
			/**/       +   p31*C._35 + p11*C._55 + p21*C._56
			/**/       +   p32*C._36 + p12*C._56 + p22*C._66)*wf;
		}
	}
}
//---------------------------------------------------------------------------
// external stiffness matrix
void LocMatrix::stressExt(double   W,  // derivative of mean stress w.r.t dz
/* */                     double * N,  // shape functions
/* */                     double * xa, // surface tangent vectors
/* */                     double * xb,
/* */                     double   wf) // weight factor
{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *      Mean stress external stiffness (simmetrized)                         *
 *                                                                           *
 *                        | 0  0   0 |                                       *
 *      Kw_ij = N_i*N_j*W*| 0  0   0 |*wf                                    *
 *                        | 0  0  n3 |                                       *
 *                                                                           *
 *      W - derivative of mean stress w.r.t vertical displacement            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
	//------------------------
	// EXTERNAL TERM: "-" SIGN
	//------------------------
	double   X1a, X2a, X3a;
	double   X1b, X2b, X3b;
	int      inc(dim), r1, r2, r3(0);
	double   Ni, Nj, n3;
	// copy tangent vectors
	X1a = xa[0];   X1b = xb[0];
	X2a = xa[1];   X2b = xb[1];
	// evaluate z-component of cross-product
	n3 = X1a*X2b - X2a*X1b;
	// compute upper triangular matrix (outer loop)
	// !!! (-)
	for(int i(0); i < num; i++)
	{	// update iterators
		r1 = r3;
		r2 = r1+inc;
		r3 = r2+inc-1;
		inc -= 3;
		// retrieve outer loop constants
		Ni = N[i];
		// inner loop
		for(int j(i); j < num; j++)
		{	// retrieve inner loop constants
			Nj = N[j];
/*
			if(i != j) r3 += 2;
			s[r3++] -= Ni*Nj*W*n3*wf;
*/
			if(i != j)
			{//	r2 += 1;
				r3 += 2;
			}
//				r1 += 3;
//				r2 += 2;
				s[r3++] -= Ni*Nj*W*n3*wf;
		}
	}
}
//---------------------------------------------------------------------------
// internal thermal matrix
void LocMatrix::powerInt(double   Cp,   // specific heat
/* */                    double   Ro,   // density
/* */                    double   step, // time step
/* */                    double   k,    // thermal conductivity
/* */                    double * N,    // shape functions
/* */                    double * dX1,  // shape function gradients
/* */                    double * dX2,
/* */                    double * dX3,
/* */                    double   wf)   // weight factor
{
	//------------------------
	// INTERNAL TERM: "+" SIGN
	//------------------------
	// Initialize iterator
	int    itr(0);
	double H1, H2, H3, h1, h2, h3, Ni, Nj;
	// Compute upper triangular matrix integral (external loop):
	for(int i(0); i < num; i++)
	{	// Retrieve outer set of derivatives & shape function
		H1 = dX1[i];
		H2 = dX2[i];
		H3 = dX3[i];
		Ni = N  [i];
		// Internal loop:
		for(int j(i); j < num; j++)
		{	// Retrieve inner set of derivatives & shape function
			h1 = dX1[j];
			h2 = dX2[j];
			h3 = dX3[j];
			Nj = N  [j];
			// update matrix terms
			// !!! (+)
			s[itr++] += (Ni*(Ro*Cp/step)*Nj + k*(H1*h1 + H2*h2 + H3*h3))*wf;
		}
	}
}
/*---------------------------------------------------------------------------*
 *...........................   LOCAL VECTOR   ..............................*
 *---------------------------------------------------------------------------*/
void LocVector::setup(DOFList & d)
{
	num = d.num;
	dim = d.dim;
	dof = d.dof;
	for(int i(0); i < dim; s[i] = 0.0, i++);
}
//---------------------------------------------------------------------------
// internal stress vector
void LocVector::stressInt(Tensor2RS & S,   // stress tensor
/* */                     double    * dX1, // shape function gradients
/* */                     double    * dX2,
/* */                     double    * dX3,
/* */                     double      wf)  // weight factor
{
	//------------------------
	// INTERNAL TERM: "+" SIGN
	//------------------------
	double H1, H2, H3;
	// Compute divergence of stress tensor:
	for(int i(0), j(0); i < num; i++)
	{	H1 = dX1[i];
		H2 = dX2[i];
		H3 = dX3[i];
		// update force components
		// !!! (+)
		s[j++] += (H1*S._11 + H2*S._12 + H3*S._13)*wf;
		s[j++] += (H1*S._12 + H2*S._22 + H3*S._23)*wf;
		s[j++] += (H1*S._13 + H2*S._23 + H3*S._33)*wf;
	}
}
//---------------------------------------------------------------------------
// external stress vector
void LocVector::stressExt(Tensor2RS & S,  // external stress tensor
/* */                     double    * N,  // Shape functions
/* */                     double    * xa, // tangent vectors
/* */                     double    * xb,
/* */                     double      wf) // weight factor
{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                 |s11 s12 s13| | n1 |                                      *
 *      Fp_i = N_i*|....s22 s23|*| n2 |*wf                                   *
 *                 |........s33| | n3 |                                      *
 *                                                                           *
 *      Normal vector in the above formula is not unit, but scaled           *
 *      with the Jacobian determinant in the integration point               *
 *      Normal vector should point outward the domain                        *
 *---------------------------------------------------------------------------*/
	//------------------------
	// EXTERNAL TERM: "-" SIGN
	//------------------------
	double H1, H2, H3;
	double ft, n1, n2, n3;
	// compute normal vector (outward)
	n1 = xa[1]*xb[2] - xa[2]*xb[1];
	n2 = xa[2]*xb[0] - xa[0]*xb[2];
	n3 = xa[0]*xb[1] - xa[1]*xb[0];
	// compute external force vector
	for(int i(0), j(0); i < num; i++)
	{	// update force components
		// !!! (-)
		s[j++] -= N[i]*(S._11*n1 + S._12*n2 + S._13*n3)*wf;
		s[j++] -= N[i]*(S._12*n1 + S._22*n2 + S._23*n3)*wf;
		s[j++] -= N[i]*(S._13*n1 + S._23*n2 + S._33*n3)*wf;
	}
}
//---------------------------------------------------------------------------
// external force vector (gravity weight)
void LocVector::weightExt(double   Ro, // material density
/* */                     double * g,  // weight load components
/* */                     double * N,  // shape functions
/* */                     double   wf) // weight factor
{
	//------------------------
	// EXTERNAL TERM: "-" SIGN
	//------------------------
	// compute external force vector
	for(int i(0), j(0); i < num; i++)
	{	// update force components
		// !!! (-)
		s[j++] -= N[i]*Ro*g[0]*wf;
		s[j++] -= N[i]*Ro*g[1]*wf;
		s[j++] -= N[i]*Ro*g[2]*wf;
	}
}
//---------------------------------------------------------------------------
// internal power vector
void  LocVector::powerInt(double   Ro,  // density
/* */                     double   Ud,  // rate of change of internal energy
/* */                     double * q,   // internal heat flow vector
/* */                     double * N,   // shape functions
/* */                     double * dX1, // shape function gradients
/* */                     double * dX2,
/* */                     double * dX3,
/* */                     double   wf)  // weight factor
{
	//------------------------
	// INTERNAL TERM: "+" SIGN
	//------------------------
	for(int i(0); i < num; i++)
	{	// update vector components
		// !!! (+)
		s[i] += (N[i]*Ro*Ud - (dX1[i]*q[0] + dX2[i]*q[1] + dX3[i]*q[2]))*wf;
	}
}
//---------------------------------------------------------------------------
// external power vector (volume heat sources)
void LocVector::powerExt(double   Hr, // heat sources per unit volume
/* */                    double * N,  // shape functions
/* */                    double   wf) // weight factor
{
	//------------------------
	// EXTERNAL TERM: "-" SIGN
	//------------------------
	for(int i(0); i < num; i++)
	{	// update vector components
		// !!! (-)
		s[i] -= N[i]*Hr*wf;
	}
}
//---------------------------------------------------------------------------
bool operator < (const match &a, const match &b)
{
	return a.gr < b.gr;
}
//---------------------------------------------------------------------------
void operator += (double * G, LocVector & L)
{
	for(int i(0); i < L.dim; i++)
	{	// Get local and global row index:
		int glob_row = L.dof[i].gr;
		int loc_row  = L.dof[i].lr;
		// Add local to global vector:
		G[glob_row] += L.s[loc_row];
	}
}
//---------------------------------------------------------------------------







