//---------------------------------------------------------------------------
//.................... FINITE ELEMENT INTERPOLATION .........................
//---------------------------------------------------------------------------
#include <math.h>
//---------------------------------------------------------------------------
#pragma hdrstop
//---------------------------------------------------------------------------
#include "shapeFunct.h"
/*---------------------------------------------------------------------------*
 *.............   Intergation point coordinates and weights   ...............*
 *---------------------------------------------------------------------------*/
void Quadr::elem()
{
/*
	// --- 27-point integration rule for hexahedron ---
	n = 0;
	double x [3] = {-0.774596669241483, 0.               , 0.774596669241483};
	double h [3] = { 0.555555555555556, 0.888888888888889, 0.555555555555556};
	for(int i(0); i < 3; i++)
	{	for(int j(0); j < 3; j++)
		{	for(int k(0); k < 3; k++)
			{	x1[n] = x[i];
				x2[n] = x[j];
				x3[n] = x[k];
				Wt[n] = h[i]*h[j]*h[k];
				n++;
			}
		}
	}
*/
	// --- 8-point integration rule for hexahedron ---
	n = 8;
	double k(0.577350269189626), w(1.0);
	x1[0] =  k;   x2[0] =  k;   x3[0] = -k;   Wt[0] = w;
	x1[1] =  k;   x2[1] = -k;   x3[1] = -k;   Wt[1] = w;
	x1[2] = -k;   x2[2] =  k;   x3[2] = -k;   Wt[2] = w;
	x1[3] = -k;   x2[3] = -k;   x3[3] = -k;   Wt[3] = w;
	x1[4] =  k;   x2[4] =  k;   x3[4] =  k;   Wt[4] = w;
	x1[5] =  k;   x2[5] = -k;   x3[5] =  k;   Wt[5] = w;
	x1[6] = -k;   x2[6] =  k;   x3[6] =  k;   Wt[6] = w;
	x1[7] = -k;   x2[7] = -k;   x3[7] =  k;   Wt[7] = w;
}
//---------------------------------------------------------------------------
void Quadr::face()
{
	// --- first order hexahedron integration rule on the face ---
	n = 4;
	double k(0.577350269189626), w(1.0);
	x1[0] =  k;   x2[0] =  k;   Wt[0] = w;
	x1[1] =  k;   x2[1] = -k;   Wt[1] = w;
	x1[2] = -k;   x2[2] =  k;   Wt[2] = w;
	x1[3] = -k;   x2[3] = -k;   Wt[3] = w;
}
/*---------------------------------------------------------------------------*
 *..........................   Shape functions   ............................*
 *---------------------------------------------------------------------------*/
void Shapes::elem(double x1, double x2, double x3)
{
	n = 8;
	// Define constants:
	double k  (0.125    );
	double px1(1.0 + x1 ), px2(1.0 + x2 ), px3(1.0 + x3 );
	double mx1(1.0 - x1 ), mx2(1.0 - x2 ), mx3(1.0 - x3 );
	// Compute nodal shape functions in the space:
	N[0] = k*mx1*mx2*mx3;  // N1
	N[1] = k*px1*mx2*mx3;  // N2
	N[2] = k*px1*px2*mx3;  // N3
	N[3] = k*mx1*px2*mx3;  // N4
	N[4] = k*mx1*mx2*px3;  // N5
	N[5] = k*px1*mx2*px3;  // N6
	N[6] = k*px1*px2*px3;  // N7
	N[7] = k*mx1*px2*px3;  // N8
}
//---------------------------------------------------------------------------
void Shapes::face(double x1, double x2)
{
	n = 4;
	// Define constants:
	double k  (0.25     );
	double px1(1.0 + x1 ), px2(1.0 + x2 );
	double mx1(1.0 - x1 ), mx2(1.0 - x2 );
	// Compute nodal shape functions on the face:
	N[0] = k*mx1*mx2;  // N1
	N[1] = k*px1*mx2;  // N2
	N[2] = k*px1*px2;  // N3
	N[3] = k*mx1*px2;  // N4
}
//---------------------------------------------------------------------------
double Shapes::interpScalElem(double * nscal)
{
	return N[0]*nscal[0] + N[1]*nscal[1] + N[2]*nscal[2] + N[3]*nscal[3]+
			 N[4]*nscal[4] + N[5]*nscal[5] + N[6]*nscal[6] + N[7]*nscal[7];
}
//---------------------------------------------------------------------------
double Shapes::interpScalFace(double * nscal)
{
	return N[0]*nscal[0] + N[1]*nscal[1] + N[2]*nscal[2] + N[3]*nscal[3];
}
//---------------------------------------------------------------------------
void Shapes::interpVectElem(double * nvec, double * vec)
{
	vec[0] = N[0]*nvec[0 ] + N[1]*nvec[3 ] + N[2]*nvec[6 ] + N[3]*nvec[9 ]
	+        N[4]*nvec[12] + N[5]*nvec[15] + N[6]*nvec[18] + N[7]*nvec[21];
	vec[1] = N[0]*nvec[1 ] + N[1]*nvec[4 ] + N[2]*nvec[7 ] + N[3]*nvec[10]
	+        N[4]*nvec[13] + N[5]*nvec[16] + N[6]*nvec[19] + N[7]*nvec[22];
	vec[2] = N[0]*nvec[2 ] + N[1]*nvec[5 ] + N[2]*nvec[8 ] + N[3]*nvec[11]
	+        N[4]*nvec[14] + N[5]*nvec[17] + N[6]*nvec[20] + N[7]*nvec[23];
}
//---------------------------------------------------------------------------
void Shapes::interpVectFace(double * nvec, double * vec)
{
	vec[0] = N[0]*nvec[0] + N[1]*nvec[3] + N[2]*nvec[6] + N[3]*nvec[9 ];
	vec[1] = N[0]*nvec[1] + N[1]*nvec[4] + N[2]*nvec[7] + N[3]*nvec[10];
	vec[2] = N[0]*nvec[2] + N[1]*nvec[5] + N[2]*nvec[8] + N[3]*nvec[11];
}
//---------------------------------------------------------------------------
void Shapes::interpVectQuad(double * nvec, double * vec)
{
	vec[0] = N[0]*nvec[0] + N[1]*nvec[2] + N[2]*nvec[4] + N[3]*nvec[6];
	vec[1] = N[0]*nvec[1] + N[1]*nvec[3] + N[2]*nvec[5] + N[3]*nvec[7];
}
/*---------------------------------------------------------------------------*
 *....................   Shape function derivatives   .......................*
 *---------------------------------------------------------------------------*/
void Deriv::elem(double x1, double x2, double x3)
{
	// --- first order hexahedron shape function derivatives in the space ---
	n = 8;
	// Define constants:
	double k  (0.125    );
	double px1(1.0 + x1 ), px2(1.0 + x2 ), px3(1.0 + x3 );
	double mx1(1.0 - x1 ), mx2(1.0 - x2 ), mx3(1.0 - x3 );
	/* Compute shape functions derivatives with respect to local coordinates:
	d/dX1                d/dX2                 d/dX3                 */
	dX1[0]= - k*mx2*mx3; dX2[0] = - k*mx1*mx3; dX3[0] = - k*mx1*mx2; // dN1
	dX1[1]=   k*mx2*mx3; dX2[1] = - k*px1*mx3; dX3[1] = - k*px1*mx2; // dN2          
	dX1[2]=   k*px2*mx3; dX2[2] =   k*px1*mx3; dX3[2] = - k*px1*px2; // dN3          
	dX1[3]= - k*px2*mx3; dX2[3] =   k*mx1*mx3; dX3[3] = - k*mx1*px2; // dN4          
	dX1[4]= - k*mx2*px3; dX2[4] = - k*mx1*px3; dX3[4] =   k*mx1*mx2; // dN5          
	dX1[5]=   k*mx2*px3; dX2[5] = - k*px1*px3; dX3[5] =   k*px1*mx2; // dN6          
	dX1[6]=   k*px2*px3; dX2[6] =   k*px1*px3; dX3[6] =   k*px1*px2; // dN7          
	dX1[7]= - k*px2*px3; dX2[7] =   k*mx1*px3; dX3[7] =   k*mx1*px2; // dN8          
}
//---------------------------------------------------------------------------
void Deriv::face(double x1, double x2)
{
	// --- first order hexahedron shape function derivatives on the face ---
	n = 4;
	// Define constants:
	double k  (0.25     );
	double px1(1.0 + x1 ), px2(1.0 + x2 );
	double mx1(1.0 - x1 ), mx2(1.0 - x2 );
	/* Compute shape functions derivatives with respect to local coordinates:
	d/dX1            d/dX2             */
	dX1[0]= - k*mx2; dX2[0] = - k*mx1; // dN1
	dX1[1]=   k*mx2; dX2[1] = - k*px1; // dN2
	dX1[2]=   k*px2; dX2[2] =   k*px1; // dN3
	dX1[3]= - k*px2; dX2[3] =   k*mx1; // dN4
}
//---------------------------------------------------------------------------
void Deriv::setup()
{
	n = 8;
	dX1[0] = 0.0; dX2[0] = 0.0; dX3[0] = 0.0; // dN1
	dX1[1] = 0.0; dX2[1] = 0.0; dX3[1] = 0.0; // dN2
	dX1[2] = 0.0; dX2[2] = 0.0; dX3[2] = 0.0; // dN3
	dX1[3] = 0.0; dX2[3] = 0.0; dX3[3] = 0.0; // dN4
	dX1[4] = 0.0; dX2[4] = 0.0; dX3[4] = 0.0; // dN5
	dX1[5] = 0.0; dX2[5] = 0.0; dX3[5] = 0.0; // dN6
	dX1[6] = 0.0; dX2[6] = 0.0; dX3[6] = 0.0; // dN7
	dX1[7] = 0.0; dX2[7] = 0.0; dX3[7] = 0.0; // dN8
}
//---------------------------------------------------------------------------
void Deriv::integrate(Deriv & B, double dV)
{
	dX1[0] += B.dX1[0]*dV; dX2[0] += B.dX2[0]*dV; dX3[0] += B.dX3[0]*dV; // dN1
	dX1[1] += B.dX1[1]*dV; dX2[1] += B.dX2[1]*dV; dX3[1] += B.dX3[1]*dV; // dN2
	dX1[2] += B.dX1[2]*dV; dX2[2] += B.dX2[2]*dV; dX3[2] += B.dX3[2]*dV; // dN3
	dX1[3] += B.dX1[3]*dV; dX2[3] += B.dX2[3]*dV; dX3[3] += B.dX3[3]*dV; // dN4
	dX1[4] += B.dX1[4]*dV; dX2[4] += B.dX2[4]*dV; dX3[4] += B.dX3[4]*dV; // dN5
	dX1[5] += B.dX1[5]*dV; dX2[5] += B.dX2[5]*dV; dX3[5] += B.dX3[5]*dV; // dN6
	dX1[6] += B.dX1[6]*dV; dX2[6] += B.dX2[6]*dV; dX3[6] += B.dX3[6]*dV; // dN7
	dX1[7] += B.dX1[7]*dV; dX2[7] += B.dX2[7]*dV; dX3[7] += B.dX3[7]*dV; // dN8
}
//---------------------------------------------------------------------------
//............... Coordinate transformation (Jacoby) matrix .................
//---------------------------------------------------------------------------
double Jacoby::elem(Deriv & LB, Deriv & GB, double * coord)
{
	double dX1, dX2, dX3, det;
	// set number of nodes
	GB.n = LB.n;
	// compute inverse jacoby matrix
	det = invJac(LB, coord);
	// Transform derivatives into global coordinates:
	// |dNi/dx1|            |dNi/dX1|
	// |dNi/dx2| = [J]^-1 * |dNi/dX2|
	// |dNi/dx3|            |dNi/dX3|
	for(int i(0), n(LB.n); i < n; i++)
	{	// Get shape functions derivatives:
		dX1 = LB.dX1[i];
		dX2 = LB.dX2[i];
		dX3 = LB.dX3[i];
		// Transform derivatives:
		GB.dX1[i] = i11 * dX1 + i12 * dX2 + i13 * dX3;
		GB.dX2[i] = i21 * dX1 + i22 * dX2 + i23 * dX3;
		GB.dX3[i] = i31 * dX1 + i32 * dX2 + i33 * dX3;
	}
	return det;
}
//---------------------------------------------------------------------------
void Jacoby::face(Deriv & LB, double * coord, double * xa, double * xb)
{
	double dx1, dx2, dx3, dX1, dX2;
	// initialize tangent vectors
	xa[0] = 0.0;   xb[0] = 0.0;
	xa[1] = 0.0;   xb[1] = 0.0;
	xa[2] = 0.0;   xb[2] = 0.0;
	// compute tangent vectors
	for(unsigned i(0), j(0), n(LB.n); i < n; i++)
	{	// get face coordinates & derivatives
		dx1 = coord[j++];
		dx2 = coord[j++];
		dx3 = coord[j++];
		dX1 = LB.dX1[i];
		dX2 = LB.dX2[i];
		// update tangent vectors
		xa[0] += dx1*dX1;   xb[0] += dx1*dX2;
		xa[1] += dx2*dX1;   xb[1] += dx2*dX2;
		xa[2] += dx3*dX1;   xb[2] += dx3*dX2;
	}
}
//---------------------------------------------------------------------------
double Jacoby::invJac(Deriv & LB, double * coord)
{
	double det;
	double dx1, dx2, dx3, dX1, dX2, dX3;
	double j11, j12, j13, j21, j22, j23, j31, j32, j33;
	// Initialize Jacoby matrix:
	j11 = 0.0;   j12 = 0.0;   j13 = 0.0;
	j21 = 0.0;   j22 = 0.0;   j23 = 0.0;
	j31 = 0.0;   j32 = 0.0;   j33 = 0.0;
	// Compute Jacoby matrix:
	for(int i(0), j(0), n(LB.n); i < n; i++)
	{	// Get nodal point coordinates:
		dx1 = coord[j++];
		dx2 = coord[j++];
		dx3 = coord[j++];
		// Get shape functions derivatives:
		dX1 = LB.dX1[i];
		dX2 = LB.dX2[i];
		dX3 = LB.dX3[i];
		// Update Jacoby matrix:
		j11 += dX1 * dx1;   j12 += dX1 * dx2;   j13 += dX1 * dx3;
		j21 += dX2 * dx1;   j22 += dX2 * dx2;   j23 += dX2 * dx3;
		j31 += dX3 * dx1;   j32 += dX3 * dx2;   j33 += dX3 * dx3;
	}
	// Compute Jacoby matrix determinant:
	det = (j11*(j22*j33 - j23*j32)
	+      j12*(j23*j31 - j21*j33)
	+      j13*(j21*j32 - j22*j31));
	// Compute inverse Jacoby matrix:
	i11 = (j22 * j33 - j23 * j32)/det;
	i12 = (j13 * j32 - j12 * j33)/det;
	i13 = (j12 * j23 - j13 * j22)/det;
	i21 = (j23 * j31 - j21 * j33)/det;
	i22 = (j11 * j33 - j13 * j31)/det;
	i23 = (j13 * j21 - j11 * j23)/det;
	i31 = (j21 * j32 - j22 * j31)/det;
	i32 = (j12 * j31 - j11 * j32)/det;
	i33 = (j11 * j22 - j12 * j21)/det;
	return det;
}
//---------------------------------------------------------------------------
void Jacoby::update(double * eps, double * r)
{
	// eps += [J]^-1 * r
	eps[0] += i11*r[0] + i12*r[1] + i13*r[2];
	eps[1] += i21*r[0] + i22*r[1] + i23*r[2];
	eps[2] += i31*r[0] + i32*r[1] + i33*r[2];
}
//---------------------------------------------------------------------------
void Jac2D::invJac(Deriv & LB, double * coord)
{
	double det;
	double dx1, dx2, dX1, dX2;
	double j11, j12, j21, j22;
	// Initialize Jacoby matrix:
	j11 = 0.0;   j12 = 0.0;
	j21 = 0.0;   j22 = 0.0;
	// Compute Jacoby matrix:
	for(int i(0), j(0), n(LB.n); i < n; i++)
	{	// Get nodal point coordinates:
		dx1 = coord[j++];
		dx2 = coord[j++];
		// Get shape functions derivatives:
		dX1 = LB.dX1[i];
		dX2 = LB.dX2[i];
		// Update Jacoby matrix:
		j11 += dX1 * dx1;   j12 += dX1 * dx2;
		j21 += dX2 * dx1;   j22 += dX2 * dx2;
	}
	// Compute Jacoby matrix determinant:
	det = j11*j22 - j12*j21;
	// Compute inverse Jacoby matrix:
	i11 =   j22 /det;
	i12 = - j12 /det;
	i21 = - j21 /det;
	i22 =   j11 /det;
}
//---------------------------------------------------------------------------
void Jac2D::update(double * eps, double * r)
{
	// eps += [J]^-1 * r
	eps[0] += i11*r[0] + i12*r[1];
	eps[1] += i21*r[0] + i22*r[1];
}
//---------------------------------------------------------------------------

