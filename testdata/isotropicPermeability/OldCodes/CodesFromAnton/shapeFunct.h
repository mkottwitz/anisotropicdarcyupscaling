//---------------------------------------------------------------------------
//.................... FINITE ELEMENT INTERPOLATION .........................
//---------------------------------------------------------------------------
#ifndef shapeFunctH
#define shapeFunctH
/*---------------------------------------------------------------------------*
 *.............   Intergation point coordinates and weights   ...............*
 *---------------------------------------------------------------------------*/
class Quadr
{
 public:
	int      n;     // Number of integration points
	double   x1[27]; // x1 coordinates array
	double   x2[27]; // x2 coordinates array
	double   x3[27]; // x3 coordinates array
	double   Wt[27]; // Array of weights
	// Integration point array computation:
	void elem ();
	void face ();
};
/*---------------------------------------------------------------------------*
 *..........................   Shape functions   ............................*
 *---------------------------------------------------------------------------*/
class Shapes
{
 public:
	unsigned n;    // Number of nodal shape functions
	double   N[8]; // Shape functions values
	// Nodal shape functions:
	void   elem      (double x1 = 0.0, double x2 = 0.0, double x3 = 0.0);
	void   face      (double x1 = 0.0, double x2 = 0.0);
	double interpScalElem(double * nscal);
	double interpScalFace(double * nscal);
	void   interpVectElem(double * nvec, double * vec);
	void   interpVectFace(double * nvec, double * vec);
	void   interpVectQuad(double * nvec, double * vec);
};
/*---------------------------------------------------------------------------*
 *....................   Shape function derivatives   .......................*
 *---------------------------------------------------------------------------*/
class Deriv
{
 public:
	int      n;      // Number of derivatives with respect to each   coord.
	double   dX1[8]; // Shape function deriv. with respect to X1(x1) coord.
	double   dX2[8]; // Shape function deriv. with respect to X2(x2) coord.
	double   dX3[8]; // Shape function deriv. with respect to X3(x3) coord.
	// X1, X2, X3 - are the local element coordinates
	// x1, x2, x3 - are the global Cartesian coordinates
	// Shape function derivatives with respect to local coordinates:
	void   elem     (double x1 = 0.0, double x2 = 0.0, double x3 = 0.0);
	void   face     (double x1 = 0.0, double x2 = 0.0);
	void   setup    ();
	void   integrate(Deriv & B, double dV);
};
//---------------------------------------------------------------------------
//............... Coordinate transformation (Jacoby) matrix .................
//---------------------------------------------------------------------------
class Jacoby
{
	// inverse Jacoby matrix (differential operator transformation)
 public:
	double i11, i12, i13, i21, i22, i23, i31, i32, i33;
	double elem  (Deriv  & LB,  Deriv  & GB, double * coord);
	void   face  (Deriv  & LB,  double * coord,  double * xa, double * xb);
	double invJac(Deriv  & LB,  double * coord);
	void   update(double * eps, double * r);
};
//---------------------------------------------------------------------------
class Jac2D
{
	// inverse Jacoby matrix (2D version)
 public:
	double i11, i12, i21, i22;
	void invJac(Deriv  & LB,  double * coord);
	void update(double * eps, double * r);
};
//---------------------------------------------------------------------------
#endif




