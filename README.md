This code is an high-performance-computing optimized finite-element element software for effective permeability predictions using the steady-state Darcy equation with unique, anisotropic permeabilities of the continuum cells.


Author contact:

* Maximilian O. Kottwitz (Johannes Gutenberg University Mainz, mkottwi@uni-mainz.de)

* Anton A. Popov         (Johannes Gutenberg University Mainz, popov@uni-mainz.de) 

* Boris J. P. Kaus       (Johannes Gutenberg University Mainz, kaus@uni-mainz.de)  

The development of this code was funded by the German Federal Ministry of Education and Research (BMBF) program GEO:N, grant no. 03G0865A. 

This code originates from the open-source software LaMEM (https://bitbucket.org/bkaus/lamem/src/master/).  
Find below some basic instructions how to install and build LaMEM, that are applicable for this code as well.



## 1. Directory structure
```
/matlab       -  Various matlab files to change initial mesh and read binary output into matlab.
/scripts      -	 Various scripts, currently mainly related to Paraview VTK files.
/src          -	 LaMEM source code; compile with "make mode=opt all" and "make mode=deb all"
/testdata     -	 Directory with various test-cses. 
/utils        -	 Various non-essential files.
/bin          -  Contains binaries after compilation (/deb contains the debug version and /opt the optimized)
```

## 2. Dependencies

### Main dependencies
LaMEM crucially relies on:

  * PETSc 3.9.4, ideally installed with the external packages SUPERLU_DIST, MUMPS and PASTIX

and to a minor extend on:

  * Python, to run the LaMEM testing environment, and see if things work as expected. 
  * Paraview, to visualize results.
  * GIT, in order to pull the latest version of LaMEM
  * MATLAB (version not important), in order to facilitate creating more complicated input geometries
  * geomIO, to create input geometries from Inkscape (see https://geomio.bitbucket.io) 
  * Any text editor, to modify the LaMEM input files. 
  * Visual Studio Code, in case you want to develop new code (some on the development team prefer Eclipse for this)

### Dependency installation
Developed for Linux and Mac machines, but we also have had success on Windows 10, where we recommend installing it through the (new) bash shell. In order for LaMEM to work, you'll need to install the correct version of PETSc first. PETSc is usually not backwards compatible, so it won't work with the incorrect version. Also please note that we do not always immediately update LaMEM to the latest version of PETSc, so you may have to download/install an older version.

* PETSc: 
     See http://www.mcs.anl.gov/petsc/petsc-as/documentation/installation.html
     for installation instructions. We also provide some installation instructions on how to compile 
     PETSc (and mpich, gcc, as well as PETSc) on various machines in /doc/installation. The simplest manner is sometimes to let PETSc install all additional packages (like MPICH), but that often does not result in the most efficient code. You also have to make sure that the path is set correctly in that case (for MPICH, for example). On a large scale cluster, you will have to link against the cluster libraries (for MPI, for example).

* If you want to do debugging of LaMEM as well, it is a good idea to install both a DEBUG and an OPTIMIZED version of LaMEM, in separate directories.

* Nothing else - if the correct version of PETSc is installed, LaMEM will work fine.

	
## 2. Download and build
* Download from BitBucket, preferable by using GIT on your system:

```
        $ git clone https://mkottwitz@bitbucket.org/mkottwitz/anisotropicdarcyupscaling.git
``` 

* Set the environment variables in your .bashrc or .bash_profile scripts such that the LaMEM makefile knows where to look for PETSc:
```
        export PETSC_OPT=DIRECTORY_WHERE_YOU_INSTALLED_YOUR_OPTIMIZED_PETSC
        export PETSC_DEB=DIRECTORY_WHERE_YOU_INSTALLED_YOUR_DEBUG_PETSC 
```

* To build the source in the /src directory:
```
        $ make mode=opt all 
```





